﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SAPBuildingDLL
/// </summary>
public class SAPBuildingDLL
{
    public SAPBuildingDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetCompany(string[] shopgroup)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string shopcode_ = "";

        if (shopgroup.Length != 0)
        {
            shopcode_ += "(";
            for (int i = 0; i < shopgroup.Length; i++)
            {
                shopcode_ += " a.CompanyCode =" + shopgroup[i].Replace("&", "' + char(38) + '") + " or";
            }

            shopcode_ = shopcode_.Substring(0, shopcode_.Length - 2);
            shopcode_ = shopcode_ + ")";
        }

        //strSQL += " select * from SAPCompany where IsActive = '1' order by CompanyNameTH  ";

        strSQL += " select * from SAPCompany a " +
                    " inner join TblCompanyGroupMapping b on b.CompanyCode = a.CompanyCode " +
                    " where a.IsActive = '1' and b.IsActive = 'y' ";
        if (shopcode_ != "")
        {
            strSQL = strSQL + " and " + shopcode_;
        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetBuilding(string BuildingName, string companycode, string IsActive , string[] comcode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string comcode_ = "";

        if (comcode.Length != 0)
        {
            comcode_ += "(";
            for (int i = 0; i < comcode.Length; i++)
            {
                comcode_ += " b.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
            }

            comcode_ = comcode_.Substring(0, comcode_.Length - 2);
            comcode_ = comcode_ + ")";
        }


        if (companycode == "")
        {
            strSQL += " select b.*,c.CompanyNameTH,c.CompanyNameEN, case when b.IsActive = '1' then 'Active' else 'Inactive' end as status  " +
                 " from SAPBuilding b inner " +
                 " join SAPCompany c on b.CompanyCode = c.CompanyCode " +
                 " where (b.BuildingNameEN like '%" + BuildingName.Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or b.BuildingNameTH like '%" + BuildingName.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') and  b.IsActive = '" + IsActive + "'  ";

            if(comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

        }
        else
        {
            strSQL += " select b.*,c.CompanyNameTH ,c.CompanyNameEN, case when b.IsActive = '1' then 'Active' else 'Inactive' end as status " +
                " from SAPBuilding b inner " +
                " join SAPCompany c on b.CompanyCode = c.CompanyCode and b.CompanyCode = '" + companycode + "' " +
                " where (b.BuildingNameEN like '%" + BuildingName.Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or b.BuildingNameTH like '%" + BuildingName.Replace("'", "''").Replace("&", "'+char(38)+'") + "%')  and  b.IsActive = '" + IsActive + "'    ";
        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


}