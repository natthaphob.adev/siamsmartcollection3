﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for add_hierrachyDLL
/// </summary>
public class add_hierrachyDLL
{
    public add_hierrachyDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetUserByTeam_Role(string team, string role , string[] shopgroup)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string shopcode_ = "";

        if (shopgroup.Length != 0)
        {
            shopcode_ += "(";
            for (int i = 0; i < shopgroup.Length; i++)
            {
                shopcode_ += " b.company_code =" + shopgroup[i].Replace("&", "' + char(38) + '") + " or";
            }

            shopcode_ = shopcode_.Substring(0, shopcode_.Length - 2);
            shopcode_ = shopcode_ + ")";
        }

        strSQL += " select distinct u.userid , concat(u.fname,' ',u.lname) as name " +
                    " from tbluser u " +
                    " inner join tblmapp_User_Company b on b.userid = u.userid  " +
                    " inner join TblCompanyDetail c on c.Companycode = b.company_code  " +
                    " where u.user_group = '" + team + "' and role = '" + role + "' and u.flag_active = 'y'  ";
        if (shopcode_ != "")
        {
            strSQL = strSQL + " and " + shopcode_;
        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetOfficer(string team, string[] shopgroup)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string shopcode_ = "";

        if (shopgroup.Length != 0)
        {
            shopcode_ += "(";
            for (int i = 0; i < shopgroup.Length; i++)
            {
                shopcode_ += " b.company_code =" + shopgroup[i].Replace("&", "' + char(38) + '") + " or";
            }

            shopcode_ = shopcode_.Substring(0, shopcode_.Length - 2);
            shopcode_ = shopcode_ + ")";
        }

        strSQL +=   " select distinct u.userid , concat(u.fname,' ',u.lname) as name  " +
                    " from tbluser u " +
                    " inner join tblmapp_User_Company b on b.userid = u.userid     " +
                    " inner join TblCompanyDetail c on c.Companycode = b.company_code  " +
                    "where u.user_group = '" + team + "' and role = 'officer' " +
                    " and u.userid not in(select s.officer_id from tblStaffHierrachy s )  and u.flag_active = 'y'   ";

        if (shopcode_ != "")
        {
            strSQL = strSQL + " and " + shopcode_;
        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void InsertHierrachy(string team, string vp_id, string manager_id, string officer_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Insert into tblStaffHierrachy(team,vp_id,manager_id,officer_id,update_date) values('" + team + "','" + vp_id + "','" + manager_id + "','" + officer_id + "',GETDATE()) ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



}