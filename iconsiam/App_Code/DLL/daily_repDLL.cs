﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class daily_repDLL
    {

        public DataTable GetRep1(string d1, string d2, string[] ShopName, string companyname, string[] floor, string[] cuscode, string[] shopgroup, string building, string roomtype,
            string contract_type, string cat_leasing, string group_location, string industry_group, string user_group, string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string shopname_ = "";
            string companyname_ = "";
            string floor_ = "";
            string cuscode_ = "";
            string shopgroup_ = "";
            string ar_ = "";

            if (ShopName.Length > 0)
            {
                for (int i = 0; i < ShopName.Length; i++)
                {
                    if (ShopName[i] != "")
                    {
                        shopname_ += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }

                }
                if (shopname_ != "")
                {
                    shopname_ = " and " + shopname_.Substring(0, shopname_.Length - 2);

                }
            }

            if (companyname != "")
            {
                companyname_ += " and com.CompanyCode in (" + companyname + ") ";

            }

            if (floor.Length > 0)
            {
                for (int i = 0; i < floor.Length; i++)
                {
                    if (floor[i] != "")
                    {
                        floor_ += " s.smart_floor like '%" + floor[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (floor_ != "")
                {
                    floor_ = " and " + floor_.Substring(0, floor_.Length - 2);
                }
            }

            if (cuscode.Length > 0)
            {
                for (int i = 0; i < cuscode.Length; i++)
                {
                    if (cuscode[i] != "")
                    {
                        cuscode_ += " s.BusinessPartnerCode like '%" + cuscode[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (cuscode_ != "")
                {
                    cuscode_ = " and " + cuscode_.Substring(0, cuscode_.Length - 2);
                }
            }

            if (shopgroup.Length > 0)
            {
                for (int i = 0; i < shopgroup.Length; i++)
                {
                    if (shopgroup[i] != "")
                    {
                        shopgroup_ += " sg.ShopGroupNameTH like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or sg.ShopGroupNameEN like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (shopgroup_ != "")
                {
                    shopgroup_ = " and " + shopgroup_.Substring(0, shopgroup_.Length - 2);
                }
            }

            if (building != "")
            {
                building = " and s.smart_building = '" + building + "' ";

            }
            else
            {
                building = "";
            }

            if (roomtype != "")
            {
                roomtype = " and s.smart_usage_name = '" + roomtype + "' ";

            }
            else
            {
                roomtype = "";
            }

            if (contract_type != "")
            {
                contract_type = " and s.smart_contract_type = '" + contract_type + "' ";

            }
            else
            {
                contract_type = "";
            }//

            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//

            if (group_location != "")
            {
                group_location = " and s.smart_group_location  = '" + group_location + "' ";

            }
            else
            {
                group_location = "";
            }//

            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//


            if (user_group != "")
            {
                //ar_ = "  and s.smart_icon_staff_id in (" + user_group + ") ";
                ar_ = "";
            }
            else
            {
                ar_ = "";
            }

            if (status != "all")
            {
                strSQL = "  select tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,s.SalesTypeName, " +

                    " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2, " +


                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type, s.smart_sqm, s.smart_room_no  " +
                    
                    //nut
                    //" ,case when tr.record_date < s.smart_shop_open then 'ร้านยังไม่เปิด' when tr.no_type = 'close' then 'ร้านปิด' " +
                    //" when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    //" when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' end as statussales, " +
                    //" case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " ,case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' " +
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END " +
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +


                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber   " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and tr.flag_confirm = '" + status + "' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group +
                    " " + ar_ +
                    " Order by tr.record_date,s.ShopName ";
            }
            else
            {
                strSQL = "  select tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,s.SalesTypeName, " +

                    " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2, " +


                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type, s.smart_sqm, s.smart_room_no  " +
                    
                    //nut
                    //" ,case when tr.record_date < s.smart_shop_open then 'ร้านยังไม่เปิด' when tr.no_type = 'close' then 'ร้านปิด' " +
                    //" when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    //" when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' end as statussales, " +
                    //" case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " ,case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' " +
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END " +
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +


                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber   " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group +
                    " " + ar_ +
                    " Order by tr.record_date,s.ShopName ";
            }
            


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetRep1_ae(string d1, string d2, string[] ShopName, string companyname, string[] floor, string[] cuscode, string[] shopgroup, string building, string roomtype,
      string contract_type, string cat_leasing, string group_location, string industry_group, string user_group , string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string shopname_ = "";
            string companyname_ = "";
            string floor_ = "";
            string cuscode_ = "";
            string shopgroup_ = "";

            if (ShopName.Length > 0)
            {
                for (int i = 0; i < ShopName.Length; i++)
                {
                    if (ShopName[i] != "")
                    {
                        shopname_ += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }

                }
                if (shopname_ != "")
                {
                    shopname_ = " and " + shopname_.Substring(0, shopname_.Length - 2);

                }
            }

            if (companyname != "")
            {
                companyname_ += " and com.CompanyCode in(" + companyname + ") ";

            }

            if (floor.Length > 0)
            {
                for (int i = 0; i < floor.Length; i++)
                {
                    if (floor[i] != "")
                    {
                        floor_ += " s.smart_floor like '%" + floor[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (floor_ != "")
                {
                    floor_ = " and " + floor_.Substring(0, floor_.Length - 2);
                }
            }

            if (cuscode.Length > 0)
            {
                for (int i = 0; i < cuscode.Length; i++)
                {
                    if (cuscode[i] != "")
                    {
                        cuscode_ += " s.BusinessPartnerCode like '%" + cuscode[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (cuscode_ != "")
                {
                    cuscode_ = " and " + cuscode_.Substring(0, cuscode_.Length - 2);
                }
            }

            if (shopgroup.Length > 0)
            {
                for (int i = 0; i < shopgroup.Length; i++)
                {
                    if (shopgroup[i] != "")
                    {
                        shopgroup_ += " sg.ShopGroupNameTH like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or sg.ShopGroupNameEN like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (shopgroup_ != "")
                {
                    shopgroup_ = " and " + shopgroup_.Substring(0, shopgroup_.Length - 2);
                }
            }

            if (building != "")
            {
                building = " and s.smart_building = '" + building + "' ";

            }
            else
            {
                building = "";
            }

            if (roomtype != "")
            {
                roomtype = " and s.smart_usage_name = '" + roomtype + "' ";

            }
            else
            {
                roomtype = "";
            }

            if (contract_type != "")
            {
                contract_type = " and s.smart_contract_type = '" + contract_type + "' ";

            }
            else
            {
                contract_type = "";
            }//

            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//

            if (group_location != "")
            {
                group_location = " and s.smart_group_location  = '" + group_location + "' ";

            }
            else
            {
                group_location = "";
            }//

            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//


            string ae_ = "";
            if (user_group != "")
            {
                ae_ = "  and s.smart_icon_ae_id in (" + user_group + ") ";
            }
            else
            {
                ae_ = "";
            }

            if (status != "all")
            {
                strSQL = "  select tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,s.SalesTypeName, " +

                    " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2, " +


                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type, s.smart_sqm, s.smart_room_no  " +

                    " ,case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' "+
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END "+
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and tr.flag_confirm = '" + status + "' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ae_ + " " +
                    " Order by tr.record_date,s.ShopName ";
            }
            else
            {
                strSQL = "  select tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,s.SalesTypeName, " +

                    " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2, " +


                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type, s.smart_sqm, s.smart_room_no  " +

                     //nut
                     //" ,case when tr.record_date < s.smart_shop_open then 'ร้านยังไม่เปิด' when tr.no_type = 'close' then 'ร้านปิด' " +
                     //" when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                     //" when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' end as statussales, " +
                     //" case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " ,case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' " +
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END " +
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ae_ + " " +
                    " Order by tr.record_date,s.ShopName ";
            }
            


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetRep2(string d1, string d2, string cat_leasing, string industry_group, string companyname , string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string companyname_ = "";

            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//

            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//

            if (companyname != "")
            {
                companyname_ += " and com.CompanyCode in (" + companyname + ") ";

            }


            if (status != "all")
            {
                strSQL = "  select top(10) tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName, " +


                    " case when s.smart_record_keyin_type = 'prod_sale' then tr.product_amount - tr.product_amount_vat " +
                    " when s.smart_record_keyin_type = 'prod_service' then (tr.product_amount - tr.product_amount_vat) + " +
                    " (tr.service_serCharge_amount - tr.service_serCharge_amount_vat) else  " +
                    " (tr.product_amount - tr.product_amount_vat) - tr.service_serCharge_amount " +
                    " end as total_ex_var, " +

                    //" tr.product_amount,(tr.product_amount * 7) / 107 as vat, " +
                    //" tr.product_amount - (tr.product_amount * 7) / 107 as exvat, " +
                    // " tr.service_serCharge_amount,(tr.service_serCharge_amount * 7) / 107 as vat2, " +
                    //" tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107 as exvat2, " +

                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm, s.smart_room_no, " +

                    //nut
                     //" case when tr.record_date < s.smart_shop_open then 'ร้านยังไม่เปิด' when tr.no_type = 'close' then 'ร้านปิด' " +
                     //" when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                     //" when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' end as statussales, " +
                     //" case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' " +
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END " +
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and tr.flag_confirm = '" + status + "' " +
                    " " + cat_leasing + " " + industry_group + " " + companyname_ + "    " +
                    " order by total_ex_var desc ";
            }
            else
            {
                strSQL = "  select top(10) tr.record_date ,tr.contractnumber,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName, " +


                    " case when s.smart_record_keyin_type = 'prod_sale' then tr.product_amount - tr.product_amount_vat " +
                    " when s.smart_record_keyin_type = 'prod_service' then (tr.product_amount - tr.product_amount_vat) + " +
                    " (tr.service_serCharge_amount - tr.service_serCharge_amount_vat) else  " +
                    " (tr.product_amount - tr.product_amount_vat) - tr.service_serCharge_amount " +
                    " end as total_ex_var, " +

                    //" tr.product_amount,(tr.product_amount * 7) / 107 as vat, " +
                    //" tr.product_amount - (tr.product_amount * 7) / 107 as exvat, " +
                    // " tr.service_serCharge_amount,(tr.service_serCharge_amount * 7) / 107 as vat2, " +
                    //" tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107 as exvat2, " +

                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm, s.smart_room_no, " +

                    //nut
                    //" case when tr.record_date < s.smart_shop_open then 'ร้านยังไม่เปิด' when tr.no_type = 'close' then 'ร้านปิด' " +
                    //" when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    //" when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' end as statussales, " +
                    //" case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark " +

                    " case when (tr.record_date < s.smart_shop_open or tr.record_date < s.ContractStartDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านยังไม่เปิด'  " +
                    " when (tr.record_date > s.smart_shop_close or tr.record_date > s.ContractEndDate) and (tr.[product_amount] = 0) and (tr.status = 'n')   then 'ร้านปิดก่อนหมดสัญญา'  " +
                    " when tr.no_type = 'close' then 'ร้านปิด' " +
                    " when tr.no_type = 'nobill' then 'No Bill' when tr.no_type = 'other' then 'Other' " +
                    " when tr.no_type = 'Closed' then 'C' when tr.no_type = 'Renovated' then 'R' " +
                    " when tr.no_type = '' then CASE tr.[status] WHEN 'y' THEN	'มียอดขาย' ELSE '' END " +
                    " ELSE '' end as statussales, " +
                    " case when tr.flag_adjust = 'y' then 'ขอแก้ไขยอด' when tr.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status ,tr.remark" +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59'  " +
                    " " + cat_leasing + " " + industry_group + " " + companyname_ + "    " +
                    " order by total_ex_var desc ";
            }
            


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetBuilding(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " a.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            //strSQL += " select * from TblIndustryGroup  ";

            strSQL += " select distinct a.* from SAPBuilding a  ";
                        
            if (comcode_ != "")
            {
                strSQL = strSQL + " where " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetRoomType()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct UsageTypeName from SAPRentalObject  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Getcontract_type(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            strSQL += " select distinct a.* from tblcontracttype a  " +
                        " inner join TblContractType_mapping b on b.ContractTypeId = a.id  " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetCategory_leasing(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            //strSQL += " select * from TblIndustryGroup  ";

            strSQL +=   " select distinct a.* from tblcategoryleasing a  " +
                        " inner join TblCategoryLeasing_mapping b on b.CategoryleasingId = a.id  " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Getgroup_location(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            strSQL += " select distinct a.* from tblgrouplocation a   " +
                        " inner join TblGroupLocation_mapping b on b.GroupLocationId = a.id  " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetIndustry_group(string[] comcode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            //strSQL += " select * from TblIndustryGroup  ";

            strSQL += " select distinct a.* from TblIndustryGroup a  " +
                        " inner join TblIndustryGroup_mapping b on b.IndustryGroupCode = a.IndustryGroupCode " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetMapCompany(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (userid != "")
            {
                strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

            }
            else
            {
                strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";

            }
            //strSQL += " select * from tblmapp_User_Company where userid = '" + userid + "';  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetCompany(string com_code)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblCompanyDetail where IsActive = 'y' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetCompany_ae(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct c.CompanyCode,c.CompanyNameEN from tblsmart_Contract s inner join TblCompanyDetail c on s.CompanyCode = c.CompanyCode  where smart_icon_ae_id in (" + userid + ")  order by c.CompanyNameEN  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }




    }
}