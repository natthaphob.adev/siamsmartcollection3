﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for edit_user
/// </summary>
public class edit_userDLL
{
    public edit_userDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getUserByUsername(string username, string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where username = '" + username + "'  and userid <> '" + userid + "' and flag_active = 'y' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByUsername_own(string username, string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where username = '" + username + "' and userid = '" + userid + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getUserByUserid(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where userid = '" + userid + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void UpdateUser(string Fname, string Lname, string role, string user_group, string username,
        string employee_code, string img_uri, string email, string tel, string mobile, string flag_active,
        string update_date, string update_id, string userid,string noshow_img)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tbluser set Fname = '" + Fname + "',Lname = '" + Lname + "',role = '" + role + "',user_group = '" + user_group + "', " +
            " username = '" + username + "', employee_code = '" + employee_code + "',img_uri = '" + img_uri + "' ,email = '" + email + "', " +
            " tel = '" + tel + "', mobile = '" + mobile + "', flag_active = '" + flag_active + "' , update_date = '" + update_date + "',update_id = '" + update_id + "', noshow_img = '" + noshow_img + "'	" +
            " Where userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UpdatePAss(string password, string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tbluser set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) " +
            " ,flag_reset_password = 'y' " +
            " Where userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }





}