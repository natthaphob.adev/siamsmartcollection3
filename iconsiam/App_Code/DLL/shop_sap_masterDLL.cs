﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for shop_sap_masterDLL
/// </summary>
public class shop_sap_masterDLL
{
    public shop_sap_masterDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable GetStaff(string user_group, string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select distinct u.* ,  concat(fname,' ',lname) as name  from tbluser u inner join tblmapp_User_Company m on u.userid = m.userid " +
            "  where user_group = '" + user_group + "' and role = 'officer' and  m.company_code in (" + companycode + ") ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetStaff_CC(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select *,  concat(fname,' ',lname) as name from TblUser  where userid in (  " +
                 " select userid from tblmapp_User_Company " +
                 " where ComGroupID in (select ComgroupID from TblCompanyGroupMapping " +
                 " where companycode in (" + companycode + "))) ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetStaff_office(string user_group)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * ,  concat(fname,' ',lname) as name  from tbluser  where user_group = '" + user_group + "' and role = 'officer' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetGroupLocation(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select distinct g.* from tblgrouplocation g inner join TblGroupLocation_mapping gm on g.id = gm.GroupLocationId " +
            " inner join TblCompanyGroupMapping cgm on gm.ComGroupID = cgm.ComGroupID " +
            " where Flag_active = '1' and cgm.CompanyCode in (" + CompanyCode + ") order by GrouplocationNameEN  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContract_type(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select distinct g.* from tblcontracttype g inner join TblContractType_mapping gm on g.id = gm.ContractTypeId " +
            " inner join TblCompanyGroupMapping cgm on gm.ComGroupID = cgm.ComGroupID " +
            " where Flag_active = '1' and cgm.CompanyCode in (" + CompanyCode + ") order by ContractTypeNameEn  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCategory_leasing(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select distinct g.* from tblcategoryleasing g inner join TblCategoryLeasing_mapping gm on g.id = gm.CategoryleasingId " +
            " inner join TblCompanyGroupMapping cgm on gm.ComGroupID = cgm.ComGroupID " +
            " where Flag_active = '1' and cgm.CompanyCode in (" + CompanyCode + ") order by CategoryleasingNameEN ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetContractDetail(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select com.CompanyNameTH, si.IndustryNameTH , si.IndustryNameEN,sc.* " +
                " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                " left join SAPIndustry si on sc.IndustryCode = si.IndustryCode " +
                " where sc.ContractNumber = '" + ContractNumber + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetRetalObj(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select distinct BuildingCode,BuildingName,FloorDescription, UsageTypeName " +
                " from SAPRentalObject where ContractNumber = '" + ContractNumber + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetIndustry_group()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblIndustryGroup where IsActive = '1' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetIndustry_groupByCode(string IndustryGroupCode, string comCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblIndustryGroup where IndustryGroupCode = '" + IndustryGroupCode + "' and IsActive = '1' and " +
            "  IndustryGroupCode in (select IndustryGroupCode from TblIndustryGroup_mapping " +
            " where ComGroupID in (select ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + comCode + "))) ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetIndustry_groupByID(string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblIndustryGroup where Id = '" + id + "' and IsActive = '1' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopGroup(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "    select distinct g.* from TblShopGroup g inner join TblShopGroup_mapping gm on g.id = gm.ShopGroupId  " +
            "             inner join TblCompanyGroupMapping cgm on gm.ComGroupID = cgm.ComGroupID " +
            "             where Flag_active = '1' and cgm.CompanyCode in (" + CompanyCode + ") order by ShopGroupNameTH  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetContactPoint(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select *, case when IsMain = 'y' then 'ผู้ดูแลหลัก' else 'ไม่ใช่ผู้ดูแลหลัก' end as main,  case when status = '1' then 'Active' else 'Inactive' end as  status_text  " +
                        " from tblcontact_point where ContractNumber = '" + ContractNumber + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContactPoint_distinct_mail(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select distinct email,IsMain,status  from tblcontact_point where ContractNumber = '" + ContractNumber + "' and IsMain = 'y' and  status = '1'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetAccount(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontract_account where ContractNumber = '" + ContractNumber + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetAccountByUsername(string username)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontract_account where username = '" + username + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByUsername(string username)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where username = '" + username + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContactPoint_Existing(string name, string tel1, string email, string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from  tblcontact_point where contractnumber = '" + contractnumber + "' and name = '" + name + "' and email = '" + email + "' and tel1 = '" + tel1 + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void Insert_contact_point(string contractnumber, string name, string email, string tel1, string tel2, string IsMain, string status)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tblcontact_point(contractnumber,name,email,tel1,tel2,IsMain,status) " +
                    " values('" + contractnumber + "','" + name + "','" + email + "','" + tel1 + "','" + tel2 + "','" + IsMain + "','" + status + "'); ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Insert_email_cc(string contract_number, string email, string name, string create_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Insert Into TblEmailCC_Contract(contract_number,email,name,create_date,create_id,update_date,update_id) " +
            " values('" + contract_number + "','" + email + "','" + name + "', getdate(),'" + create_id + "', getdate(),'" + create_id + "') ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void Update_contact_point(string name, string email, string tel1, string tel2, string IsMain, string status, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " update tblcontact_point set " +
             " name = '" + name + "',email = '" + email + "',tel1 = '" + tel1 + "',tel2 = '" + tel2 + "',IsMain = '" + IsMain + "',status = '" + status + "' " +
             " where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void delete_contact_point(string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " delete from tblcontact_point where id ='" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getContact_pointByID(string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " select * from tblcontact_point where id ='" + id + "'   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;

    }

    public DataTable GetContract_Expire(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblsmart_contract " +
            " where smart_shop_close < GETDATE() and smart_collection_flag_status <> 'y' and smart_contract_status = '1' " +
            " and  ContractNumber = '" + ContractNumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction_shopenddate(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select sl.id " +
                  " FROM [SiamSmartCollection].[dbo].[tbltransaction_record]	sl " +
                  " LEFT JOIN [SiamSmartCollection].[dbo].[tblsmart_Contract]	sh ON sl.contractnumber	= sh.contractnumber " +
                  " WHERE sl.Contractnumber	= '"+ ContractNumber + "' AND sl.Product_amount	= 0.00 " +
                  " AND sl.status = 'n' AND sl.[record_date]	not between sh.[smart_shop_open] and sh.[smart_shop_close] ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Del_Transaction_shopenddate(string trans_id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  delete tbltransaction_record where id in ("+trans_id+") ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UpdateContact_Master(string UpdateDate,
                                      string smart_room_no,
                                      string smart_sqm,
                                      string smart_update_date,
                                      string smart_update_user,
                                      string smart_isShop_group,
                                      string smart_shop_group_id,
                                      string smart_shop_open,
                                      string smart_shop_close,
                                      string smart_group_location,
                                      string smart_contract_type,
                                      string smart_Category_leasing,
                                      string smart_Industry_group,
                                      string smart_icon_staff_id,
                                      string smart_record_person_type,
                                      string smart_record_type,
                                      string smart_contract_status,
                                      string smart_record_keyin_type,
                                      string smart_object_rental_code,
                                      string smart_icon_ae_id, string dd, string tt, string ContractNumber,
                                      string smart_building, string smart_floor, string smart_usage_name, string create_user
                                      )
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (create_user != "")
        {
            strSQL += " update tblsmart_Contract  " +
             " set " +
             " UpdateDate = '" + UpdateDate + "', " +
             " smart_room_no = '" + smart_room_no + "',  " +
             " smart_sqm = '" + smart_sqm + "',  " +
             " smart_update_date = '" + smart_update_date + "',  " +
             " smart_update_user = '" + smart_update_user + "',  " +
             " smart_isShop_group = '" + smart_isShop_group + "',  " +
             " smart_shop_group_id = '" + smart_shop_group_id + "',  " +
             " smart_shop_open  = '" + smart_shop_open + "', " +
             " smart_shop_close = '" + smart_shop_close + "',  " +
             " smart_group_location = '" + smart_group_location + "',  " +
             " smart_contract_type  = '" + smart_contract_type + "', " +
             " smart_Category_leasing = '" + smart_Category_leasing + "',  " +
             " smart_Industry_group = '" + smart_Industry_group + "',  " +
             " smart_icon_staff_id = '" + smart_icon_staff_id + "',  " +
             " smart_record_person_type = '" + smart_record_person_type + "',  " +
             " smart_record_type = '" + smart_record_type + "',  " +
             " smart_contract_status = '" + smart_contract_status + "',  " +
             " smart_record_keyin_type  = '" + smart_record_keyin_type + "', " +
             " smart_object_rental_code  = '" + smart_object_rental_code + "', " +
             " smart_building  = '" + smart_building + "', " +
             " smart_usage_name  = '" + smart_usage_name + "', " +
             " smart_floor  = '" + smart_floor + "', " +
             " smart_icon_ae_id  = '" + smart_icon_ae_id + "' ,  ";
        }
        else
        {
            strSQL += " update tblsmart_Contract  " +
             " set " +
             " UpdateDate = '" + UpdateDate + "', " +
             " smart_room_no = '" + smart_room_no + "',  " +
             " smart_sqm = '" + smart_sqm + "',  " +
             " smart_update_date = '" + smart_update_date + "',  " +
             " smart_update_user = '" + smart_update_user + "',  " +
             " smart_isShop_group = '" + smart_isShop_group + "',  " +
             " smart_shop_group_id = '" + smart_shop_group_id + "',  " +
             " smart_shop_open  = '" + smart_shop_open + "', " +
             " smart_shop_close = '" + smart_shop_close + "',  " +
             " smart_group_location = '" + smart_group_location + "',  " +
             " smart_contract_type  = '" + smart_contract_type + "', " +
             " smart_Category_leasing = '" + smart_Category_leasing + "',  " +
             " smart_Industry_group = '" + smart_Industry_group + "',  " +
             " smart_icon_staff_id = '" + smart_icon_staff_id + "',  " +
             " smart_record_person_type = '" + smart_record_person_type + "',  " +
             " smart_record_type = '" + smart_record_type + "',  " +
             " smart_contract_status = '" + smart_contract_status + "',  " +
             " smart_record_keyin_type  = '" + smart_record_keyin_type + "', " +
             " smart_object_rental_code  = '" + smart_object_rental_code + "', " +
             " smart_building  = '" + smart_building + "', " +
             " smart_usage_name  = '" + smart_usage_name + "', " +
             " smart_floor  = '" + smart_floor + "', " +
             " smart_icon_ae_id  = '" + smart_icon_ae_id + "',  ";
        }






        if (smart_record_type == "Daily")
        {
            strSQL += " smart_record_daily_type = '" + dd + "', smart_record_daily_time = '" + tt + "' ";
            strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
            strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
        }
        else if (smart_record_type == "Monthly")
        {
            strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
            strSQL += ",  smart_record_monthly_month = '" + dd + "', smart_record_monthly_time = '" + tt + "' ";
            strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
        }
        else if (smart_record_type == "Weekly")
        {
            strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
            strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
            strSQL += ",  smart_record_weekly_date = '" + dd + "', smart_record_weekly_time = '" + tt + "' ";
        }
        strSQL += " where ContractNumber = '" + ContractNumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UpdateContact_Master_copy(string UpdateDate,
                                    string smart_room_no,
                                    string smart_sqm,
                                    string smart_update_date,
                                    string smart_update_user,
                                    string smart_isShop_group,
                                    string smart_shop_group_id,
                                    string smart_shop_open,
                                    string smart_shop_close,
                                    string smart_group_location,
                                    string smart_contract_type,
                                    string smart_Category_leasing,
                                    string smart_Industry_group,
                                    string smart_icon_staff_id,
                                    string smart_record_person_type,
                                    string smart_record_type,
                                    string smart_contract_status,
                                    string smart_record_keyin_type,
                                    string smart_object_rental_code,
                                    string smart_icon_ae_id, string dd, string tt, string ContractNumber, string Id,
                                     string smart_building, string smart_floor, string smart_usage_name)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " update tblsmart_Contract  " +
                " set " +
                " UpdateDate = '" + UpdateDate + "', " +
                " smart_room_no = '" + smart_room_no + "',  " +
                " smart_sqm = '" + smart_sqm + "',  " +
                " smart_update_date = '" + smart_update_date + "',  " +
                " smart_update_user = '" + smart_update_user + "',  " +
                " smart_isShop_group = '" + smart_isShop_group + "',  " +
                " smart_shop_group_id = '" + smart_shop_group_id + "',  " +
                " smart_shop_open  = ContractStartDate , " + //'" + smart_shop_open + "', " +
                " smart_shop_close = ContractEndDate , " + // + smart_shop_close + "',  " +
                " smart_group_location = '" + smart_group_location + "',  " +
                " smart_contract_type  = '" + smart_contract_type + "', " +
                " smart_Category_leasing = '" + smart_Category_leasing + "',  " +
                " smart_Industry_group = '" + smart_Industry_group + "',  " +
                " smart_icon_staff_id = '" + smart_icon_staff_id + "',  " +
                " smart_record_person_type = '" + smart_record_person_type + "',  " +
                " smart_record_type = '" + smart_record_type + "',  " +
                " smart_contract_status = '" + smart_contract_status + "',  " +
                " smart_record_keyin_type  = '" + smart_record_keyin_type + "', " +
                " smart_object_rental_code  = '" + smart_object_rental_code + "', " +
                " smart_building  = '" + smart_building + "', " +
                " smart_floor  = '" + smart_floor + "', " +
                " smart_usage_name  = '" + smart_usage_name + "', " +
                " smart_icon_ae_id  = '" + smart_icon_ae_id + "',  ";

        if (smart_record_type == "Daily")
        {
            strSQL += " smart_record_daily_type = '" + dd + "', smart_record_daily_time = '" + tt + "' ";
            strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
            strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
        }
        else if (smart_record_type == "Monthly")
        {
            strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
            strSQL += ",  smart_record_monthly_month = '" + dd + "', smart_record_monthly_time = '" + tt + "' ";
            strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
        }
        else if (smart_record_type == "Weekly")
        {
            strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
            strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
            strSQL += ",  smart_record_weekly_date = '" + dd + "', smart_record_weekly_time = '" + tt + "' ";
        }
        strSQL += " , Contract_old_number = '" + ContractNumber + "' where ContractNumber = '" + Id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void CreateAccount(string contractnumber, string username, string password, string createid, string create_date, string Expiry_date, string flag_sap, string status)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        if (password != ConfigurationManager.AppSettings["default_password"])
        {
            password = password.Replace("'", "''");
            strSQL = " Insert into tblcontract_account(contractnumber,username,password,createid,create_date, Expiry_date, flag_sap, flag_active) " +
                " values('" + contractnumber + "','" + username + "','" + password + "','" + createid + "', '" + create_date + "', '" + Expiry_date + "', '" + flag_sap + "', '" + status + "' ) ";

        }
        else
        {
            strSQL = " Insert into tblcontract_account(contractnumber,username,password,createid,create_date, Expiry_date, flag_sap, flag_active) " +
                    " values('" + contractnumber + "','" + username + "', ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ),'" + createid + "', '" + create_date + "', '" + Expiry_date + "', '" + flag_sap + "', '" + status + "' ) ";

        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



    public void Update_Account(string contractnumber_new, string Expiry_date, string contractnumber, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblcontract_account set " +
            " contractnumber = '" + contractnumber_new + "',  Expiry_date = '" + Expiry_date + "',update_date = getdate(), update_id = '" + update_id + "'  where contractnumber = '" + contractnumber + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Transaction(string contractnumber_new, string record_date, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " update tbltransaction_record set contractnumber = '" + contractnumber_new + "' where  record_date > '" + record_date + "' and contractnumber = '" + contractnumber + "' ;   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void delete_Transaction(string contractnumber_new, string record_date)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "delete tbltransaction_record where  contractnumber = '" + contractnumber_new + "' and   record_date >= '" + record_date + "'; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public DataTable Find_transaction_newContract(string contractnumber_new)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbltransaction_record where contractnumber = '" + contractnumber_new + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Update_status_Contract(string smart_contract_status, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = "  Update tblsmart_contract set smart_contract_status = '" + smart_contract_status + "'  where contractnumber = '" + contractnumber + "'    ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Account_Status(string flag_reset, string flag_active, string contractnumber, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Update tblcontract_account set flag_reset = '" + flag_reset + "' , flag_active = '" + flag_active + "',update_date = getdate(), update_id = '" + update_id + "' " +
            " where contractnumber = '" + contractnumber + "'    ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void Update_Contract_Point(string contractnumber_new, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblcontact_point set " +
            " contractnumber = '" + contractnumber_new + "'  where contractnumber = '" + contractnumber + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



    public DataTable GetSmart_Contract(string CompanyCode, string ShopName, string ContractNumber)
    {
        ShopName = ShopName.Replace("'", "''");

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                  " sc.BusinessPartnerName,case when sc.smart_icon_staff_id <> '' then 'Active' else 'Inactive' end as status,sc.smart_update_date " +
                  " ,u.username, " +
                 " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate " +
                  " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                  " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                  "    left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                  " where sc.ShopName like '%" + ShopName + "%'  and sc.CompanyCode = '" + CompanyCode + "' " +
                  " and sc.smart_icon_staff_id <> '' and sc.ContractNumber like '%" + ContractNumber + "%'  ";

        //if (CompanyCode != "")
        //{
        //    strSQL += " and sc.CompanyCode = '" + CompanyCode + "' ";
        //}



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";

        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany(string com_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    //public DataTable GetCompany()
    //{

    //    SqlConnection objConn = new SqlConnection();
    //    SqlCommand objCmd = new SqlCommand();
    //    SqlDataAdapter dtAdapter = new SqlDataAdapter();

    //    DataSet ds = new DataSet();
    //    DataTable dt = null;
    //    string strSQL = "";

    //    strSQL += " select * from SAPCompany where IsActive = '1' order by CompanyNameTH  ";

    //    objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
    //    var _with1 = objCmd;
    //    _with1.Connection = objConn;
    //    _with1.CommandText = strSQL;
    //    _with1.CommandType = CommandType.Text;
    //    dtAdapter.SelectCommand = objCmd;

    //    dtAdapter.Fill(ds);
    //    dt = ds.Tables[0];

    //    dtAdapter = null;
    //    objConn.Close();
    //    objConn = null;

    //    return dt;
    //}

    public void UpdatePassword(string password, string contractnumber, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , " +
            " flag_reset = 'y',update_date = getdate(), update_id = '" + update_id + "'  " +
            " where  flag_active = 'y' and contractnumber = '" + contractnumber + "'  ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public DataTable GetContractByNumber(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblsmart_contract where contractnumber = '" + ContractNumber + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetCompanyCodeByCompanyCode(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from SAPCompany where CompanyCode = '" + companycode + "'  and IsActive = '1' ";


        objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void InsertMailCC(string contract_number, string email, string name, string create_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Insert into TblEmailCC_Contract(contract_number,email,name,create_date,create_id,update_date,update_id) " +
            " values('" + contract_number + "','" + email + "','" + name + "', getdate(),'" + create_id + "', getdate(), '" + create_id + "') ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void DeleteMailCC(string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " delete TblEmailCC_Contract where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getEmailCC(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblEmailCC_Contract where contract_number = '" + ContractNumber + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }




}