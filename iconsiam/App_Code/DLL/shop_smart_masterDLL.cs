﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class shop_smart_masterDLL
    {

        public DataTable GetStaff(string user_group,string comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            //strSQL += " select * ,  concat(fname,' ',lname) as name  from tbluser  where user_group = '" + user_group + "' and role = 'officer' ";

            strSQL +=   " select distinct a.userid,concat(a.fname,' ',a.lname) as name from tbluser a  " +
                        " inner  join tblmapp_User_Company m on a.userid = m.userid    " +
                        " inner  join TblCompanyDetail s on m.company_code = s.Companycode  " +
                        " where a.user_group = '"+ user_group + "' and a.role = 'officer' and m.company_code in ("+ comcode + ")  "; 

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetGroupLocation(string comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL +=   " select distinct c.id,c.GrouplocationNameEN,a.ComGroupID from  TblGroupLocation_mapping a " +
                        " inner join TblCompanyGroupMapping b on b.ComGroupID = a.ComGroupID " +
                        " inner join tblgrouplocation c on c.id = a.GroupLocationId" +
                        " where b.CompanyCode in ("+  comcode +") and c.Flag_active = '1'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetMapCompany(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            if (userid != "")
            {
                strSQL += " select  distinct company_code from tblmapp_User_Company where userid in (" + userid + ")  ;  ";

            }
            else
            {
                strSQL += " select  distinct company_code from tblmapp_User_Company   ;  ";

            }


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetCompany(string com_code)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            //strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

            strSQL +=   " select distinct s.CompanyNameTh,s.Companycode  " +
                        " from tbluser u  " +
                        " inner  join tblmapp_User_Company m on u.userid = m.userid    " +
                        " inner  join TblCompanyDetail s on m.company_code = s.Companycode  " +
                        " where u.username like '%%'  and m.company_code in ("+ com_code + ") and u.user_group = 'ar'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransaction_shopenddate(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select sl.id " +
                      " FROM [SiamSmartCollection].[dbo].[tbltransaction_record]	sl " +
                      " LEFT JOIN [SiamSmartCollection].[dbo].[tblsmart_Contract]	sh ON sl.contractnumber	= sh.contractnumber " +
                      " WHERE sl.Contractnumber	= '" + ContractNumber + "' AND sl.Product_amount	= 0.00 " +
                      " AND sl.status = 'n' AND sl.[record_date]	not between sh.[smart_shop_open] and sh.[smart_shop_close] ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Del_Transaction_shopenddate(string trans_id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  delete tbltransaction_record where id in (" + trans_id + ") ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetIndustry_group(string comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL +=   " select distinct c.id,a.IndustryGroupCode,c.IndustryGroupNameEN,a.ComGroupID from TblIndustryGroup_mapping a " +
                        " inner join TblCompanyGroupMapping b on b.ComGroupID = a.ComGroupID " +
                        " inner join TblIndustryGroup c on c.IndustryGroupCode = a.IndustryGroupCode" +
                        " where b.CompanyCode in ("+ comcode +") and c.IsActive = '1' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetContract_type(string comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL +=   " select distinct c.id,c.ContractTypeNameEn,a.ComGroupID from  TblContractType_mapping a" +
                        " inner join TblCompanyGroupMapping b on b.ComGroupID = a.ComGroupID" +
                        " inner join tblcontracttype c on c.id = a.ContractTypeId" +
                        " where b.CompanyCode in ("+ comcode + ") and c.Flag_active = '1'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetCategory_leasing(string comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL +=   " select distinct c.id,c.CategoryleasingNameEN,a.ComGroupID from  TblCategoryLeasing_mapping a" +
                        " inner join TblCompanyGroupMapping b on b.ComGroupID = a.ComGroupID" +
                        " inner join tblcategoryleasing c on c.id = a.CategoryleasingId" +
                        " where b.CompanyCode in ("+ comcode +") and c.Flag_active = '1'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetShop_type_seq(string smart_object_rental_code)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select top(1) case when s.smart_shoptype_number is null  then 0 else s.smart_shoptype_number end as type_seq " +
                    " from tblsmart_Contract s where s.smart_object_rental_code = '" + smart_object_rental_code + "' " +
                    " order by s.smart_shoptype_number desc ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetContractnumber_now(string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblsmart_Contract where ContractNumber = '"+ contractnumber + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void InsertMailCC(string contract_number, string email, string name, string create_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblEmailCC_Contract(contract_number,email,name,create_date,create_id,update_date,update_id) " +
                " values('" + contract_number + "','" + email + "','" + name + "', getdate(),'" + create_id + "', getdate(), '" + create_id + "') ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable getEmailCC(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblEmailCC_Contract where contract_number = '" + ContractNumber + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void DeleteMailCC(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete TblEmailCC_Contract where id = '" + id + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetBuildingByCompanyCode(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from SAPBuilding b where b.CompanyCode  = '" + CompanyCode + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetFloorByBuildingCode(string BuildingCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from SAPFloor f where f.BuildingCode  = '" + BuildingCode + "' order by  f.FloorDescripition ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetShopGroup(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from  tblshopgroup  where flag_active = '1' and CompanyCode = '" + CompanyCode + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Insert_COntract(string ContractNumber, string smart_create_user, string smart_create_date, string smart_shoptype_number, string smart_object_rental_code)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into tblsmart_Contract(ContractNumber,smart_create_user,smart_create_date, smart_shoptype_number,smart_object_rental_code) " +
                    "  values('" + ContractNumber + "','" + smart_create_user + "','" + smart_create_date + "', '" + smart_shoptype_number + "','" + smart_object_rental_code + "')  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable getContact_pointByID(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " select * from tblcontact_point where id ='" + id + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;

        }


        public void delete_contact_point(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from tblcontact_point where id ='" + id + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetContactPoint(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select *, case when IsMain = 'y' then 'ผู้ดูแลหลัก' else 'ไม่ใช่ผู้ดูแลหลัก' end as main , case when status = '1' then 'Active' else 'Inactive' end as  status_text  " +
                            " from tblcontact_point where ContractNumber = '" + ContractNumber + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetContactPoint_distinct_mail(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct email,IsMain,status  from tblcontact_point where ContractNumber = '" + ContractNumber + "' and IsMain = 'y' and  status = '1'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Insert_contact_point(string contractnumber, string name, string email, string tel1, string tel2, string IsMain, string status)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into tblcontact_point(contractnumber,name,email,tel1,tel2,IsMain, status) " +
                        " values('" + contractnumber + "','" + name + "','" + email + "','" + tel1 + "','" + tel2 + "','" + IsMain + "' , '" + status + "'); ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void UpdateContact_Master(
                                        string CompanyCode,
                                        string ContractStartDate,
                                        string ContractEndDate,
                                        string Shopname,
                                        string BusinessPartnerCode,
                                        string BusinessPartnerName,
                                        string SalesTypeName,
                                        string smart_building,
                                        string smart_floor,

                                        string UpdateDate,

                                      string smart_room_no,
                                      string smart_sqm,
                                      string smart_update_date,
                                      string smart_update_user,
                                      string smart_isShop_group,
                                      string smart_shop_group_id,
                                      string smart_shop_open,
                                      string smart_shop_close,
                                      string smart_group_location,
                                      string smart_contract_type,
                                      string smart_Category_leasing,
                                      string smart_Industry_group,
                                      string smart_icon_staff_id,
                                      string smart_record_person_type,
                                      string smart_record_type,
                                      string smart_contract_status,
                                      string smart_record_keyin_type,
                                      string smart_object_rental_code,
                                      string smart_icon_ae_id, string dd, string tt, string ContractNumber)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " update tblsmart_Contract  " +
                    " set " +
                    " CompanyCode = '" + CompanyCode + "', " +
                    " ContractStartDate = '" + ContractStartDate + "', " +
                    " ContractEndDate = '" + ContractEndDate + "', " +
                    " Shopname = '" + Shopname + "', " +
                    " BusinessPartnerCode = '" + BusinessPartnerCode + "', " +
                    " BusinessPartnerName = '" + BusinessPartnerName + "', " +
                    " SalesTypeName = '" + SalesTypeName + "', " +
                    " smart_building = '" + smart_building + "', " +
                    " smart_floor = '" + smart_floor + "', " +

                    //" UpdateDate = '" + UpdateDate + "', " +
                    " smart_room_no = '" + smart_room_no + "',  " +
                    " smart_sqm = '" + smart_sqm + "',  " +
                    " smart_create_date = '" + smart_update_date + "',  " +
                    " smart_create_user = '" + smart_update_user + "',  " +
                    " smart_isShop_group = '" + smart_isShop_group + "',  " +
                    " smart_shop_group_id = '" + smart_shop_group_id + "',  " +
                    " smart_shop_open  = '" + smart_shop_open + "', " +
                    " smart_shop_close = '" + smart_shop_close + "',  " +
                    " smart_group_location = '" + smart_group_location + "',  " +
                    " smart_contract_type  = '" + smart_contract_type + "', " +
                    " smart_Category_leasing = '" + smart_Category_leasing + "',  " +
                    " smart_Industry_group = '" + smart_Industry_group + "',  " +
                    " smart_icon_staff_id = '" + smart_icon_staff_id + "',  " +
                    " smart_record_person_type = '" + smart_record_person_type + "',  " +
                    " smart_record_type = '" + smart_record_type + "',  " +
                    " smart_contract_status = '" + smart_contract_status + "',  " +
                    " smart_record_keyin_type  = '" + smart_record_keyin_type + "', " +
                    " smart_object_rental_code  = '" + smart_object_rental_code + "', " +
                    " smart_icon_ae_id  = '" + smart_icon_ae_id + "',  " +
                    " smart_collection_flag_status  = 'y',  ";

            if (smart_record_type == "Daily")
            {
                strSQL += " smart_record_daily_type = '" + dd + "', smart_record_daily_time = '" + tt + "' ";
                strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
                strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
            }
            else if (smart_record_type == "Monthly")
            {
                strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
                strSQL += ",  smart_record_monthly_month = '" + dd + "', smart_record_monthly_time = '" + tt + "' ";
                strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
            }
            else if (smart_record_type == "Weekly")
            {
                strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
                strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
                strSQL += ",  smart_record_weekly_date = '" + dd + "', smart_record_weekly_time = '" + tt + "' ";
            }
            strSQL += " where ContractNumber = '" + ContractNumber + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void UpdateContact_Master_exist(
                                       string CompanyCode,
                                       string ContractStartDate,
                                       string ContractEndDate,
                                       string Shopname,
                                       string BusinessPartnerCode,
                                       string BusinessPartnerName,
                                       string SalesTypeName,
                                       string smart_building,
                                       string smart_floor,

                                       string UpdateDate,

                                     string smart_room_no,
                                     string smart_sqm,
                                     string smart_update_date,
                                     string smart_update_user,
                                     string smart_isShop_group,
                                     string smart_shop_group_id,
                                     string smart_shop_open,
                                     string smart_shop_close,
                                     string smart_group_location,
                                     string smart_contract_type,
                                     string smart_Category_leasing,
                                     string smart_Industry_group,
                                     string smart_icon_staff_id,
                                     string smart_record_person_type,
                                     string smart_record_type,
                                     string smart_contract_status,
                                     string smart_record_keyin_type,
                                     string smart_object_rental_code,
                                     string smart_icon_ae_id, string dd, string tt, string ContractNumber)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " update tblsmart_Contract  " +
                    " set " +
                    " CompanyCode = '" + CompanyCode + "', " +
                    " ContractStartDate = '" + ContractStartDate + "', " +
                    " ContractEndDate = '" + ContractEndDate + "', " +
                    " Shopname = '" + Shopname + "', " +
                    " BusinessPartnerCode = '" + BusinessPartnerCode + "', " +
                    " BusinessPartnerName = '" + BusinessPartnerName + "', " +
                    " SalesTypeName = '" + SalesTypeName + "', " +
                    " smart_building = '" + smart_building + "', " +
                    " smart_floor = '" + smart_floor + "', " +

                    //" UpdateDate = '" + UpdateDate + "', " +
                    " smart_room_no = '" + smart_room_no + "',  " +
                    " smart_sqm = '" + smart_sqm + "',  " +
                    " smart_update_date = '" + smart_update_date + "',  " +
                    " smart_update_user = '" + smart_update_user + "',  " +
                    " smart_isShop_group = '" + smart_isShop_group + "',  " +
                    " smart_shop_group_id = '" + smart_shop_group_id + "',  " +
                    " smart_shop_open  = '" + smart_shop_open + "', " +
                    " smart_shop_close = '" + smart_shop_close + "',  " +
                    " smart_group_location = '" + smart_group_location + "',  " +
                    " smart_contract_type  = '" + smart_contract_type + "', " +
                    " smart_Category_leasing = '" + smart_Category_leasing + "',  " +
                    " smart_Industry_group = '" + smart_Industry_group + "',  " +
                    " smart_icon_staff_id = '" + smart_icon_staff_id + "',  " +
                    " smart_record_person_type = '" + smart_record_person_type + "',  " +
                    " smart_record_type = '" + smart_record_type + "',  " +
                    " smart_contract_status = '" + smart_contract_status + "',  " +
                    " smart_record_keyin_type  = '" + smart_record_keyin_type + "', " +
                    " smart_object_rental_code  = '" + smart_object_rental_code + "', " +
                    " smart_icon_ae_id  = '" + smart_icon_ae_id + "',  " +
                    " smart_collection_flag_status  = 'y',  ";

            if (smart_record_type == "Daily")
            {
                strSQL += " smart_record_daily_type = '" + dd + "', smart_record_daily_time = '" + tt + "' ";
                strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
                strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
            }
            else if (smart_record_type == "Monthly")
            {
                strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
                strSQL += ",  smart_record_monthly_month = '" + dd + "', smart_record_monthly_time = '" + tt + "' ";
                strSQL += ",  smart_record_weekly_date = '', smart_record_weekly_time = '' ";
            }
            else if (smart_record_type == "Weekly")
            {
                strSQL += " smart_record_daily_type = '', smart_record_daily_time = '' ";
                strSQL += ",  smart_record_monthly_month = '', smart_record_monthly_time = '' ";
                strSQL += ",  smart_record_weekly_date = '" + dd + "', smart_record_weekly_time = '" + tt + "' ";
            }
            strSQL += " where ContractNumber = '" + ContractNumber + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public DataTable GetAccount(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblcontract_account where ContractNumber = '" + ContractNumber + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetContractDetail(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select com.CompanyNameTH, si.IndustryNameTH, si.IndustryNameEN,sc.* " +
                    " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                    " left join SAPIndustry si on sc.IndustryCode = si.IndustryCode " +
                    " where sc.ContractNumber = '" + ContractNumber + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetAccountByUsername(string username)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblcontract_account where username = '" + username + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetUserByUsername(string username)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tbluser where username = '" + username + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public void CreateAccount(string contractnumber, string username, string password, string createid, string create_date, string Expiry_date, string flag_sap)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into tblcontract_account(contractnumber,username,password,createid,create_date, Expiry_date, flag_sap, flag_active) " +
                    " values('" + contractnumber + "','" + username + "', ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ), " +
                    " '" + createid + "', '" + create_date + "', '" + Expiry_date + "', '" + flag_sap + "', 'y' ) ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void Update_contact_point(string name, string email, string tel1, string tel2, string IsMain, string status, string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update tblcontact_point set " +
                 " name = '" + name + "',email = '" + email + "',tel1 = '" + tel1 + "',tel2 = '" + tel2 + "',IsMain = '" + IsMain + "',status = '" + status + "' " +
                 " where id = '" + id + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void Del_ContractSmartCollectio(string userid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from tblsmart_contract where ContractNumber != '' and CompanyCode is null and Shopname is null and smart_record_type is null and smart_create_user = '" + userid + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void Del_Contact_Contract(string ContractNumber)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from tblcontact_point where contractnumber = '" + ContractNumber + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetSmartContract(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select distinct ContractNumber from tblsmart_contract where ContractNumber != '' and CompanyCode is null and Shopname is null and smart_record_type is null and smart_create_user = '" + userid + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetContactPoint_Existing(string name, string tel1, string email, string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from  tblcontact_point where contractnumber = '" + contractnumber + "' and name = '" + name + "' and email = '" + email + "' and tel1 = '" + tel1 + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void UpdatePassword(string password, string contractnumber,string update_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = "  update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , " +
                " flag_reset = 'y',update_date = getdate(), update_id = '" + update_id + "' " +
                " where  flag_active = 'y' and contractnumber = '" + contractnumber + "'  ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetContractByNumber(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblsmart_contract where contractnumber = '" + ContractNumber + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable GetCompanyCodeByCompanyCode(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "select * from SAPCompany where CompanyCode = '" + companycode + "'  and IsActive = '1' ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }









    }
}