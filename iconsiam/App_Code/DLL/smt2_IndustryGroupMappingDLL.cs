﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class smt2_IndustryGroupMappingDLL
    {

        public DataTable getCompayGroup(string comCOde)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblCompanyGroup where ComGroupID in (select ComGroupID from TblCompanyGroupMapping where CompanyCode in ("+ comCOde + ")) " +
                " and  IsActive = 'y'; ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getCompanyGroupByID(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyGroup where ComGroupID = '" + ComGroupID + "' ; ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getIndustryGroup_mapping(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select a.*,case when ComGroupID is not null then 'y' else 'n' end chk " +
                " from(select IndustryGroupCode, IndustryGroupNameEN, IndustryGroupNameTH from TblIndustryGroup where IsActive = '1' " +
                " ) a left join(select ComGroupID, IndustryGroupCode from TblIndustryGroup_mapping where ComGroupID = '" + ComGroupID + "' ) " +
                " b on a.IndustryGroupCode = b.IndustryGroupCode  ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getIndustryGroup_Notmapping(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblIndustryGroup where IndustryGroupCode not in ( " +
                " select inGm.IndustryGroupCode from TblIndustryGroup_mapping inGm " +
                " inner join TblIndustryGroup sap on inGm.IndustryGroupCode = sap.IndustryGroupCode where ComGroupID = '" + ComGroupID + "' ) ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getIndustryGroupMapping(string ComGroupID, string IndustryGroupName)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select inM.id,sapIn.IndustryGroupNameEN,sapIn.IndustryGroupNameTH , inM.IsActive,inM.UpdateDate, u.username " +
                " from TblIndustryGroup_mapping inM inner join TblIndustryGroup sapIn on inM.IndustryGroupCode = sapIn.IndustryGroupCode " +
                " inner join tbluser u on inm.UpdateId = u.userid where inM.ComGroupID = '" + ComGroupID + "' " +
                " and (sapIn.IndustryGroupNameEN like '%" + IndustryGroupName + "%' or sapIn.IndustryGroupNameTH like '%" + IndustryGroupName + "%' )   ;  ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public void InsertIndustryGroupMapp(string IndustryGroupCode, string ComGroupID, string CreateId, string UpdateId, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert Into TblIndustryGroup_mapping(IndustryGroupCode,ComGroupID,CreateDate,UpdateDate,CreateId,UpdateId,IsActive) " +
                " values('" + IndustryGroupCode + "','" + ComGroupID + "',getdate(),getdate(),'" + CreateId + "','" + UpdateId + "','" + IsActive + "');   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void DeleteIndustryGroupMapp(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Delete from TblIndustryGroup_mapping where id = '" + id + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void DeleteIndustryGroupMappByCompanyID(string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Delete from TblIndustryGroup_mapping where ComGroupID = '" + ComGroupID + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }





    }
}