﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for user_acc_tenantDLL
/// </summary>
public class user_acc_tenantDLL
{
    public user_acc_tenantDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetUser_account(string[] name, string flag_active, string[] shopname, string contract , string comcode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string name_ = "";
        string shopname_ = "";


        if (name.Length != 0)
        {
            for (int i = 0; i < name.Length; i++)
            {
                name_ += "  c.username like '%" + name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            name_ = name_.Substring(0, name_.Length - 2);
        }

        if (shopname.Length != 0)
        {
            for (int i = 0; i < shopname.Length; i++)
            {
                shopname_ += "  s.ShopName like '%" + shopname[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            shopname_ = shopname_.Substring(0, shopname_.Length - 2);
        }




        if (flag_active != "")
        {
            if (shopname_ == "")
            {
                strSQL += " select c.id, c.contractnumber, c.username, c.Expiry_date,  c.flag_active ,case when c.flag_active = 'y' then 'Active' else 'Inactive' end as status,s.ShopName ,s.CompanyCode " +
                 " from tblcontract_account c inner join tblsmart_Contract s on c.contractnumber = s.ContractNumber where " + name_ + "  and c.flag_active = '" + flag_active + "' and c.contractnumber like '%" + contract + "%'   ";
            }
            else
            {
                strSQL += " select c.id, c.contractnumber, c.username, c.Expiry_date,  c.flag_active ,case when c.flag_active = 'y' then 'Active' else 'Inactive' end as status,s.ShopName ,s.CompanyCode " +
                 " from tblcontract_account c inner join tblsmart_Contract s on c.contractnumber = s.ContractNumber where " + name_ + "  and c.flag_active = '" + flag_active + "' and " + shopname_ + "  and c.contractnumber like '%" + contract + "%'  ";
            }

        }
        else
        {
            if (shopname_ == "")
            {
                strSQL += " select c.id, c.contractnumber, c.username, c.Expiry_date, c.flag_active, case when c.flag_active = 'y' then 'Active' else 'Inactive' end as status,s.ShopName ,s.CompanyCode " +
                 " from tblcontract_account c inner join tblsmart_Contract s on c.contractnumber = s.ContractNumber where " + name_ + "  and c.contractnumber like '%" + contract + "%'   ";
            }
            else
            {
                strSQL += " select c.id, c.contractnumber, c.username, c.Expiry_date, c.flag_active, case when c.flag_active = 'y' then 'Active' else 'Inactive' end as status,s.ShopName ,s.CompanyCode " +
                " from tblcontract_account c inner join tblsmart_Contract s on c.contractnumber = s.ContractNumber where " + name_ + "  and " + shopname_ + "  and c.contractnumber like '%" + contract + "%'  ";
            }

        }

        strSQL = strSQL + " and s.CompanyCode in ("+ comcode + ") ;  ";




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUser_accountExist(string username, string id_exist)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select c.id, c.contractnumber, c.username, c.Expiry_date,case when c.flag_active = 'y' then 'Active' else 'Inactive' end as status " +
           " from tblcontract_account c where c.username = '" + username + "' and c.id <> '" + id_exist + "' ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContactPoint(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select *, case when IsMain = 'y' then 'ผู้ดูแลหลัก' else 'ไม่ใช่ผู้ดูแลหลัก' end as main  " +
                        " from tblcontact_point where ContractNumber = '" + ContractNumber + "' and status = '1' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContractByNumber(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblsmart_contract where contractnumber = '" + ContractNumber + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompanyCodeByCompanyCode(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from SAPCompany where CompanyCode = '" + companycode + "'  and IsActive = '1' ";


        objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void UpdatePassword(string password, string id, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , " +
            " flag_reset = 'y',update_date = getdate(),update_id = '" + update_id + "' where id = '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void UpdateUsername(string username, string flag_active, string id, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tblcontract_account set username = '" + username + "' , flag_active = '" + flag_active + "',update_date = getdate(), update_id = '" + update_id + "'  where id = '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



}