﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for user_mgmDLL
/// </summary>
public class user_mgmDLL
{
    public user_mgmDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetUser(string[] name, string role, string flag_active,string company_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string name_ = "";


        if (name.Length != 0)
        {
            for (int i = 0; i < name.Length; i++)
            {
                name_ += " (concat(u.fname, ' ', u.lname)  like '%" + name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or u.username like '%" + name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            name_ = name_.Substring(0, name_.Length - 2);
        }

        if (role != "")
        {
            if (HttpContext.Current.Session["role"].ToString() == "super_admin" )
            {
                strSQL += "  select u.userid , u.username , concat(u.fname,' ',u.lname) as name ,u.mobile, " +
                   " u.Role, u.user_group,u.update_date, case when u.flag_active = 'y' then 'Active' else 'Inactive' end as status  " +
                   " from tbluser u where " + name_ + " " +
                   " and u.role like '" + role + "' and u.flag_active = '" + flag_active + "' ";
              
            }
            else
            {
                strSQL += "  select u.userid , u.username , concat(u.fname,' ',u.lname) as name ,u.mobile, " +
                   " u.Role, u.user_group,u.update_date, case when u.flag_active = 'y' then 'Active' else 'Inactive' end as status  " +
                   " from tbluser u where " + name_ + " " +
                   " and u.role like '" + role + "' and u.flag_active = '" + flag_active + "' ";

                if (company_code != "")
                {
                    strSQL += " and u.userid in (select userid from tblmapp_User_Company where company_code in (" + company_code + "))  ";

                }
            }
            
        }
        else
        {
            if (HttpContext.Current.Session["role"].ToString() == "super_admin" )
            {
                strSQL += "  select u.userid , u.username , concat(u.fname,' ',u.lname) as name ,u.mobile, " +
                  " u.Role, u.user_group,u.update_date, case when u.flag_active = 'y' then 'Active' else 'Inactive' end as status  " +
                  " from tbluser u where " + name_ + " " +
                  " and u.flag_active = '" + flag_active + "'  ";
               
            }
            else
            {
                strSQL += "  select u.userid , u.username , concat(u.fname,' ',u.lname) as name ,u.mobile, " +
                  " u.Role, u.user_group,u.update_date, case when u.flag_active = 'y' then 'Active' else 'Inactive' end as status  " +
                  " from tbluser u where " + name_ + " " +
                  " and u.flag_active = '" + flag_active + "'  ";

                if (company_code != "")
                {
                    strSQL += " and u.userid in (select userid from tblmapp_User_Company where company_code in (" + company_code + "))  ";

                }
            }
               
        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
}