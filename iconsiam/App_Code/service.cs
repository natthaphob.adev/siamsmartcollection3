﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data;
/// <summary>
/// Summary description for service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class service : System.Web.Services.WebService
{
    webserv_UserDLL query = new webserv_UserDLL();
    public service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class userinfo
    {
		public string contractnumber { get; set; }
        public string username { get; set; }
        public string smart_record_keyin_type { get; set; }
		public string Shopname { get; set; }
		public string Floor { get; set; }
		public string returncode { get; set; }
        public string desc { get; set; }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetUserByUsernamePassword(string username, string password)
    {
        List<userinfo> user_info = new List<userinfo>();

        userinfo x = new userinfo();
        //x.desc = X2;
        //x.returncode = name;

        //xx.Add(x);

        var user = query.GetUserByUserPass(username, password);
        if (user.Rows.Count != 0)
        {
           

            var contract = query.GetContractDetail(user.Rows[0]["contractnumber"].ToString());
            if (contract.Rows.Count != 0)
            {
				var Floor = query.GetFloorfromContractNumber(user.Rows[0]["contractnumber"].ToString());
				if (Floor.Rows.Count != 0)
				{
					x.returncode = "1000";
					x.desc = "success";
					x.contractnumber = user.Rows[0]["contractnumber"].ToString();
					x.smart_record_keyin_type = contract.Rows[0]["smart_record_keyin_type"].ToString();
					x.Shopname = contract.Rows[0]["ShopName"].ToString();
					x.username = user.Rows[0]["username"].ToString();
					x.Floor = Floor.Rows[0]["FloorDescription"].ToString();
					user_info.Add(x);
				}
				else
				{
					x.returncode = "3800";
					x.desc = "fail";
					x.contractnumber = "";
					x.smart_record_keyin_type = "";
					x.username = "";
					x.Shopname = "";
					x.Floor = "";
					user_info.Add(x);
				}
               
            }
            else
            {
				x.returncode = "3800";
				x.desc = "fail";
				x.contractnumber = "";
				x.smart_record_keyin_type = "";
				x.username = "";
				x.Shopname = "";
				x.Floor = "";
				user_info.Add(x);
			}
        }
        else
        {
			x.returncode = "3800";
			x.desc = "fail";
			x.contractnumber = "";
			x.smart_record_keyin_type = "";
			x.username = "";
			x.Shopname = "";
			x.Floor = "";
			user_info.Add(x);
		}


        JavaScriptSerializer js = new JavaScriptSerializer();
        Context.Response.Write(js.Serialize(user_info));
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetUserByEmail(string Email)
    {
        List<userinfo> user_info = new List<userinfo>();

        userinfo x = new userinfo();

        var user = query.GetUserByEmail(Email);
        if (user.Rows.Count != 0) //check email
        {
			var contract = query.GetContractDetail(user.Rows[0]["contractnumber"].ToString());
			if (contract.Rows.Count != 0) //check contractnumber
			{
				var username = query.GetUserByContractnumber(user.Rows[0]["contractnumber"].ToString());
				if (username.Rows.Count != 0) //check username password
				{
					var Floor = query.GetFloorfromContractNumber(user.Rows[0]["contractnumber"].ToString());
					if (Floor.Rows.Count != 0) //getfloor
					{
						x.username = username.Rows[0]["username"].ToString();
						x.contractnumber = user.Rows[0]["contractnumber"].ToString();
						x.smart_record_keyin_type = contract.Rows[0]["smart_record_keyin_type"].ToString();
						x.Shopname = contract.Rows[0]["ShopName"].ToString();
						x.Floor = Floor.Rows[0]["FloorDescription"].ToString();
						x.returncode = "1000";
						x.desc = "success";
						user_info.Add(x);
					}
					else
					{
						x.returncode = "3800";
						x.desc = "fail";
						x.contractnumber = "";
						x.smart_record_keyin_type = "";
						x.username = "";
						x.Shopname = "";
						x.Floor = "";
						user_info.Add(x);
					}
				}
				else
				{
					x.returncode = "3800";
					x.desc = "fail";
					x.contractnumber = "";
					x.smart_record_keyin_type = "";
					x.username = "";
					x.Shopname = "";
					x.Floor = "";
					user_info.Add(x);
				}
			}
			else
			{
				x.returncode = "3800";
				x.desc = "fail";
				x.contractnumber = "";
				x.smart_record_keyin_type = "";
				x.username = "";
				x.Shopname = "";
				x.Floor = "";
				user_info.Add(x);
			}
        }
		else
		{
			x.returncode = "3800";
			x.desc = "fail";
			x.contractnumber = "";
			x.smart_record_keyin_type = "";
			x.username = "";
			x.Shopname = "";
			x.Floor = "";
			user_info.Add(x);
		}
        JavaScriptSerializer js = new JavaScriptSerializer();
        Context.Response.Write(js.Serialize(user_info));
    }


}
