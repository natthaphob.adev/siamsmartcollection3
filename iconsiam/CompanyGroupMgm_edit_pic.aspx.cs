﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;
using System.Text;
using System.Configuration;
using System.IO;

namespace iconsiam
{
    public partial class CompanyGroupMgm_edit_pic : System.Web.UI.Page
    {
        protected string MyPic { get; set; }
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                     bind_data();
                    bind_defult();
                }
            }
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["comgroupid"]))
            {
                var com = Serv.getComGroupNameByID(Request.QueryString["comgroupid"].ToString());
                if (com.Rows.Count != 0)
                {
                    if (com.Rows[0]["CompanyGroupLogo"].ToString() != "")
                    {
                        this.imgCompany.ImageUrl = com.Rows[0]["CompanyGroupLogo"].ToString();
                    }
                    txtCompanyGroupName.Text = com.Rows[0]["ComGroupName"].ToString();
                    ddlTheme.SelectedValue = com.Rows[0]["ThemeId"].ToString();

                    string pathpic = getPicTheme(com.Rows[0]["ThemeId"].ToString());
                    Image1.ImageUrl = pathpic;
                }
                else
                {
                    Response.Redirect("~/CompanyGroupMgm.aspx");
                }
            }
            else
            {
                Response.Redirect("~/CompanyGroupMgm.aspx");
            }
        }

        protected void bind_defult()
        {

            var theme = Serv.getTheme();
            if (theme.Rows.Count != 0)
            {
                ddlTheme.DataTextField = "ThemeName";
                ddlTheme.DataValueField = "ThemeId";
                ddlTheme.DataSource = theme;
                ddlTheme.DataBind();
            }
            else
            {
                ddlTheme.DataSource = null;
                ddlTheme.DataBind();

            }
            ddlTheme.Items.Insert(0, new ListItem("Theme", ""));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            if (fileupload_logo.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_logo.FileName);
                FileInfo fi1 = new FileInfo(fileupload_logo.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    string img_name1 = ConfigurationManager.AppSettings["imgLogo"] + Request.QueryString["comgroupid"].ToString() + "_" + xx;
                    fileupload_logo.SaveAs(Server.MapPath(img_name1));

                    Serv.UpdateCompanyDetail_PicComgroup(ddlTheme.SelectedValue,img_name1, HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["comgroupid"].ToString());

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = 'CompanyGroupMgm.aspx' ;", true);

                }
                else
                {
                    POPUPMSG("Logo ต้องเป็นภาพเท่านั้น");
                    return;
                }

            }
            else
            {
                Serv.UpdateCompanyDetail(ddlTheme.SelectedValue, HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["comgroupid"].ToString());

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = 'CompanyGroupMgm.aspx' ;", true);
            }
                
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyGroupMgm.aspx");

        }

        protected void ddlTheme_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string themeid = ddlTheme.SelectedValue;
            Image1.ImageUrl = getPicTheme(themeid);

        }

        protected string getPicTheme(string themeid)
        {
            string Urlpic = "";
            var pic = Serv.getTheme();
            if (pic.Rows.Count != 0)
            {
                if (themeid == "1")
                {
                    Urlpic = pic.Rows[0]["Urlpictheme"].ToString();
                }
                else if (themeid == "2")
                {
                    Urlpic = pic.Rows[1]["Urlpictheme"].ToString();
                }
                else if (themeid == "3")
                {
                    Urlpic = pic.Rows[2]["Urlpictheme"].ToString();
                }
                else if (themeid == "4")
                {
                    Urlpic = pic.Rows[3]["Urlpictheme"].ToString();
                }
                else if (themeid == "5")
                {
                    Urlpic = pic.Rows[4]["Urlpictheme"].ToString();
                }
                else if (themeid == "6")
                {
                    Urlpic = pic.Rows[5]["Urlpictheme"].ToString();
                }
                else if (themeid == "7")
                {
                    Urlpic = pic.Rows[6]["Urlpictheme"].ToString();
                }
                else if (themeid == "8")
                {
                    Urlpic = pic.Rows[7]["Urlpictheme"].ToString();
                }
                else if (themeid == "9")
                {
                    Urlpic = pic.Rows[8]["Urlpictheme"].ToString();
                }
                else if (themeid == "10")
                {
                    Urlpic = pic.Rows[9]["Urlpictheme"].ToString();
                }
                else if (themeid == "11")
                {
                    Urlpic = pic.Rows[10]["Urlpictheme"].ToString();
                }
                else if (themeid == "12")
                {
                    Urlpic = pic.Rows[11]["Urlpictheme"].ToString();
                }
                else if (themeid == "13")
                {
                    Urlpic = pic.Rows[12]["Urlpictheme"].ToString();
                }
                else if (themeid == "14")
                {
                    Urlpic = pic.Rows[13]["Urlpictheme"].ToString();
                }
                else
                {
                    Urlpic = "~/img/Pic_color/Defult.jpg";
                }
            }

            return Urlpic;

        }

    }
}