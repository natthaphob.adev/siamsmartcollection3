﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for add_mappingDLL
/// </summary>
public class add_mappingDLL
{
    public add_mappingDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetUser(string roles)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbluser where  flag_active = 'y' and role = '" + roles + "' ;  ";
        //strSQL += " select * from tbluser where user_group = 'ar' and flag_active = 'y' and role = 'officer';  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetCompanyGroupMapp(string ComGroupID)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from TblCompanyGroupMapping where ComGroupID = '" + ComGroupID + "' ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetCompany(string userid,string comcode)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
      
        strSQL += " select s.CompanyCode , s.CompanyNameTH " +
                    " from TblCompanyDetail s " +
                    " where s.IsActive = 'y' and Companycode in ("+ comcode + ")  and s.CompanyCode not in( " +
                    " select m.company_code from tblmapp_User_Company m where m.userid = '" + userid + "' " +
                    " ) order by  s.CompanyNameTH  ";

        //strSQL += " select s.ComGroupID , s.ComGroupName " +
        //    " from TblCompanyGroup s where s.IsActive = 'y' and " +
        //    " s.ComGroupID not in( select m.ComGroupID from tblmapp_User_Company m where m.userid = '" + userid + "' ) order by  s.ComGroupName  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany_Group(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblCompanyGroup where ComGroupID in ( " +
            " select distinct ComGroupID from TblCompanyGroupMapping " +
            " where CompanyCode not in (select company_code from tblmapp_User_Company where userid = '" + userid + "')) and IsActive = 'y' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void InsertMapping(string userid, string company_code)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Insert into tblmapp_User_Company(userid,company_code,create_date,update_date) values('" + userid + "','" + company_code + "',GETDATE(),GETDATE()) ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }




}