﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for add_userDLL
/// </summary>
public class add_userDLL
{
    public add_userDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getUserByUsername(string username)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where username = '" + username + "' and flag_active = 'y' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getUser_tenantByUsername(string username)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontract_account where username = '" + username + "' and flag_active = 'y' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getCompanyAdmin(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblmapp_User_Company where userid = '" + userid + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getCompanyALL()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyDetail where IsActive = 'y';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable InsertUser(string Fname, string Lname, string role, string user_group, string username, string password,
        string employee_code, string img_uri, string email, string tel, string mobile, string flag_active, string create_date,
        string noshow_img, string create_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Insert into tbluser(Fname,Lname,role,user_group,username,password,employee_code,img_uri,email,tel,mobile,flag_active " +
                    " ,create_date,noshow_img,create_id) " +
                    " values('" + Fname + "','" + Lname + "','" + role + "','" + user_group + "','" + username + "',ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ), " +
                    " '" + employee_code + "','" + img_uri + "','" + email + "','" + tel + "','" + mobile + "','" + flag_active + "'	" +
                    " , '" + create_date + "' ,'" + noshow_img + "','" + create_id + "') ; SELECT SCOPE_IDENTITY()  as lastid ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;

    }


    public void InsertMappCom(string userid, string company_code)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  insert into tblmapp_User_Company(userid,create_date,update_date,company_code) " +
                " values('" + userid + "', getdate(), getdate(),'" + company_code + "')  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable GetCompany_Group()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyGroup where IsActive = 'y' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getComcode_byComGroup(string comgroup)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyGroupMapping a where a.ComGroupID = '" + comgroup + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getComcode_byUser(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblmapp_User_Company where userid = '"+userid+"' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }



}