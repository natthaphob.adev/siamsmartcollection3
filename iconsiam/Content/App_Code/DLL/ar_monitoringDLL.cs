﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ar_monitoringDLL
/// </summary>
public class ar_monitoringDLL
{
    public ar_monitoringDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetContract(string smart_icon_staff_id, string[] BusinessPartnerName, string[] ShopName,
        string smart_room_no, string CompanyCode, string d1)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string bpname = "";
        string shop_name = "";


        if (BusinessPartnerName.Length != 0)
        {
            for (int i = 0; i < BusinessPartnerName.Length; i++)
            {
                bpname += " s.BusinessPartnerName like '%" + BusinessPartnerName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            bpname = bpname.Substring(0, bpname.Length - 2);
        }


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shop_name += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            shop_name = shop_name.Substring(0, shop_name.Length - 2);
        }




        if (d1 != "")
        {
            if (CompanyCode != "")
            {
                strSQL += " select t.record_date, t.product_amount, t.service_serCharge_amount, case when t.flag_adjust = 'y' then 'ขอแก้ไข' when t.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status, " +
                    " s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no , s.ContractNumber, " +
                      " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type " +
                      " from  tbltransaction_record t inner join tblsmart_Contract s on t.ContractNumber =  s.ContractNumber inner " +
                      " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                      " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                      " and  (" + bpname + ") and (" + shop_name + ") " +
                      " and s.smart_room_no like '%" + smart_room_no + "%' and s.CompanyCode = '" + CompanyCode + "' " +
                      " and t.record_date between '" + d1 + " 00:00:00' and '" + d1 + " 23:59:59'  " +
                      " order by s.shopname ";
            }
            else
            {
                strSQL += " select  t.record_date, t.product_amount, t.service_serCharge_amount, case when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' when t.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status, " +
                    " s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.ContractNumber, " +
                      " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type " +
                      " from  tbltransaction_record t inner join tblsmart_Contract s on t.ContractNumber =  s.ContractNumber inner " +
                      " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                      " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                      " and (" + bpname + ") and (" + shop_name + ") " +
                      " and s.smart_room_no like '%" + smart_room_no + "%'  " +
                      " and t.record_date between '" + d1 + " 00:00:00' and '" + d1 + " 23:59:59' " +
                      " order by s.shopname ";
            }
        }
        else
        {
            if (CompanyCode != "")
            {
                strSQL += " select t.record_date, t.product_amount, t.service_serCharge_amount, case when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' when t.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status, " +
                    " s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no , s.ContractNumber, " +
                      " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type " +
                      " from  tbltransaction_record t inner join tblsmart_Contract s on t.ContractNumber =  s.ContractNumber inner " +
                      " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                      " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                      " and  (" + bpname + ") and (" + shop_name + ") " +
                      " and s.smart_room_no like '%" + smart_room_no + "%' and s.CompanyCode = '" + CompanyCode + "'  " +
                      //" and t.record_date between '" + d1 + " 00:00:00' and '" + d1 + " 23:59:59' " +
                      " order by s.shopname ";
            }
            else
            {
                strSQL += " select  t.record_date, t.product_amount, t.service_serCharge_amount, case when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' when t.status = 'y' then 'ส่งยอดแล้ว' else 'ยังไม่ส่งยอด' end as status, " +
                    " s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.ContractNumber, " +
                      " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type " +
                      " from  tbltransaction_record t inner join tblsmart_Contract s on t.ContractNumber =  s.ContractNumber inner " +
                      " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                      " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                      " and (" + bpname + ") and (" + shop_name + ") " +
                      " and s.smart_room_no like '%" + smart_room_no + "%'  " +
                      //" and t.record_date between '" + d1 + " 00:00:00' and '" + d1 + " 23:59:59' " +
                      " order by s.shopname ";
            }
        }




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetContract_ar(string smart_icon_staff_id, string[] BusinessPartnerName, string[] ShopName,
      string smart_room_no, string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string bpname = "";
        string shop_name = "";


        if (BusinessPartnerName.Length != 0)
        {
            for (int i = 0; i < BusinessPartnerName.Length; i++)
            {
                bpname += " s.BusinessPartnerName like '%" + BusinessPartnerName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            bpname = bpname.Substring(0, bpname.Length - 2);
        }


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shop_name += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  or";
            }

            shop_name = shop_name.Substring(0, shop_name.Length - 2);
        }


        if (CompanyCode != "")
        {
            strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no , s.ContractNumber, " +
                  " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type , u.username " +
                  " from  tblsmart_Contract s inner " +
                  " join SAPCompany c on s.CompanyCode = c.CompanyCode " +

                  " inner join tbluser u  on s.smart_icon_staff_id = u.userid  " +



                  " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                  " and  (" + bpname + ") and (" + shop_name + ") " +
                  " and s.smart_room_no like '%" + smart_room_no + "%' and s.CompanyCode = '" + CompanyCode + "'  and s.smart_record_person_type = 'ar' and  s.smart_contract_status = '1'  " +
                  " order by s.shopname ";
        }
        else
        {
            strSQL += " select  s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.ContractNumber, " +
                  " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, s.smart_record_keyin_type , u.username " +
                  " from tblsmart_Contract s inner join SAPCompany c on s.CompanyCode = c.CompanyCode " +

                  " inner join tbluser u  on s.smart_icon_staff_id = u.userid  " +



                  " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
                  " and (" + bpname + ") and (" + shop_name + ") " +
                  " and s.smart_room_no like '%" + smart_room_no + "%'  and s.smart_record_person_type = 'ar' and  s.smart_contract_status = '1'  " +
                  " order by s.shopname ";
        }



        //if (d1 != "")
        //{
        //    if (CompanyCode != "")
        //    {
        //        strSQL += " select  " +
        //            " s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no , s.ContractNumber, " +
        //              " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type , u.username " + 

        //              " from  tblsmart_Contract s  inner  join SAPCompany c on s.CompanyCode = c.CompanyCode " +

        //              " inner join tbluser u  on s.smart_icon_staff_id = u.userid  " + 

        //              " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
        //              " and  (" + bpname + ") and (" + shop_name + ") " +
        //              " and s.smart_room_no like '%" + smart_room_no + "%' and s.CompanyCode = '" + CompanyCode + "' " +
        //              " and s.smart_record_person_type = 'ar' " +
        //              " order by s.shopname ";
        //    }
        //    else
        //    {
        //        strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.ContractNumber, " +
        //              " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type , u.username " +
        //              " from  tblsmart_Contract s inner join SAPCompany c on s.CompanyCode = c.CompanyCode " +

        //              " inner join tbluser u  on s.smart_icon_staff_id = u.userid  " +



        //              " where s.smart_icon_staff_id in (" + smart_icon_staff_id + ") " +
        //              " and (" + bpname + ") and (" + shop_name + ") " +
        //              " and s.smart_room_no like '%" + smart_room_no + "%'  " +
        //              " and s.smart_record_person_type = 'ar'  " +
        //              " order by s.shopname ";
        //    }
        //}
        //else
        //{

        //}




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany(string com_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";

        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }



}