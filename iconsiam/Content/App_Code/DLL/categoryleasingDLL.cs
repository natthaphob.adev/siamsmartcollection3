﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for categoryleasingDLL
/// </summary>
public class categoryleasingDLL
{
    public categoryleasingDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getCategoryLeasingByName(string[] categoryleasingName, string Flag_active, string company_code, string role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string categoryleasingName_ = "";



        if (categoryleasingName.Length != 0)
        {
            for (int i = 0; i < categoryleasingName.Length; i++)
            {
                categoryleasingName_ += " (c.CategoryleasingNameEN like '%" + categoryleasingName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                    " or c.CategoryleasingNameTH like '%" + categoryleasingName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' ) or";
            }

            categoryleasingName_ = categoryleasingName_.Substring(0, categoryleasingName_.Length - 2);
        }


        if (role != "datacenter" && role != "super_admin")
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
                " from tblcategoryleasing c inner join tbluser u on c.createid = u.userid where " +
                " " + categoryleasingName_ + " " +
                " and c.Flag_active = '" + Flag_active + "'" +
                " and c.id in(select CategoryleasingId from TblCategoryLeasing_mapping " +
                " where ComGroupID in (select distinct ComGroupID from TblCompanyGroupMapping " +
                " where CompanyCode in (" + company_code + ")))   order by id ;   ";

        }
        else
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
                " from tblcategoryleasing c inner join tbluser u on c.createid = u.userid where " +
                " " + categoryleasingName_ + " " +
                " and c.Flag_active = '" + Flag_active + "'  order by id ;   ";

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void InsertCategoryLeasing(string CategoryleasingNameTH, string CategoryleasingNameEN, string Flag_active, string CreateId)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tblcategoryleasing(CategoryleasingNameTH,CategoryleasingNameEN,Flag_active,CreateDate,CreateId) " +
                " values('" + CategoryleasingNameTH + "','" + CategoryleasingNameEN + "','" + Flag_active + "', GETDATE(), '" + CreateId + "');   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getcategoryleasingById(string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcategoryleasing where Id = '" + Id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getcategoryleasingByNameEN(string CategoryleasingNameEN)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcategoryleasing where CategoryleasingNameEN = '" + CategoryleasingNameEN + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getcategoryleasingByNameTH(string CategoryleasingNameTH)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcategoryleasing where CategoryleasingNameTH = '" + CategoryleasingNameTH + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Updatecategoryleasing(string CategoryleasingNameTH, string CategoryleasingNameEN, string Flag_active, string CreateId, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblcategoryleasing set CategoryleasingNameTH = '" + CategoryleasingNameTH + "', CategoryleasingNameEN = '" + CategoryleasingNameEN + "', " +
            " Flag_active = '" + Flag_active + "' , UpdateDate = GETDATE() , UpdateId = '" + CreateId + "' where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getcategoryleasingByNameEN_nid(string CategoryleasingNameEN, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcategoryleasing where CategoryleasingNameEN = '" + CategoryleasingNameEN + "'  and id != '" + id + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getcategoryleasingByNameTH_nid(string CategoryleasingNameTH, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcategoryleasing where CategoryleasingNameTH = '" + CategoryleasingNameTH + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


}