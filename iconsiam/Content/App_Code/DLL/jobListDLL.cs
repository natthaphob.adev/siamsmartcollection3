﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class jobListDLL
    {
        public DataTable getCompany(string search, string[] comcode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ");";
            }

            strSQL += " select * from TblCompanyDetail where (CompanyNameTh like '%" + search + "%' or CompanyNameEn like '%" + search + "%' )  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }




        public DataTable getJobByName(string job_name, string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "   select top(1) * from tbljob where job_name = '" + job_name + "' and CompanyCode = '" + CompanyCode + "'  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
    }
}