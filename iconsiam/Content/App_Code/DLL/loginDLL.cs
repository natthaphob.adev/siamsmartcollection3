﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for loginDLL
/// </summary>
public class loginDLL
{
    public loginDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getLogo()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyGroup a where a.IsActive = 'y' and a.CompanyGroupLogo <> '' and a.CompanyGroupLogo IS NOT NULL   ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getUser(string username, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where username = '" + username + "' and DecryptByPassPhrase('spwgsmart',password )  = '" + password + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable Get_Confirm_new(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbljob where job_name = 'jobconfirm' and CompanyCode = '" + CompanyCode + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable Get_Confirm_new2(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbljob where job_name = 'jobconfirm2' and CompanyCode = '" + CompanyCode + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getUser_tenant(string username, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontract_account where username = '" + username + "' " +
            " and DecryptByPassPhrase('spwgsmart',password )  = '" + password + "'  and flag_active = 'y'    ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getThemeUser_tenant(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select  b.ThemeId,b.ComGroupID,c.ButtonColor,c.NavbarColor,c.BgUri,c.BgUri2,c.BgUri3,c.BgUri4,c.BgUri5,c.NavbarColor2,b.CompanyGroupLogo from TblCompanyGroupMapping a " +
                    "  inner join TblCompanyGroup b on b.ComGroupID = a.ComGroupID " +
                    "  inner join TblTheme c on c.ThemeId = b.ThemeId " +
                    "  where a.CompanyCode = '" + companycode + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getThemeUser_tenant_mobile(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select  b.ThemeId,b.ComGroupID,c.ButtonColor,c.NavbarColor,c.BgUri,c.BgUri2,c.BgUri3,c.BgUri4,c.BgUri5,c.NavbarColor2,b.CompanyGroupLogo from TblCompanyGroupMapping a " +
                    "  inner join TblCompanyGroup b on b.ComGroupID = a.ComGroupID " +
                    "  inner join TblTheme c on c.ThemeId = b.ThemeId " +
                    "  where a.CompanyCode =  @companycode  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@companycode", companycode);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getComcode_by_ContractNumber(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select CompanyCode from tblsmart_Contract where ContractNumber = '" + ContractNumber + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getComcode_by_ContractNumber_mobile(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select CompanyCode from tblsmart_Contract where ContractNumber =  @ContractNumber  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getCompany_name_tenant(string Companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select a.Companycode,a.CompanyNameTh from TblCompanyDetail a  " +
                    " inner join tblsmart_Contract b on b.CompanyCode = a.Companycode  " +
                    " where b.ContractNumber = '" + Companycode + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void UpdatePassword(string userid, string password, string usertype, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        if (usertype == "icon")
        {
            strSQL = " update tbluser set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , flag_reset_password = 'n' where userid = '" + userid + "' ";

        }
        else
        {
            strSQL = " update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ),  flag_reset = 'n',update_date = getdate(), update_id = '" + update_id + "'  where id = '" + userid + "' ";

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Del_ContractSmartCollectio(string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " delete from tblsmart_contract where ContractNumber != '' and CompanyCode is null and Shopname is null  and smart_record_type is null  and smart_create_user = '" + userid + "' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Tenant_login(string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tblcontract_account set update_date = getdate(), update_id = '" + userid + "'  where id = '" + userid + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void Insert_log_login(string userid, string user_type, string channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Insert into tbllog_login(userid,user_type,login_date,channel) values('" + userid + "','" + user_type + "', getdate(), '" + channel + "'); ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public void Insert_log_login_mobile(string userid, string user_type, string channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Insert into tbllog_login(userid,user_type,login_date,channel) values( @userid , @user_type , getdate(),  @channel ); ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@userid", userid);
        _with1.Parameters.AddWithValue("@user_type", user_type);
        _with1.Parameters.AddWithValue("@channel", channel);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Tenant_login_mobile(string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  update tblcontract_account set update_date = getdate(), update_id = '" + userid + "'  where id = '" + userid + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }




    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct company_code from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            //strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";
            strSQL += " select companycode as company_code from TblCompanyDetail ;   ";


        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetCompany(string comCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select top(1) * from SAPCompany  where COmpanyCode = '" + comCode + "' ;  ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetOfficerByManagerId(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblStaffHierrachy where manager_id = '" + userid + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetOfficerByVPId(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblStaffHierrachy where vp_id = '" + userid + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetOfficer(string company_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbluser u inner join tblmapp_User_Company m " +
            " on u.userid = m.userid where role = 'officer' and user_group = 'ar' and m.company_code in (" + company_code + ") ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByUserPass(string username, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblcontract_account s where s.username = '" + username + "'" +
            " and DecryptByPassPhrase('spwgsmart',s.password )  = '" + password + "' and s.Expiry_date > GETDATE()   and s.flag_active = 'y'   ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByUserPass_mobile(string username, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblcontract_account s where s.username =  @username " +
                   " and s.Expiry_date > GETDATE() and s.flag_active = 'y'  " +
                   " and CAST(DecryptByPassPhrase('spwgsmart',password )AS VARCHAR(100)) =  @password ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@username", username);
        _with1.Parameters.AddWithValue("@password", password);

        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getUser_tenant_Password_mobile(string username)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select CAST(DecryptByPassPhrase('spwgsmart',password )AS VARCHAR(100)) as password_decrypt " +
                    " from tblcontract_account where username = @username and flag_active = 'y' ;";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@username", username);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetContractDetail(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblsmart_Contract where contractnumber = '" + contractnumber + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetContractDetail_mobile(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblsmart_Contract where contractnumber =  @contractnumber  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTheme_by_companycode(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select a.CompanyCode,b.ComGroupID,b.ThemeId,c.ButtonColor,c.NavbarColor from TblCompanyGroupMapping a " +
                    " inner join TblCompanyGroup b on b.ComGroupID = a.ComGroupID " +
                    " inner join TblTheme c on c.ThemeId = b.ThemeId " +
                    " where a.CompanyCode = '" + CompanyCode + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetTheme_by_companycode_mobile(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select a.CompanyCode,b.ComGroupID,b.ThemeId,c.ButtonColor,c.NavbarColor from TblCompanyGroupMapping a " +
                    " inner join TblCompanyGroup b on b.ComGroupID = a.ComGroupID " +
                    " inner join TblTheme c on c.ThemeId = b.ThemeId " +
                    " where a.CompanyCode =  @CompanyCode   ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@CompanyCode", CompanyCode);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetFloorfromContractNumber(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select top(1)f.FloorDescription from [dbo].[SAPRentalObject] f where ContractNumber = '" + contractnumber + "'";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetFloorfromContractNumber_mobile(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select top(1)f.FloorDescription from [dbo].[SAPRentalObject] f where ContractNumber =  @contractnumber ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetUserByContractnumber(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tblcontract_account s where s.contractnumber = '" + contractnumber + "' and s.Expiry_date > GETDATE() and flag_active = 'y'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByContractnumber_mobile(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tblcontract_account s where s.contractnumber =  @contractnumber  and s.Expiry_date > GETDATE() and flag_active = 'y'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByEmail(string email)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tblcontact_point  where tblcontact_point.email = '" + email + "' AND IsMain = 'y'  and status = '1'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByEmail_mobile(string email)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tblcontact_point  where tblcontact_point.email = @email  AND IsMain = 'y'  and status = '1'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@email", email);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserByUserid(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tbluser  where userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetUserByUserid_mobile(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tbluser  where userid =  @userid  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@userid", userid);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetSmartUser_ByEmail(string email)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbluser where email = '" + email + "' and flag_active = 'y'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable Get_Confirm()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from tblsetting_confirm ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void UpdatePassword(string password, string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tbluser set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) where userid = '" + userid + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable Get_Theme_user(string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select a.company_code,c.ComGroupName,d.ButtonColor,d.NavbarColor,d.BgUri,d.NavbarColor2,d.BgUri2,d.BgUri3,d.BgUri4,d.BgUri5 " +
                  "  from tblmapp_User_Company a    " +
                  "  inner join TblCompanyGroupMapping b on b.CompanyCode = a.company_code " +
                  "  inner join TblCompanyGroup c on c.ComGroupID = b.ComGroupID " +
                  "  inner join TblTheme d on d.ThemeId = c.ThemeId " +
                  "  where a.userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable Get_Pic_Company_Group_user(string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select a.userid,b.Companycode,b.CompanyNameTh,c.ComGroupID,d.CompanyGroupLogo from tblmapp_User_Company a  " +
                  " inner join TblCompanyDetail b on b.Companycode = a.company_code " +
                  " inner join TblCompanyGroupMapping c on c.CompanyCode = b.Companycode  " +
                  " inner join TblCompanyGroup d on d.ComGroupID = c.ComGroupID  " +
                  " where a.userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable Get_Pic_Logo_login()
    {
        string status = "y";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblCompanyGroup a where a.IsActive = @status and a.CompanyGroupLogo <> '' and a.CompanyGroupLogo IS NOT NULL ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Parameters.AddWithValue("@status", status);
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getCompanycode_by_userid(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser a inner join tblmapp_User_Company b on b.userid = a.userid where a.userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getCompany_name_user(string Companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyDetail a where a.Companycode = '" + Companycode + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getCompanyGroup_name_user(string Companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from TblCompanyGroup a  " +
                    " inner join TblCompanyGroupMapping b on b.ComGroupID = a.ComGroupID  " +
                    " inner join TblCompanyDetail c on c.Companycode = b.CompanyCode  " +
                    " where c.Companycode = '" + Companycode + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getImageById(string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbltransaction_record_img where id = '" + id + "'   ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getImageAdjustById(string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbledit_transaction_record_img where id = '" + id + "'   ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }





}