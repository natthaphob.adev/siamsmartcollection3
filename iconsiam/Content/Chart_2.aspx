﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Chart_2.aspx.cs" Inherits="iconsiam.Chart_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-8 col-lg-8" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddltype" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"  Width="114px" />
                     <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" Width="114px" />
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <script type="text/javascript">
                    google.charts.load('current', { 'packages': ['corechart'] });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {

                        $.ajax({
                            type: "POST",
                            url: "Chart_2.aspx/GetChartData",
                            data: '{}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (r) {

                                var data = google.visualization.arrayToDataTable(r.d);

                                var options = {
                                    title: 'Sale YTD',
                                    hAxis: { title: 'Month', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                    vAxis: { title: 'Millions', minValue: 0 }
                                };

                                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                                chart.draw(data, options);
                            }
                        });
                    }
                </script>
                <div id="chart_div" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div id="div_location" runat="server" visible="true">
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_location",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var view = new google.visualization.DataView(data);
                                    view.setColumns([0, //The "descr column"
                                               1, //Downlink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 1, // Create an annotation column with source column "1"
                                                   type: "string",
                                                   role: "annotation"
                                               },
                                               2, // Uplink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 2, // Create an annotation column with source column "2"
                                                   type: "string",
                                                   role: "annotation"
                                               }]);

                                    var options = {
                                        title: 'YTD Compare',
                                        hAxis: { title: 'Location', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                        vAxis: { title: 'Millions', minValue: 0 }
                                    };

                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_loca1'));
                                    chart.draw(view, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_location_donut1",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_loca1'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_location_donut2",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_loca2'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <div id="chart_div_loca1" style="width: 100%; height: 500px;"></div>
                    <br />
                    <div class="row" style="width: 100%; padding: 0px; margin: 0px;">
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label1" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_loca2" style="width: 100%; height: 500px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label2" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_loca1" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>



                <div id="div_floor" runat="server" visible="false">
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_floor",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var view = new google.visualization.DataView(data);
                                    view.setColumns([0, //The "descr column"
                                               1, //Downlink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 1, // Create an annotation column with source column "1"
                                                   type: "string",
                                                   role: "annotation"
                                               },
                                               2, // Uplink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 2, // Create an annotation column with source column "2"
                                                   type: "string",
                                                   role: "annotation"
                                               }]);

                                    var options = {
                                        title: 'YTD Compare',
                                        hAxis: { title: 'Floor', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                        vAxis: { title: 'Millions', minValue: 0 }
                                    };

                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_floor1'));
                                    chart.draw(view, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_Floor_donut1",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_floor1'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_Floor_donut2",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_floor2'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <div id="chart_div_floor1" style="width: 100%; height: 500px;"></div>
                    <br />
                    <div class="row" style="width: 100%; padding: 0px; margin: 0px;">
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label3" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_floor2" style="width: 100%; height: 500px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label4" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_floor1" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>





                <div id="div_cat" runat="server" visible="false">
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_Category",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var view = new google.visualization.DataView(data);
                                    view.setColumns([0, //The "descr column"
                                               1, //Downlink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 1, // Create an annotation column with source column "1"
                                                   type: "string",
                                                   role: "annotation"
                                               },
                                               2, // Uplink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 2, // Create an annotation column with source column "2"
                                                   type: "string",
                                                   role: "annotation"
                                               }]);

                                    var options = {
                                        title: 'YTD Compare',
                                        hAxis: { title: 'Category Leasing', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                        vAxis: { title: 'Millions', minValue: 0 }
                                    };

                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_cat1'));
                                    chart.draw(view, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_Category_donut1",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_cat1'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_Category_donut2",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_cat2'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <div id="chart_div_cat1" style="width: 100%; height: 500px;"></div>
                    <br />
                    <div class="row" style="width: 100%; padding: 0px; margin: 0px;">
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label5" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_cat2" style="width: 100%; height: 500px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label6" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_cat1" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>

                <div id="div_business" runat="server" visible="false">
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_IndustrtGroup",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var view = new google.visualization.DataView(data);
                                    view.setColumns([0, //The "descr column"
                                               1, //Downlink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 1, // Create an annotation column with source column "1"
                                                   type: "string",
                                                   role: "annotation"
                                               },
                                               2, // Uplink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 2, // Create an annotation column with source column "2"
                                                   type: "string",
                                                   role: "annotation"
                                               }]);

                                    var options = {
                                        title: 'YTD Compare',
                                        hAxis: { title: 'Business Type', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                        vAxis: { title: 'Millions', minValue: 0 }
                                    };

                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_business1'));
                                    chart.draw(view, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_IndustrtGroup_donut1",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_business1'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_IndustrtGroup_donut2",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_business2'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <div id="chart_div_business1" style="width: 100%; height: 500px;"></div>
                    <br />
                    <div class="row" style="width: 100%; padding: 0px; margin: 0px;">
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label7" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_business2" style="width: 100%; height: 500px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label8" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_business1" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>

                <div id="div_building" runat="server" visible="false">
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_building",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var view = new google.visualization.DataView(data);
                                    view.setColumns([0, //The "descr column"
                                               1, //Downlink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 1, // Create an annotation column with source column "1"
                                                   type: "string",
                                                   role: "annotation"
                                               },
                                               2, // Uplink column
                                               {
                                                   calc: "stringify",
                                                   sourceColumn: 2, // Create an annotation column with source column "2"
                                                   type: "string",
                                                   role: "annotation"
                                               }]);

                                    var options = {
                                        title: 'YTD Compare',
                                        hAxis: { title: 'Building', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                                        vAxis: { title: 'Millions', minValue: 0 }
                                    };

                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_building1'));
                                    chart.draw(view, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_building_donut1",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_building1'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <script type="text/javascript">
                        google.charts.load('current', { packages: ['corechart', 'bar'] });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {

                            $.ajax({
                                type: "POST",
                                url: "Chart_2.aspx/GetChartData_building_donut2",
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (r) {

                                    var data = google.visualization.arrayToDataTable(r.d);

                                    var options = {
                                        pieHole: 0.4,
                                    };

                                    var chart = new google.visualization.PieChart(document.getElementById('chart_donut_building2'));
                                    chart.draw(data, options);
                                }
                            });
                        }
                    </script>
                    <div id="chart_div_building1" style="width: 100%; height: 500px;"></div>
                    <br />
                    <div class="row" style="width: 100%; padding: 0px; margin: 0px;">
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label9" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_building2" style="width: 100%; height: 500px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center; border: solid;">
                            <asp:Label ID="Label10" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                            <div id="chart_donut_building1" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>








</asp:Content>
