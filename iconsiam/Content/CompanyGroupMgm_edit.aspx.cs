﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class CompanyGroupMgm_edit : System.Web.UI.Page
    {
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["comgroupid"]))
            {
                var comp = Serv.getCompanyGroupByID(Request.QueryString["comgroupid"].ToString());
                if (comp.Rows.Count != 0)
                {

                    txtGroupName.Text = comp.Rows[0]["ComGroupName"].ToString();
                    ddlstatus.SelectedValue = comp.Rows[0]["IsActive"].ToString();
                    imgCompany.ImageUrl = comp.Rows[0]["CompanyGroupLogo"].ToString().Replace("~", "");

                    bind_data_1();
                }
                else
                {
                    Response.Redirect("~/login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/login.aspx");

            }
        }

        protected void bind_data_1()
        {
            var commapping = Serv.getCompanyGroupMappingByID(Request.QueryString["comgroupid"].ToString());
            if (commapping.Rows.Count != 0)
            {
                GridView_mapp.DataSource = commapping;
                GridView_mapp.DataBind();
            }
            else
            {
                GridView_mapp.DataSource = null;
                GridView_mapp.DataBind();
            }

        }

        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));



        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_mapp.PageIndex = e.NewPageIndex;
            this.bind_data_1();
        }

        //protected void btnsave_Click(object sender, EventArgs e)
        //{
        //    foreach (GridViewRow row in GridView_List.Rows) //Running all lines of grid
        //    {
        //        if (row.RowType == DataControlRowType.DataRow)
        //        {
        //            HiddenField hdd_CompanyCode = (HiddenField)row.FindControl("hdd_CompanyCode");
        //            CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
        //            if (chkRow.Checked)
        //            {
        //                Serv.InsertComGroupMapping(Request.QueryString["comgroupid"].ToString(), hdd_CompanyCode.Value, HttpContext.Current.Session["s_userid"].ToString()
        //                    , HttpContext.Current.Session["s_userid"].ToString(), "y");
        //            }
        //        }
        //    }

        //    bind_data();



        //}


        //protected void btnupdate_Click(object sender, EventArgs e)
        //{
        //    foreach (GridViewRow row in GridView_mapp.Rows) //Running all lines of grid
        //    {
        //        if (row.RowType == DataControlRowType.DataRow)
        //        {
        //            HiddenField hddmapp_id = (HiddenField)row.FindControl("hddmapp_id");
        //            HiddenField hdd_CompanyCode = (HiddenField)row.FindControl("hdd_CompanyCode");
        //            CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
        //            if (chkRow.Checked)
        //            {
        //                //Serv.DeleteComGroupMapping(hddmapp_id.Value);

        //                Serv.InsertComGroupMapping(Request.QueryString["comgroupid"].ToString(), hdd_CompanyCode.Value, HttpContext.Current.Session["s_userid"].ToString()
        //                    , HttpContext.Current.Session["s_userid"].ToString(), "y");
        //            }
        //        }
        //    }

        //    bind_data();


        //}


        protected void btnupdateinfo_Click(object sender, EventArgs e)
        {
            string img_name1 = "";

            if (fileupload_logo.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_logo.FileName);
                FileInfo fi1 = new FileInfo(fileupload_logo.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    img_name1 = ConfigurationManager.AppSettings["imgLogo"] + Request.QueryString["comgroupid"].ToString() + "_" + xx;
                    fileupload_logo.SaveAs(Server.MapPath(img_name1));
                }

            }

            var company_ = Serv.DeleteCompanyMapping_template_by_comgroup(Request.QueryString["comgroupid"].ToString());
            if (company_.Rows.Count != 0)
            {
                for (int i = 0; i < company_.Rows.Count; i++)
                {
                    Serv.DeletecompanyMapping_template(company_.Rows[i]["CompanyCode"].ToString());
                }
            }

            Serv.DeleteComGroupMappingByComGroup(Request.QueryString["comgroupid"].ToString());

            foreach (GridViewRow row in GridView_mapp.Rows) //Running all lines of grid
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hddmapp_id = (HiddenField)row.FindControl("hddmapp_id");
                    HiddenField hdd_CompanyCode = (HiddenField)row.FindControl("hdd_CompanyCode");
                    CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
                    if (chkRow.Checked)
                    {
                        //Serv.DeleteComGroupMapping(hddmapp_id.Value);
                        Serv.InsertComGroupMapping(Request.QueryString["comgroupid"].ToString(), hdd_CompanyCode.Value, HttpContext.Current.Session["s_userid"].ToString()
                             , HttpContext.Current.Session["s_userid"].ToString(), "y");

                        // Email Template                       
                        Serv.Insert_Emailtemplate_company(hdd_CompanyCode.Value, HttpContext.Current.Session["s_userid"].ToString());
                    }
                   
                }
            }

            Serv.UpdateCompanyGroup(txtGroupName.Text, ddlstatus.SelectedValue, Request.QueryString["comgroupid"].ToString(), img_name1,
                HttpContext.Current.Session["s_userid"].ToString());

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = " +
                " 'CompanyGroupMgm.aspx?comgroupid=" + Request.QueryString["comgroupid"].ToString() + "';", true);


        }

        protected void GridView_mapp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkBox = (CheckBox)e.Row.FindControl("CheckBox1");
                HiddenField hddcheck = (HiddenField)e.Row.FindControl("hddcheck");
                HiddenField hddcheckNo = (HiddenField)e.Row.FindControl("hddcheckNo");

                if (hddcheck.Value == "y")
                {
                    chkBox.Checked = true;
                }
                else
                {
                    chkBox.Checked = false;

                }

                if (hddcheckNo.Value == "y")
                {
                    e.Row.Enabled = false;
                }
                else
                {
                    e.Row.Enabled = true;

                }


            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyGroupMgm.aspx?comgroupid=" + Request.QueryString["comgroupid"].ToString());
        }
    }
}