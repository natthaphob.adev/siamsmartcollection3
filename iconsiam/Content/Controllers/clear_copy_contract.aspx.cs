﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam.Controllers
{
    public partial class clear_copy_contract : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            LogService l = new LogService();
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            jobscheduleDLL Serv = new jobscheduleDLL();
            sentmail mail = new sentmail();
            dateconvert date_con = new dateconvert();

            if (!Page.IsPostBack)
            {
                var com = Serv.getCompany();
                if (com.Rows.Count != 0)
                {
                    for (int j = 0; j < com.Rows.Count; j++)
                    {
                        var job = Serv.getTime("jobcopy_contract", com.Rows[j]["CompanyCode"].ToString());
                        if (job.Rows.Count != 0)
                        {
                            try
                            {
                                string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
                                string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

                                int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

                                if (result > 0)
                                {

                                    var con_ex = Serv.GetContract_Expire();
                                    if (con_ex.Rows.Count != 0)
                                    {
                                        for (int i = 0; i < con_ex.Rows.Count; i++)
                                        {
                                            var new_con = Serv.Find_new_Contract(con_ex.Rows[i]["ContractNumber"].ToString());
                                            if (new_con.Rows.Count != 0)
                                            {
                                                Serv.Update_Account("y", "n", con_ex.Rows[i]["ContractNumber"].ToString(), "1");
                                                Serv.Update_Account("n", "y", new_con.Rows[0]["ContractNumber"].ToString(), "1");

                                                Serv.Update_status_Contract("0", con_ex.Rows[i]["ContractNumber"].ToString());

                                                Serv.Update_Transaction(new_con.Rows[0]["ContractNumber"].ToString(), con_ex.Rows[i]["smart_shop_close"].ToString(),
                                                    con_ex.Rows[i]["ContractNumber"].ToString());

                                            }
                                        }
                                    }
                                    Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));

                                }

                            }
                            catch (Exception ex)
                            {
                                Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t_adhoc");
                            }
                        }
                    }
                }



            }

        }
    }
}