﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="iconsiam.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChartData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var data = google.visualization.arrayToDataTable(r.d);

                    var options = {
                        title: 'Daily Sale by Location',
                        hAxis: { title: 'Date', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                        vAxis: { title: 'Millions', minValue: 0 }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            });
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChart_Floor",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var data = google.visualization.arrayToDataTable(r.d);

                    var options = {
                        title: 'Daily Sale by Floor',
                        hAxis: { title: 'Date', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                        vAxis: { title: 'Millions', minValue: 0 }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
                    chart.draw(data, options);
                }
            });
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChart_CategoryLeasing",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var data = google.visualization.arrayToDataTable(r.d);

                    var options = {
                        title: 'Daily Sale by Category Leasing',
                        hAxis: { title: 'Date', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                        vAxis: { title: 'Millions', minValue: 0 }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div3'));
                    chart.draw(data, options);
                }
            });
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChart_IndustryGroup",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var data = google.visualization.arrayToDataTable(r.d);

                    var options = {
                        title: 'Daily Sale by Business Type',
                        hAxis: { title: 'Date', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                        vAxis: { title: 'Millions', minValue: 0 }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div4'));
                    chart.draw(data, options);
                }
            });
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChart_Building",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var data = google.visualization.arrayToDataTable(r.d);

                    var options = {
                        title: 'Daily Sale by Building',
                        hAxis: { title: 'Date', titleTextStyle: { color: '#333' }, textStyle: { fontSize: 10 } },
                        vAxis: { title: 'Millions', minValue: 0 }
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div5'));
                    chart.draw(data, options);
                }
            });
        }
    </script>


    <div id="chart_div" style="width: 100%; height: 500px;"></div>
    <div id="chart_div2" style="width: 100%; height: 500px;"></div>
    <div id="chart_div3" style="width: 100%; height: 500px;"></div>
    <div id="chart_div4" style="width: 100%; height: 500px;"></div>
    <div id="chart_div5" style="width: 100%; height: 500px;"></div>

</asp:Content>
