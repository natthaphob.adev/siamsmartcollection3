﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;
using System.Text;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace iconsiam
{
    public partial class EmailTemplate_Comgroup_edit : System.Web.UI.Page
    {
        EmailTemplateDLL Serv = new EmailTemplateDLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();
                }
            }
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Companygroup"]))
            {
                var com = Serv.getComGroupName_template(Request.QueryString["Companygroup"].ToString());
                if (com.Rows.Count != 0)
                { 
                    txtComGroupName.Text = com.Rows[0]["ComGroupName"].ToString();
                    //txtlink.Text = com.Rows[0]["Link"].ToString();
                    txtsendfromteam.Text = com.Rows[0]["Sendfromteam"].ToString();
                    txtsendfromcompany.Text = com.Rows[0]["Sendfromcompany"].ToString();
                    txttel.Text = com.Rows[0]["Tel"].ToString();
                    txtemail_footer.Text = com.Rows[0]["F_Email"].ToString();
                    txtcallcenter_Footer.Text = com.Rows[0]["F_Callcenter"].ToString();


                }
                else
                {
                    Response.Redirect("~/EmailTemplate_CompanyGroup.aspx");

                }
            }
            else
            {
                Response.Redirect("~/EmailTemplate_CompanyGroup.aspx");

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Companygroup"]))
            {
                if (txtsendfromteam.Text != "" && txtsendfromcompany.Text != "" && txttel.Text != "" && txtemail_footer.Text != ""
                                && txtcallcenter_Footer.Text != "")
                {
                    Serv.UpdateCompanyGroup_Template( txtsendfromteam.Text, txtsendfromcompany.Text, txttel.Text, txtemail_footer.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), 
                        txtcallcenter_Footer.Text, HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["Companygroup"].ToString());

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='EmailTemplate_CompanyGroup.aspx';", true);

                }
                else
                {
                    POPUPMSG("กรุณากรอกข้อมูลให้ถูกต้อง");
                    return;
                }
            }
            else
            {
                Response.Redirect("~/EmailTemplate_CompanyGroup.aspx");
            }
            
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmailTemplate_CompanyGroup.aspx");
        }
    }
}