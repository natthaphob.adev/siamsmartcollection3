﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected string MyTheme { get; set; } //btn
        protected string NavbarColor { get; set; } //txtbtn
        protected string NavbarColor2 { get; set; } //txthover
        protected string BgColor { get; set; } //gb bar
        protected string BgColor2 { get; set; } //gb bar
        protected string BgColor3 { get; set; } //gb bar
        protected string BgColor4 { get; set; } //gb bar

        protected string BgColor5 { get; set; } //gb bar
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["s_userid"] == null)
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    lbusername.Text = HttpContext.Current.Session["s_username"].ToString();
                    lbusernam2.Text = HttpContext.Current.Session["s_username"].ToString();
                    //  lbemail.Text = HttpContext.Current.Session["s_email"].ToString();
                    lbcompany.Text = HttpContext.Current.Session["company_name_login"].ToString();

                    if (HttpContext.Current.Session["code_theme"] != null)
                    {
                        this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                    }
                    else
                    {
                        this.MyTheme = "#00263d";
                    }

                    if (HttpContext.Current.Session["code_Navbar"] != null)
                    {
                        this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                    }
                    else
                    {
                        this.NavbarColor = "#fff";
                    }

                    if (HttpContext.Current.Session["code_Navbar2"] != null)
                    {
                        this.NavbarColor2 = HttpContext.Current.Session["code_Navbar2"].ToString();
                    }
                    else
                    {
                        this.NavbarColor2 = "#fff";
                    }

                    if (HttpContext.Current.Session["code_Bg"] != null)
                    {
                        this.BgColor = HttpContext.Current.Session["code_Bg"].ToString();
                    }
                    else
                    {
                        this.BgColor = "##a6a29e";
                    }

                    if (HttpContext.Current.Session["code_Bg2"] != null)
                    {
                        this.BgColor2 = HttpContext.Current.Session["code_Bg2"].ToString();
                    }
                    else
                    {
                        this.BgColor2 = "##cab6a3";
                    }

                    if (HttpContext.Current.Session["code_Bg3"] != null)
                    {
                        this.BgColor3 = HttpContext.Current.Session["code_Bg3"].ToString();
                    }
                    else
                    {
                        this.BgColor3 = "#5f4a79";
                    }

                    if (HttpContext.Current.Session["code_Bg4"] != null)
                    {
                        this.BgColor4 = HttpContext.Current.Session["code_Bg4"].ToString();
                    }
                    else
                    {
                        this.BgColor4 = "#7b5f9c";
                    }

                    if (HttpContext.Current.Session["code_Bg5"] != null)
                    {
                        this.BgColor5 = HttpContext.Current.Session["code_Bg5"].ToString();
                    }
                    else
                    {
                        this.BgColor5 = "#9775c0";
                    }


                    if (HttpContext.Current.Session["PicGroupCompany"] != null)
                    {
                        this.imgCompany.ImageUrl = HttpContext.Current.Session["PicGroupCompany"].ToString();
                    }
                    else
                    {
                        this.imgCompany.ImageUrl = "img/siampi.png";
                    }


                    if (HttpContext.Current.Session["s_user_img"].ToString() == "")
                    {
                        Image1.Visible = false;
                        Image2.Visible = false;
                    }
                    else
                    {
                        if (HttpContext.Current.Session["noshow_img"].ToString() == "y")
                        {
                            Image1.Visible = false;
                            Image2.Visible = false;
                        }
                        else
                        {
                            Image1.ImageUrl = HttpContext.Current.Session["s_user_img"].ToString();
                            Image2.ImageUrl = HttpContext.Current.Session["s_user_img"].ToString();
                        }


                    }


                    if (HttpContext.Current.Session["s_user_group"].ToString() == "ar")
                    {
                        if (HttpContext.Current.Session["role"].ToString() == "vp")
                        {
                            liDashboard1.Visible = true;
                            liMasterData.Visible = false;
                            liarmonitoring.Visible = true;
                            lireport.Visible = true;
                            lishop.Visible = true;
                            liCompany.Visible = false;
                            ligroupmapp.Visible = false;


                            liSAPData.Visible = false;
                            liuser.Visible = false;
                            lisetting.Visible = false;
                            lirepsap.Visible = true;
                            limasterrep.Visible = true;
                        }
                        else
                        {
                            liDashboard1.Visible = true;
                            liMasterData.Visible = false;
                            liarmonitoring.Visible = true;
                            lireport.Visible = true;
                            lishop.Visible = true;
                            liCompany.Visible = false;
                            ligroupmapp.Visible = false;


                            liSAPData.Visible = false;
                            liuser.Visible = false;
                            lisetting.Visible = false;
                            lirepsap.Visible = true;
                            limasterrep.Visible = true;
                        }

                    }
                    else if (HttpContext.Current.Session["s_user_group"].ToString() == "ae")
                    {

                        liDashboard1.Visible = true;
                        liMasterData.Visible = false;
                        liarmonitoring.Visible = false;
                        lireport.Visible = true;
                        lishop.Visible = false;
                        liCompany.Visible = false;
                        ligroupmapp.Visible = false;


                        liSAPData.Visible = false;
                        liuser.Visible = false;
                        lisetting.Visible = false;

                        if (HttpContext.Current.Session["role"].ToString() == "officer")
                        {
                            liDashboard1.Visible = false;
                            litopten.Visible = false;
                            liCompany.Visible = false;
                        }


                    }
                    else if (HttpContext.Current.Session["s_user_group"].ToString() == "management")
                    {
                        liDashboard1.Visible = true;
                        liMasterData.Visible = false;
                        liarmonitoring.Visible = false;
                        lireport.Visible = true;
                        lishop.Visible = false;
                        liCompany.Visible = false;
                        ligroupmapp.Visible = false;

                        lisaletransaction.Visible = true;

                        liSAPData.Visible = false;
                        liuser.Visible = false;
                        lisetting.Visible = false;
                    }
                    else if (HttpContext.Current.Session["s_user_group"].ToString() == "admin")
                    {
                        liDashboard1.Visible = true;
                        liMasterData.Visible = true;
                        liarmonitoring.Visible = true;
                        lireport.Visible = true;
                        lishop.Visible = true;
                        liCompany.Visible = false;
                        ligroupmapp.Visible = false;
                        licommapp.Visible = true;

                        liindustryGroupMapp.Visible = true;
                        licontracttypeMapp.Visible = true;
                        licategoryleasingMapp.Visible = true;
                        ligrouplocationMapp.Visible = true;
                        lishopGroupListMapp.Visible = true;

                        liindustryGroup.Visible = false;
                        licontracttype.Visible = false;
                        licategoryleasing.Visible = false;
                        ligrouplocation.Visible = false;
                        lishopGroupList.Visible = false;

                        liSAPData.Visible = true;
                        liuser.Visible = true;
                        lisetting.Visible = true;

                        lirepsap.Visible = true;
                        limasterrep.Visible = true;
                    }

                    if (HttpContext.Current.Session["role"].ToString() == "super_admin")
                    {
                        liDashboard1.Visible = true;
                        liMasterData.Visible = true;
                        liarmonitoring.Visible = true;
                        lireport.Visible = true;
                        lishop.Visible = true;


                        liSAPData.Visible = true;
                        liuser.Visible = true;
                        lisetting.Visible = true;

                        lirepsap.Visible = true;
                        limasterrep.Visible = true;
                    }
                    else if (HttpContext.Current.Session["role"].ToString() == "datacenter")
                    {
                        liDashboard1.Visible = false;
                        liMasterData.Visible = true;
                        liarmonitoring.Visible = false;
                        lireport.Visible = false;
                        lishop.Visible = false;
                        liCompany.Visible = false;
                        ligroupmapp.Visible = false;


                        liSAPData.Visible = false;
                        liuser.Visible = false;
                        lisetting.Visible = false;

                        lirepsap.Visible = false;
                        limasterrep.Visible = false;

                        liindustryGroupMapp.Visible = false;
                        licontracttypeMapp.Visible = false;
                        licategoryleasingMapp.Visible = false;
                        ligrouplocationMapp.Visible = false;
                        lishopGroupListMapp.Visible = false;
                    }


                    loginDLL Serv = new loginDLL();
                    Serv.Del_ContractSmartCollectio(HttpContext.Current.Session["s_userid"].ToString());

                }
            }
            else
            {
                if (HttpContext.Current.Session["code_theme"] != null)
                {
                    this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                }
                else
                {
                    this.MyTheme = "#00263d";
                }

                if (HttpContext.Current.Session["code_Navbar"] != null)
                {
                    this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                }
                else
                {
                    this.NavbarColor = "#fff";
                }

                if (HttpContext.Current.Session["code_Navbar2"] != null)
                {
                    this.NavbarColor2 = HttpContext.Current.Session["code_Navbar2"].ToString();
                }
                else
                {
                    this.NavbarColor2 = "#fff";
                }

                if (HttpContext.Current.Session["code_Bg"] != null)
                {
                    this.BgColor = HttpContext.Current.Session["code_Bg"].ToString();
                }
                else
                {
                    this.BgColor = "##a6a29e";
                }

                if (HttpContext.Current.Session["code_Bg2"] != null)
                {
                    this.BgColor2 = HttpContext.Current.Session["code_Bg2"].ToString();
                }
                else
                {
                    this.BgColor2 = "##cab6a3";
                }

                if (HttpContext.Current.Session["code_Bg3"] != null)
                {
                    this.BgColor3 = HttpContext.Current.Session["code_Bg3"].ToString();
                }
                else
                {
                    this.BgColor3 = "#5f4a79";
                }

                if (HttpContext.Current.Session["code_Bg4"] != null)
                {
                    this.BgColor4 = HttpContext.Current.Session["code_Bg4"].ToString();
                }
                else
                {
                    this.BgColor4 = "#7b5f9c";
                }

                if (HttpContext.Current.Session["code_Bg5"] != null)
                {
                    this.BgColor5 = HttpContext.Current.Session["code_Bg5"].ToString();
                }
                else
                {
                    this.BgColor5 = "#9775c0";
                }


                if (HttpContext.Current.Session["PicGroupCompany"] != null)
                {
                    this.imgCompany.ImageUrl = HttpContext.Current.Session["PicGroupCompany"].ToString();
                }
                else
                {
                    this.imgCompany.ImageUrl = "img/siampi.png";
                }
            }



        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            Response.Redirect("~/daily_rep.aspx");
        }


    }
}