﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class contracttypeMapp_app : System.Web.UI.Page
    {
        smt2_contracttypeMappDLL Serv = new smt2_contracttypeMappDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();
                }
            }

        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["comgroupid"]))
            {
                var comp = Serv.getCompanyGroupByID(Request.QueryString["comgroupid"].ToString());
                if (comp.Rows.Count != 0)
                {

                    txtcontracttype.Text = comp.Rows[0]["ComGroupName"].ToString();

                    bind_data_1();

                }
                else
                {
                    Response.Redirect("~/login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/login.aspx");

            }


        }

        protected void bind_data_1()
        {
            var industryGroupMapp = Serv.getContractType_mapping(Request.QueryString["comgroupid"].ToString());
            if (industryGroupMapp.Rows.Count != 0)
            {
                GridView_mapp.DataSource = industryGroupMapp;
                GridView_mapp.DataBind();
            }
            else
            {
                GridView_mapp.DataSource = null;
                GridView_mapp.DataBind();
            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            Serv.DeleteContractTypeMappByCompany(Request.QueryString["comgroupid"].ToString());

            foreach (GridViewRow row in GridView_mapp.Rows) //Running all lines of grid
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hddid = (HiddenField)row.FindControl("hddid");
                    CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
                    if (chkRow.Checked)
                    {
                        Serv.InsertContractTypeMapp(hddid.Value, Request.QueryString["comgroupid"].ToString(), HttpContext.Current.Session["s_userid"].ToString(),
                            HttpContext.Current.Session["s_userid"].ToString(), "y");
                    }
                }
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = " +
                 " 'contracttypeMapp.aspx';", true);

       


        }


        protected void btnupdate_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView_mapp.Rows) //Running all lines of grid
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hddid = (HiddenField)row.FindControl("hddid");
                    CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
                    if (chkRow.Checked)
                    {
                        Serv.DeleteContractTypeMapp(hddid.Value);
                    }
                }
            }

            bind_data();


        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contracttypeMapp.aspx");

        }

        protected void GridView_mapp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_mapp.PageIndex = e.NewPageIndex;
            this.bind_data_1();
        }

        protected void GridView_mapp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkBox = (CheckBox)e.Row.FindControl("CheckBox1");
                HiddenField hddcheck = (HiddenField)e.Row.FindControl("hddcheck");

                if (hddcheck.Value == "y")
                {
                    chkBox.Checked = true;
                }
                else
                {
                    chkBox.Checked = false;

                }

            }
        }
    }
}