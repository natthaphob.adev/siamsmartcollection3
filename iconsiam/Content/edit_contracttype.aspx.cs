﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class edit_contracttype : System.Web.UI.Page
    {

        contracttypeDLL Serv = new contracttypeDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtContractTypeNameEn.Focus();
                    txtContractTypeNameEn.Attributes.Add("onkeypress", "return next_tools(event,'" + txtContractTypeNameTH.ClientID + "')");
                    txtContractTypeNameTH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                    bind_default();
                    bind_data();
                }
            }
        }
        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var indus = Serv.getcontracttypeById(Request.QueryString["id"].ToString());
                if (indus.Rows.Count != 0)
                {
                    txtContractTypeNameEn.Text = indus.Rows[0]["ContractTypeNameEn"].ToString();
                    txtContractTypeNameTH.Text = indus.Rows[0]["ContractTypeNameTH"].ToString();
                    ddlstatus.SelectedValue = indus.Rows[0]["Flag_active"].ToString();
                }
                else
                {
                    Response.Redirect("~/contracttype.aspx");
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtContractTypeNameEn.Text != "" && txtContractTypeNameTH.Text != "")
            {
                var ex1 = Serv.getcontracttypeByNameEN_nid(txtContractTypeNameEn.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                var ex2 = Serv.getcontracttypeByNameTH_nid(txtContractTypeNameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Contract Type Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Contract Type Name(TH) ซ้ำ");
                    return;
                }
                else
                {
                    Serv.Updatecontracttype(txtContractTypeNameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), 
                        txtContractTypeNameEn.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue, HttpContext.Current.Session["s_userid"].ToString(),
                        Request.QueryString["id"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='contracttype.aspx';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contracttype.aspx");
        }



    }
}