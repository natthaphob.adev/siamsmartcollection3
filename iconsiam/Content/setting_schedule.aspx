﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="setting_schedule.aspx.cs" Inherits="iconsiam.setting_schedule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }


    </script>



    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>




    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Schedule Job
            </p>
        </div>
    </div>
    <div class="card">
        <div class="row" style="margin-left: 20px; margin-right: 20px; margin-top: 20px;">
            <div class="col-md-8 col-lg-8">
                Confirmation Notification
            </div>
        </div>
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <asp:HiddenField ID="hddid1" runat="server" />
            <asp:HiddenField ID="hddid1_" runat="server" />
            <asp:HiddenField ID="hddid2" runat="server" />
            <asp:HiddenField ID="hddid3" runat="server" />
            <asp:HiddenField ID="hddid4" runat="server" />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtcompany" runat="server" placeholder="Company" ReadOnly="true" class="form-control"></asp:TextBox>
                    <asp:HiddenField ID="hddcompanyCode" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    Notification 1st - Sale Transaction Date 1-15
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <asp:TextBox ID="txtdate1" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate1_CalendarExtender" runat="server" BehaviorID="txtdate1_CalendarExtender" TargetControlID="txtdate1" OnClientShown="calendarShown" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlhh1" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlmm1" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    Notification 2nd - Sale Transaction Date 16-EOM
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <asp:TextBox ID="txtdate1_1" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate1_1_CalendarExtender" runat="server" BehaviorID="txtdate1_1_CalendarExtender" TargetControlID="txtdate1_1"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlhh1_1" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlmm1_1" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
            </div>
            <br />
            
        </div>
    </div>
    <br />

     <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Schedule Job SAP
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    Copy Contract Clearing Data
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <asp:TextBox ID="txtdate4" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate4_CalendarExtender" runat="server" BehaviorID="txtdate4_CalendarExtender" TargetControlID="txtdate4"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlhh4" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlmm4" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-8 col-lg-8">
                    Sync SAP 
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <asp:TextBox ID="txtdate3" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate3_CalendarExtender" runat="server" BehaviorID="txtdate3_CalendarExtender" TargetControlID="txtdate3"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy"></asp:CalendarExtender>
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlhh3" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:DropDownList ID="ddlmm3" runat="server" class="form-control" Width="150px"></asp:DropDownList>
                </div>
            </div>

            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>




</asp:Content>
