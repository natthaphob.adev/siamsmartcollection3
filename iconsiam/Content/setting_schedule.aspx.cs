﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class setting_schedule : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        setting_scheduleDLL Serv = new setting_scheduleDLL();
        dateconvert dateCon = new dateconvert();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtdate1.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", EngCI);
                    txtdate1_1.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", EngCI);
                    txtdate3.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", EngCI);
                    txtdate4.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", EngCI);

                    //================================================ HH-mm 1 ======================================================
                    for (int bb = 0; bb <= 23; bb++)
                    {
                        ddlhh1.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                        ddlhh1_1.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlhh1.SelectedValue = "00";

                    for (int bb = 0; bb <= 59; bb++)
                    {
                        ddlmm1.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                        ddlmm1_1.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlmm1.SelectedValue = "00";

                    //================================================ HH-mm 3 ======================================================
                    for (int bb = 0; bb <= 23; bb++)
                    {
                        ddlhh3.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlhh3.SelectedValue = "00";

                    for (int bb = 0; bb <= 59; bb++)
                    {
                        ddlmm3.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlmm3.SelectedValue = "00";

                    //================================================ HH - mm 4 ======================================================
                    for (int bb = 0; bb <= 23; bb++)
                    {
                        ddlhh4.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlhh4.SelectedValue = "00";

                    for (int bb = 0; bb <= 59; bb++)
                    {
                        ddlmm4.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                    }
                    ddlmm4.SelectedValue = "00";

                    bind_default();

                }
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void bind_default()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var vat = Serv.getCompanyByCompanyCode(Request.QueryString["id"].ToString());
                if (vat.Rows.Count != 0)
                {
                    hddcompanyCode.Value = vat.Rows[0]["Companycode"].ToString();
                    txtcompany.Text = vat.Rows[0]["CompanyNameTh"].ToString();

                    var j1 = Serv.GetJob("jobconfirm", vat.Rows[0]["Companycode"].ToString());
                    if (j1.Rows.Count != 0)
                    {
                        hddid1.Value = j1.Rows[0]["id"].ToString();

                        txtdate1.Text = Convert.ToDateTime(j1.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(8, 2) + "/" +
                            Convert.ToDateTime(j1.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(5, 2) + "/" +
                            Convert.ToDateTime(j1.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(0, 4);

                        ddlhh1.SelectedValue = Convert.ToDateTime(j1.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(11, 2);
                        ddlmm1.SelectedValue = Convert.ToDateTime(j1.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(14, 2);
                    }

                    var j1_ = Serv.GetJob("jobconfirm2", vat.Rows[0]["Companycode"].ToString());
                    if (j1_.Rows.Count != 0)
                    {
                        hddid1_.Value = j1_.Rows[0]["id"].ToString();

                        txtdate1_1.Text = Convert.ToDateTime(j1_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(8, 2) + "/" +
                            Convert.ToDateTime(j1_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(5, 2) + "/" +
                            Convert.ToDateTime(j1_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(0, 4);

                        ddlhh1_1.SelectedValue = Convert.ToDateTime(j1_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(11, 2);
                        ddlmm1_1.SelectedValue = Convert.ToDateTime(j1_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(14, 2);
                    }

                    var j3 = Serv.GetJob("syncsap", vat.Rows[0]["Companycode"].ToString());
                    if (j3.Rows.Count != 0)
                    {
                        hddid3.Value = j3.Rows[0]["id"].ToString();
                        txtdate3.Text = Convert.ToDateTime(j3.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(8, 2) + "/" +
                            Convert.ToDateTime(j3.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(5, 2) + "/" +
                            Convert.ToDateTime(j3.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(0, 4);

                        ddlhh3.SelectedValue = Convert.ToDateTime(j3.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(11, 2);
                        ddlmm3.SelectedValue = Convert.ToDateTime(j3.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(14, 2);
                    }


                    var j4 = Serv.GetJob("jobcopy_contract", vat.Rows[0]["Companycode"].ToString());
                    if (j4.Rows.Count != 0)
                    {
                        hddid4.Value = j4.Rows[0]["id"].ToString();
                        txtdate4.Text = Convert.ToDateTime(j4.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(8, 2) + "/" +
                            Convert.ToDateTime(j4.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(5, 2) + "/" +
                            Convert.ToDateTime(j4.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(0, 4);

                        ddlhh4.SelectedValue = Convert.ToDateTime(j4.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(11, 2);
                        ddlmm4.SelectedValue = Convert.ToDateTime(j4.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI).Substring(14, 2);
                    }


                }
                else
                {
                    Response.Redirect("~/vatList.aspx");

                }
            }
            else
            {
                Response.Redirect("~/vatList.aspx");

            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            string d1 = dateCon.con_date(txtdate1.Text) + " " + ddlhh1.SelectedValue + ":" + ddlmm1.SelectedValue + ":00";
            if (hddid1.Value != "")
            {
                Serv.UpdateJob(d1, HttpContext.Current.Session["s_userid"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), hddid1.Value);
            }
            else
            {
                Serv.InsertJob("jobconfirm", d1, d1, HttpContext.Current.Session["s_userid"].ToString(), hddcompanyCode.Value);
            }

            string d1_ = dateCon.con_date(txtdate1_1.Text) + " " + ddlhh1_1.SelectedValue + ":" + ddlmm1_1.SelectedValue + ":00";
            if (hddid1_.Value != "")
            {
                Serv.UpdateJob(d1_, HttpContext.Current.Session["s_userid"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), hddid1_.Value);
            }
            else
            {
                Serv.InsertJob("jobconfirm2", d1_, d1_, HttpContext.Current.Session["s_userid"].ToString(), hddcompanyCode.Value);

            }

            string d3 = dateCon.con_date(txtdate3.Text) + " " + ddlhh3.SelectedValue + ":" + ddlmm3.SelectedValue + ":00";
            if (hddid3.Value != "")
            {
                Serv.UpdateJob(d3, HttpContext.Current.Session["s_userid"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), hddid3.Value);
            }
            else
            {
                Serv.InsertJob("syncsap", d3, d3, HttpContext.Current.Session["s_userid"].ToString(), hddcompanyCode.Value);

            }

            string d4 = dateCon.con_date(txtdate4.Text) + " " + ddlhh4.SelectedValue + ":" + ddlmm4.SelectedValue + ":00";
            if (hddid4.Value != "")
            {
                Serv.UpdateJob(d4, HttpContext.Current.Session["s_userid"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), hddid4.Value);
            }
            else
            {
                Serv.InsertJob("jobcopy_contract", d4, d4, HttpContext.Current.Session["s_userid"].ToString(), hddcompanyCode.Value);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='jobList.aspx';", true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/jobList.aspx");
        }
    }
}