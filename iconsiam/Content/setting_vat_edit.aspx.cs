﻿using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class setting_vat_edit : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        setting_vatDLL Serv = new setting_vatDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtvat.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtvat.ClientID + "')");

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    //bind_default();
                    bind_data();
                }
            }
        }

        //protected void bind_default()
        //{
        //    var data = Serv.getCompany();
        //    if (data.Rows.Count != 0)
        //    {
        //        ddlcompany.DataTextField = "CompanyNameTh";
        //        ddlcompany.DataValueField = "Companycode";
        //        ddlcompany.DataSource = data;
        //        ddlcompany.DataBind();
        //    }
        //    else
        //    {
        //        ddlcompany.DataSource = null;
        //        ddlcompany.DataBind();

        //    }
        //}


        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var vat = Serv.getVatByID(Request.QueryString["id"].ToString());
                if (vat.Rows.Count != 0)
                {
                    hddcompanyCode.Value = vat.Rows[0]["Companycode"].ToString();
                    txtcompany.Text = vat.Rows[0]["CompanyNameTh"].ToString();
                    txtvat.Text = vat.Rows[0]["vat"].ToString();
                }
                else
                {
                    Response.Redirect("~/vatList.aspx");

                }
            }
            else
            {
                Response.Redirect("~/vatList.aspx");

            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {

            if (txtvat.Text == "")
            {
                POPUPMSG("กรุณากรอกค่า VAT เพื่อใช้ในการคำนวน");
                return;
            }

            if (Convert.ToDecimal(txtvat.Text) <= 0)
            {
                POPUPMSG("VAT ห้ามมีค่าน้อยกว่าเท่ากับ 0");
                return;
            }


            //var data = Serv.GetConfirmDate();
            //if (data.Rows.Count != 0)
            //{
            Serv.update_vat(txtvat.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["id"].ToString());

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='vatList.aspx';", true);
            //update
            //}
            //else
            //{
            //Serv.Insert_vat(txtvat.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //    HttpContext.Current.Session["s_userid"].ToString(), ddlcompany.SelectedValue);
            ////insert
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='vatList.aspx';", true);
            //}
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/vatList.aspx");
        }





    }
}