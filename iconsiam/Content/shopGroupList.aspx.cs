﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class shopGroupList : System.Web.UI.Page
    {
        shopGroupListDLL Serv = new shopGroupListDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_rep();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;
            //}




            //if (HttpContext.Current.Session["s_com_code"] != null)
            //{

            //}
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var comp = Serv.GetCompany(comcode_);
            if (comp.Rows.Count != 0)
            {
                ddlcompany.DataTextField = "CompanyNameTH";
                ddlcompany.DataValueField = "CompanyCode";
                ddlcompany.DataSource = comp;
                ddlcompany.DataBind();
            }
            else
            {
                ddlcompany.DataSource = null;
                ddlcompany.DataBind();

            }
            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

        }
        protected void bind_rep()
        {
            string[] shopgrop = txtshopgroupname.Text.Split(',');

            var indust = Serv.GetShopGroupByName(ddlcompany.SelectedValue, ddlstatus.SelectedValue, shopgrop,
                HttpContext.Current.Session["s_com_code"].ToString(), HttpContext.Current.Session["role"].ToString());
            if (indust.Rows.Count != 0)
            {
                GridView_List.DataSource = indust;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_shopgroup.aspx");

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            Response.Redirect("~/edit_shopgroup.aspx?id=" + hdd_id.Value);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/shopGroupList.aspx");
        }
    }
}