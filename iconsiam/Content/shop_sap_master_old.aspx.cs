﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class shop_sap_master_old : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        sentmail mail = new sentmail();
        shop_sap_masterDLL Serv = new shop_sap_masterDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtopendate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    txtClosedate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);

                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {

            ddlflag_shop_group.Items.Insert(0, new ListItem("ไม่ใช่", "n"));
            ddlflag_shop_group.Items.Insert(1, new ListItem("ใช่", "y"));

            ddlshop_type.Items.Insert(0, new ListItem("ร้านค้าผู้เช่า", "S"));
            ddlshop_type.Items.Insert(1, new ListItem("อีเว้นสถานที่", "E"));
            ddlshop_type.Items.Insert(2, new ListItem("KIOSK", "K"));

            ddl_User_keyIntype.Items.Insert(0, new ListItem("ฝ่ายบัญชีลูกหนี้", "ar"));
            ddl_User_keyIntype.Items.Insert(1, new ListItem("ร้านค้าผู้เช่า", "te"));

            ddl_keyin_type.Items.Insert(0, new ListItem("Daily", "Daily"));
            ddl_keyin_type.Items.Insert(1, new ListItem("Weekly", "Weekly"));
            ddl_keyin_type.Items.Insert(2, new ListItem("Monthly", "Monthly"));

            ddl_keyin_type_sub.Items.Clear();
            ddl_keyin_type_sub.Items.Insert(0, new ListItem("End of day", "end_of_day"));
            ddl_keyin_type_sub.Items.Insert(1, new ListItem("Next day", "next_day"));

            ddlhh.Items.Clear();
            ddlmm.Items.Clear();

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

            ddlrecord_type.Items.Clear();
            ddlrecord_type.Items.Insert(0, new ListItem("ยอดขายสินค้ารวม VAT", "prod_sale"));
            ddlrecord_type.Items.Insert(1, new ListItem("ยอดขายสินค้ารวม VAT + ยอดบริการรวม VAT", "prod_service"));
            ddlrecord_type.Items.Insert(2, new ListItem("ยอดขายสินค้ารวม VAT + Service Charge รวม VAT", "prod_ser_charge"));

            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            for (int bb = 0; bb <= 23; bb++)
            {
                ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
            }
            ddlhh.SelectedValue = "00";

            for (int bb = 0; bb <= 59; bb++)
            {
                ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
            }
            ddlmm.SelectedValue = "00";

            //var IndusGroup = Serv.GetIndustry_group();
            //if (IndusGroup.Rows.Count != 0)
            //{
            //    ddlindustry_group.DataTextField = "IndustryGroupNameTH";
            //    ddlindustry_group.DataValueField = "id";
            //    ddlindustry_group.DataSource = IndusGroup;
            //    ddlindustry_group.DataBind();
            //}
            //ddlindustry_group.Items.Insert(0, new ListItem("", ""));


            var fine_admin = Serv.GetStaff("ar", HttpContext.Current.Session["s_com_code"].ToString());
            if (fine_admin.Rows.Count != 0)
            {
                ddlicon_staff.DataTextField = "name";
                ddlicon_staff.DataValueField = "userid";
                ddlicon_staff.DataSource = fine_admin;
                ddlicon_staff.DataBind();
            }
            else
            {
                ddlicon_staff.DataSource = null;
                ddlicon_staff.DataBind();

            }
            ddlicon_staff.Items.Insert(0, new ListItem("", ""));


            var fine_ae = Serv.GetStaff("ae", HttpContext.Current.Session["s_com_code"].ToString());
            if (fine_ae.Rows.Count != 0)
            {
                ddlae_staff.DataTextField = "name";
                ddlae_staff.DataValueField = "userid";
                ddlae_staff.DataSource = fine_ae;
                ddlae_staff.DataBind();
            }
            else
            {
                ddlae_staff.DataSource = null;
                ddlae_staff.DataBind();

            }
            ddlae_staff.Items.Insert(0, new ListItem("", ""));


            var groupLoca = Serv.GetGroupLocation(HttpContext.Current.Session["s_com_code"].ToString());
            if (groupLoca.Rows.Count != 0)
            {
                ddlgroup_location.DataTextField = "GrouplocationNameEN";
                ddlgroup_location.DataValueField = "id";
                ddlgroup_location.DataSource = groupLoca;
                ddlgroup_location.DataBind();
            }
            else
            {
                ddlgroup_location.DataSource = null;
                ddlgroup_location.DataBind();

            }
            ddlgroup_location.Items.Insert(0, new ListItem("", ""));


            var contractType = Serv.GetContract_type(HttpContext.Current.Session["s_com_code"].ToString());
            if (contractType.Rows.Count != 0)
            {
                ddlcontracttype.DataTextField = "ContractTypeNameEn";
                ddlcontracttype.DataValueField = "id";
                ddlcontracttype.DataSource = contractType;
                ddlcontracttype.DataBind();
            }
            else
            {
                ddlcontracttype.DataSource = null;
                ddlcontracttype.DataBind();

            }
            ddlcontracttype.Items.Insert(0, new ListItem("", ""));

            var contractLeasing = Serv.GetCategory_leasing(HttpContext.Current.Session["s_com_code"].ToString());
            if (contractLeasing.Rows.Count != 0)
            {
                ddlcategory_leasing.DataTextField = "CategoryleasingNameEN";
                ddlcategory_leasing.DataValueField = "id";
                ddlcategory_leasing.DataSource = contractLeasing;
                ddlcategory_leasing.DataBind();
            }
            else
            {
                ddlcategory_leasing.DataSource = null;
                ddlcategory_leasing.DataBind();

            }
            ddlcategory_leasing.Items.Insert(0, new ListItem("", ""));
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var cont = Serv.GetContractDetail(Request.QueryString["id"].ToString());
                if (cont.Rows.Count != 0)
                {
                    //====================== Binding SAP Info ===============================

                    HttpContext.Current.Session["create_user"] = cont.Rows[0]["smart_create_user"].ToString();


                    lbheader.Text = "New Shop (SAP)";

                    var orent = Serv.GetRetalObj(Request.QueryString["id"].ToString());
                    if (orent.Rows.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('สัญญาของร้านค้านี้มีข้อมูลไม่ครบถ้วน');window.location ='contractList_SAP.aspx';", true);
                        return;
                    }

                    txtbuilding.Text = orent.Rows[0]["BuildingName"].ToString();
                    hddBuildingCode.Value = orent.Rows[0]["BuildingCode"].ToString();

                    txtcollection_type.Text = cont.Rows[0]["SubTypeName"].ToString();
                    txtcompanyname.Text = cont.Rows[0]["CompanyNameTH"].ToString();
                    HttpContext.Current.Session["comcode"] = cont.Rows[0]["CompanyCode"].ToString();
                    txtcondition_gp.Text = cont.Rows[0]["SalesTypeName"].ToString();
                    txtcontractcode.Text = cont.Rows[0]["ContractNumber"].ToString();
                    txtcustomercode.Text = cont.Rows[0]["BusinessPartnerCode"].ToString();
                    txtcustomername.Text = cont.Rows[0]["BusinessPartnerName"].ToString();

                    txtfloor.Text = orent.Rows[0]["FloorDescription"].ToString();
                    txtindustry.Text = cont.Rows[0]["IndustryNameEN"].ToString();
                    txtroomno.Text = cont.Rows[0]["smart_room_no"].ToString();
                    txtroomtype.Text = orent.Rows[0]["UsageTypeName"].ToString();

                    hddUsageTypeName.Value = orent.Rows[0]["UsageTypeName"].ToString();

                    txtsapxintract_type.Text = cont.Rows[0]["ContractTypeDescription"].ToString();

                    txtSAP_contract_end.Text = Convert.ToDateTime(cont.Rows[0]["ContractEndDate"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    txtClosedate.Text = Convert.ToDateTime(cont.Rows[0]["ContractEndDate"].ToString()).ToString("dd/MM/yyyy", EngCI);



                    txtSAP_contract_start.Text = Convert.ToDateTime(cont.Rows[0]["ContractStartDate"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    txtopendate.Text = Convert.ToDateTime(cont.Rows[0]["ContractStartDate"].ToString()).ToString("dd/MM/yyyy", EngCI);


                    txtshopname.Text = cont.Rows[0]["ShopName"].ToString();
                    txttotalarea.Text = cont.Rows[0]["smart_sqm"].ToString();

                    if (cont.Rows[0]["smart_object_rental_code"].ToString() == "K")
                    {
                        ddlshop_type.SelectedValue = "K";
                    }

                    if (txtshopname.Text.Length > 2)
                    {
                        txtusername.Text = cont.Rows[0]["ShopName"].ToString().Replace(".", "").Replace("\'", "").Replace("@", "").ToLower().Substring(0, 3) + txtcustomercode.Text;
                    }
                    else
                    {
                        txtusername.Text = cont.Rows[0]["ShopName"].ToString().Replace(".", "").Replace("\'", "").Replace("@", "").ToLower().Substring(0, 2) + txtcustomercode.Text;
                    }




                    //====================== Binding Exist Info ===============================
                    if (cont.Rows[0]["smart_shop_open"].ToString() != "")
                    {
                        lbheader.Text = "My Shop (SAP)";
                        Button1.Visible = false;

                        txtopendate.Text = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    }
                    if (cont.Rows[0]["smart_shop_close"].ToString() != "")
                    {
                        txtClosedate.Text = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    }
                    if (cont.Rows[0]["smart_group_location"].ToString() != "")
                    {
                        ddlgroup_location.SelectedValue = cont.Rows[0]["smart_group_location"].ToString();
                    }
                    if (cont.Rows[0]["smart_contract_type"].ToString() != "")
                    {
                        ddlcontracttype.SelectedValue = cont.Rows[0]["smart_contract_type"].ToString();
                    }
                    if (cont.Rows[0]["smart_Industry_group"].ToString() != "")
                    {
                        //ddlindustry_group.Value = cont.Rows[0]["smart_Industry_group"].ToString();


                        var indus = Serv.GetIndustry_groupByID(cont.Rows[0]["smart_Industry_group"].ToString());
                        if (indus.Rows.Count != 0)
                        {
                            ddlindustry_group.Value = indus.Rows[0]["id"].ToString();
                            txtddlindustry_group.Text = indus.Rows[0]["IndustryGroupNameEN"].ToString();
                        }
                        else
                        {
                            ddlindustry_group.Value = "";
                            txtddlindustry_group.Text = "";
                            POPUPMSG("กรุณาเพิ่ม Industry Group ที่ Master Data");
                        }


                    }
                    else
                    {
                        if (cont.Rows[0]["IndustryCode"].ToString() == "")
                        {
                            //POPUPMSG("กรุณาเช็คข้อมูล Industry ใน SAP");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('กรุณาเช็คข้อมูล Industry ใน SAP');window.location ='" + HttpContext.Current.Session["lastUri"].ToString() + "';", true);

                            return;
                        }
                        var indus = Serv.GetIndustry_groupByCode(cont.Rows[0]["IndustryCode"].ToString().Substring(0, 2),
                            HttpContext.Current.Session["s_com_code"].ToString());
                        if (indus.Rows.Count != 0)
                        {
                            ddlindustry_group.Value = indus.Rows[0]["id"].ToString();
                            txtddlindustry_group.Text = indus.Rows[0]["IndustryGroupNameEN"].ToString();
                        }
                        else
                        {
                            ddlindustry_group.Value = "";
                            txtddlindustry_group.Text = "";
                            POPUPMSG("กรุณา Mapping Industry Group เข้า Company ที่ดูแล");
                        }
                    }
                    if (cont.Rows[0]["smart_Category_leasing"].ToString() != "")
                    {
                        ddlcategory_leasing.SelectedValue = cont.Rows[0]["smart_Category_leasing"].ToString();
                    }
                    if (cont.Rows[0]["smart_isShop_group"].ToString() != "")
                    {
                        string aa = cont.Rows[0]["smart_isShop_group"].ToString();
                        ddlflag_shop_group.SelectedValue = aa;
                    }
                    if (ddlflag_shop_group.SelectedValue == "y")
                    {
                        ddlshopgroup.Visible = true;

                        var shopgrup = Serv.GetShopGroup(HttpContext.Current.Session["s_com_code"].ToString());
                        if (shopgrup.Rows.Count != 0)
                        {

                            ddlshopgroup.DataTextField = "ShopGroupNameTH";
                            ddlshopgroup.DataValueField = "id";
                            ddlshopgroup.DataSource = shopgrup;
                            ddlshopgroup.DataBind();
                        }
                    }
                    else
                    {
                        ddlshopgroup.Visible = false;
                    }
                    if (cont.Rows[0]["smart_shop_group_id"].ToString() != "")
                    {
                        ddlshopgroup.SelectedValue = cont.Rows[0]["smart_shop_group_id"].ToString();
                    }
                    if (cont.Rows[0]["smart_object_rental_code"].ToString() != "")
                    {
                        ddlshop_type.SelectedValue = cont.Rows[0]["smart_object_rental_code"].ToString();
                    }
                    if (cont.Rows[0]["smart_icon_staff_id"].ToString() != "")
                    {
                        ddlicon_staff.SelectedValue = cont.Rows[0]["smart_icon_staff_id"].ToString();
                    }
                    if (cont.Rows[0]["smart_icon_ae_id"].ToString() != "")
                    {
                        ddlae_staff.SelectedValue = cont.Rows[0]["smart_icon_ae_id"].ToString();
                    }
                    if (cont.Rows[0]["smart_contract_status"].ToString() != "")
                    {
                        ddlstatus.SelectedValue = cont.Rows[0]["smart_contract_status"].ToString();
                    }
                    if (cont.Rows[0]["smart_record_keyin_type"].ToString() != "")
                    {
                        ddlrecord_type.SelectedValue = cont.Rows[0]["smart_record_keyin_type"].ToString();
                    }

                    var contact = Serv.GetContactPoint(Request.QueryString["id"].ToString());
                    if (contact.Rows.Count != 0)
                    {
                        GridView_List.DataSource = contact;
                        GridView_List.DataBind();
                    }
                    else
                    {
                        GridView_List.DataSource = null;
                        GridView_List.DataBind();
                    }

                    var email = Serv.getEmailCC(Request.QueryString["id"].ToString());
                    if (email.Rows.Count != 0)
                    {
                        GridView2.DataSource = email;
                        GridView2.DataBind();
                    }
                    else
                    {
                        GridView2.DataSource = null;
                        GridView2.DataBind();
                    }                    

                    if (cont.Rows[0]["smart_record_type"].ToString() != "")
                    {
                        ddl_keyin_type.SelectedValue = cont.Rows[0]["smart_record_type"].ToString();

                        if (ddl_keyin_type.SelectedValue == "Daily")
                        {
                            ddl_keyin_type_sub.Items.Clear();
                            ddl_keyin_type_sub.Items.Insert(0, new ListItem("End of day", "end_of_day"));
                            ddl_keyin_type_sub.Items.Insert(1, new ListItem("Next day", "next_day"));

                            ddlhh.Items.Clear();
                            ddlmm.Items.Clear();

                            if (cont.Rows[0]["smart_record_daily_type"].ToString() != "")
                            {
                                ddl_keyin_type_sub.SelectedValue = cont.Rows[0]["smart_record_daily_type"].ToString();

                                if (ddl_keyin_type_sub.SelectedValue == "next_day")
                                {

                                    for (int bb = 0; bb <= 23; bb++)
                                    {
                                        ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                                    }
                                    ddlhh.SelectedValue = "10";

                                    for (int bb = 0; bb <= 59; bb++)
                                    {
                                        ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                                    }
                                    ddlmm.SelectedValue = "00";

                                }
                                else
                                {
                                    for (int bb = 0; bb <= 23; bb++)
                                    {
                                        ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                                    }
                                    ddlhh.SelectedValue = "00";

                                    for (int bb = 0; bb <= 59; bb++)
                                    {
                                        ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                                    }
                                    ddlmm.SelectedValue = "00";
                                }

                            }

                            if (cont.Rows[0]["smart_record_daily_time"].ToString() != "")
                            {
                                ddlhh.SelectedValue = cont.Rows[0]["smart_record_daily_time"].ToString().Substring(0, 2);
                            }
                            if (cont.Rows[0]["smart_record_daily_time"].ToString() != "")
                            {
                                ddlmm.SelectedValue = cont.Rows[0]["smart_record_daily_time"].ToString().Substring(3, 2);
                            }


                        }
                        else if (ddl_keyin_type.SelectedValue == "Weekly")
                        {
                            ddl_keyin_type_sub.Items.Clear();
                            ddl_keyin_type_sub.Items.Insert(0, new ListItem("Mon", "Mon"));
                            ddl_keyin_type_sub.Items.Insert(1, new ListItem("Tue", "Tue"));
                            ddl_keyin_type_sub.Items.Insert(2, new ListItem("Wed", "Wed"));
                            ddl_keyin_type_sub.Items.Insert(3, new ListItem("Thu", "Thu"));
                            ddl_keyin_type_sub.Items.Insert(4, new ListItem("Fri", "Fri"));
                            ddl_keyin_type_sub.Items.Insert(5, new ListItem("Sat", "Sat"));
                            ddl_keyin_type_sub.Items.Insert(6, new ListItem("Sun", "Sun"));

                            ddlhh.Items.Clear();
                            ddlmm.Items.Clear();



                            for (int bb = 0; bb <= 23; bb++)
                            {
                                ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                            }
                            ddlhh.SelectedValue = "10";

                            for (int bb = 0; bb <= 59; bb++)
                            {
                                ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                            }
                            ddlmm.SelectedValue = "00";


                            if (cont.Rows[0]["smart_record_weekly_date"].ToString() != "")
                            {
                                ddl_keyin_type_sub.SelectedValue = cont.Rows[0]["smart_record_weekly_date"].ToString();
                            }

                            if (cont.Rows[0]["smart_record_weekly_time"].ToString() != "")
                            {
                                ddlhh.SelectedValue = cont.Rows[0]["smart_record_weekly_time"].ToString().Substring(0, 2);
                            }
                            if (cont.Rows[0]["smart_record_weekly_time"].ToString() != "")
                            {
                                ddlmm.SelectedValue = cont.Rows[0]["smart_record_weekly_time"].ToString().Substring(3, 2);
                            }



                        }
                        else if (ddl_keyin_type.SelectedValue == "Monthly")
                        {
                            ddl_keyin_type_sub.Items.Clear();
                            ddl_keyin_type_sub.Visible = false;
                            txtkeydate.Visible = true;
                            txtkeydate.Text = "01";

                            ddlhh.Items.Clear();
                            ddlmm.Items.Clear();


                            for (int bb = 0; bb <= 23; bb++)
                            {
                                ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                            }
                            ddlhh.SelectedValue = "10";

                            for (int bb = 0; bb <= 59; bb++)
                            {
                                ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                            }
                            ddlmm.SelectedValue = "00";


                            if (cont.Rows[0]["smart_record_monthly_month"].ToString() != "")
                            {
                                txtkeydate.Text = cont.Rows[0]["smart_record_monthly_month"].ToString();
                            }

                            if (cont.Rows[0]["smart_record_monthly_time"].ToString() != "")
                            {
                                ddlhh.SelectedValue = cont.Rows[0]["smart_record_monthly_time"].ToString().Substring(0, 2);
                            }
                            if (cont.Rows[0]["smart_record_monthly_time"].ToString() != "")
                            {
                                ddlmm.SelectedValue = cont.Rows[0]["smart_record_monthly_time"].ToString().Substring(3, 2);
                            }
                        }
                    }

                }
                else
                {
                    Response.Redirect("~/" + HttpContext.Current.Session["lastUri"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/" + HttpContext.Current.Session["lastUri"].ToString());
            }
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }      

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/" + HttpContext.Current.Session["lastUri"].ToString());
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HttpContext.Current.Session["hdd_id_contact"] = hdd_id.Value;

            var u = Serv.getContact_pointByID(hdd_id.Value);
            if (u.Rows.Count != 0)
            {
                txtname_edit.Text = u.Rows[0]["name"].ToString();
                txttel1_edit.Text = u.Rows[0]["tel1"].ToString();
                txttel2_edit.Text = u.Rows[0]["tel2"].ToString();
                txtemail_edit.Text = u.Rows[0]["email"].ToString();
                ddlmain_contact_edit.SelectedValue = u.Rows[0]["IsMain"].ToString();
                ddlstatus_contact_point_edit.SelectedValue = u.Rows[0]["status"].ToString();

                Panel1.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = true;

            }
            else
            {
                Panel1.Visible = true;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
            }


            //var contact = Serv.GetContactPoint(Request.QueryString["id"].ToString());
            //if (contact.Rows.Count != 0)
            //{
            //    GridView_List.DataSource = contact;
            //    GridView_List.DataBind();
            //}
            //else
            //{
            //    GridView_List.DataSource = null;
            //    GridView_List.DataBind();
            //}

        }

        protected void ddlflag_shop_group_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlflag_shop_group.SelectedValue == "n")
            {
                ddlshopgroup.Visible = false;

                ddlshopgroup.DataSource = null;
                ddlshopgroup.DataBind();

            }
            else
            {
                ddlshopgroup.Visible = true;
                var shopgrup = Serv.GetShopGroup(HttpContext.Current.Session["comcode"].ToString());
                if (shopgrup.Rows.Count != 0)
                {

                    ddlshopgroup.DataTextField = "ShopGroupNameTH";
                    ddlshopgroup.DataValueField = "id";
                    ddlshopgroup.DataSource = shopgrup;
                    ddlshopgroup.DataBind();
                }

            }
        }

        protected void ddl_keyin_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_keyin_type.SelectedValue == "Daily")
            {
                ddl_keyin_type_sub.Visible = true;
                txtkeydate.Visible = false;

                ddl_keyin_type_sub.Items.Clear();
                ddl_keyin_type_sub.Items.Insert(0, new ListItem("End of day", "end_of_day"));
                ddl_keyin_type_sub.Items.Insert(1, new ListItem("Next day", "next_day"));

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();


                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "00";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
            else if (ddl_keyin_type.SelectedValue == "Weekly")
            {

                ddl_keyin_type_sub.Visible = true;
                txtkeydate.Visible = false;

                ddl_keyin_type_sub.Items.Clear();
                ddl_keyin_type_sub.Items.Insert(0, new ListItem("Mon", "Mon"));
                ddl_keyin_type_sub.Items.Insert(1, new ListItem("Tue", "Tue"));
                ddl_keyin_type_sub.Items.Insert(2, new ListItem("Wed", "Wed"));
                ddl_keyin_type_sub.Items.Insert(3, new ListItem("Thu", "Thu"));
                ddl_keyin_type_sub.Items.Insert(4, new ListItem("Fri", "Fri"));
                ddl_keyin_type_sub.Items.Insert(5, new ListItem("Sat", "Sat"));
                ddl_keyin_type_sub.Items.Insert(6, new ListItem("Sun", "Sun"));

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();


                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
            else if (ddl_keyin_type.SelectedValue == "Monthly")
            {
                ddl_keyin_type_sub.Items.Clear();


                ddl_keyin_type_sub.Visible = false;
                txtkeydate.Visible = true;


                txtkeydate.Text = "01";

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
        }

        protected void ddl_keyin_type_sub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_keyin_type_sub.SelectedValue == "end_of_day")
            {
                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                ddlhh.Items.Insert(0, new ListItem("00", "00"));

                ddlmm.Items.Insert(0, new ListItem("00", "00"));

            }
            else
            {
                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
        }       

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtusername.Text != "")
            {
                var u = Serv.GetAccountByUsername(txtusername.Text);
                var u2 = Serv.GetUserByUsername(txtusername.Text);
                if (u.Rows.Count != 0)
                {

                    POPUPMSG("Username ของท่านซ้ำกันในระบบ");
                }
                else if (u2.Rows.Count != 0)
                {
                    POPUPMSG("Username ของท่านซ้ำกันในระบบ");
                }
                else
                {

                    dateconvert cc = new dateconvert();
                    Serv.CreateAccount(HttpContext.Current.Session["con_code"].ToString(), txtusername.Text, ConfigurationManager.AppSettings["default_password"], HttpContext.Current.Session["s_userid"].ToString(),
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), Convert.ToDateTime(cc.con_date(txtClosedate.Text)).AddMonths(2).ToString("yyyy-MM-dd"), "sap", "y");

                    var contact = Serv.GetContactPoint_distinct_mail(Request.QueryString["id"].ToString());
                    if (contact.Rows.Count != 0)
                    {
                        string email = "";

                        for (int j = 0; j < contact.Rows.Count; j++)
                        {
                            if (contact.Rows[0]["IsMain"].ToString() == "y" && contact.Rows[0]["status"].ToString() == "1")
                            {
                                email = email + contact.Rows[j]["email"].ToString() + ",";
                            }
                        }

                        email = email.Substring(0, email.Length - 1);


                        mail.CallMail_new_account(email, "ระบบ Tenant Sales Data Collection  : New User  ร้าน " + txtshopname.Text + " ห้อง " +
                                         txtroomno.Text, "Tenant Sales Data Collection", txtshopname.Text, "", txtcompanyname.Text, "ชื่อร้าน", "ชื่อบริษัท",
                                     "ชั้น", "ห้อง", "Username", "Password", "", txtshopname.Text,
                                     txtcustomername.Text, txtfloor.Text,
                                     txtroomno.Text, txtusername.Text, ConfigurationManager.AppSettings["default_password"], "icon", Request.QueryString["id"].ToString(),"tenant");
                    }



                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='" + HttpContext.Current.Session["lastUri"].ToString() + "';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            var contract = Serv.GetSmart_Contract(ddlcompany.SelectedValue, txtshopname_copy.Text, txtcontractnumber_copy.Text);
            if (contract.Rows.Count != 0)
            {
                if (contract.Rows[0]["status"].ToString() == "Inactive")
                {
                    POPUPMSG("Contract Number นี้ไม่สามารถคัดลอกได้");
                    return;
                }
                else
                {

                    GridView1.DataSource = contract;
                    GridView1.DataBind();
                }
            }
            else
            {
                if (txtcontractnumber_copy.Text != "")
                {
                    POPUPMSG("ไม่พบ Contract Number : " + txtcontractnumber_copy.Text + " ที่ท่านค้นหา");
                }

                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }

        protected void btnedit_Click1(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            dateconvert cc = new dateconvert();
            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_ShopName = (HiddenField)row.FindControl("hdd_ShopName");

            var cont = Serv.GetContractDetail(hdd_id.Value);

            HttpContext.Current.Session["oldContract"] = hdd_id.Value;

            if (cont.Rows.Count != 0)
            {
                if (cont.Rows[0]["smart_record_type"].ToString() == "Daily")
                {

                    Serv.UpdateContact_Master_copy(cont.Rows[0]["UpdateDate"].ToString(),

                                      cont.Rows[0]["smart_room_no"].ToString(),

                                      cont.Rows[0]["smart_sqm"].ToString(),

                                      cont.Rows[0]["smart_update_date"].ToString(),

                                      cont.Rows[0]["smart_update_user"].ToString(),

                                      cont.Rows[0]["smart_isShop_group"].ToString(),

                                      cont.Rows[0]["smart_shop_group_id"].ToString(),

                                      cont.Rows[0]["smart_shop_open"].ToString(),

                                      cont.Rows[0]["smart_shop_close"].ToString(),

                                      cont.Rows[0]["smart_group_location"].ToString(),

                                      cont.Rows[0]["smart_contract_type"].ToString(),

                                      cont.Rows[0]["smart_Category_leasing"].ToString(),

                                      cont.Rows[0]["smart_Industry_group"].ToString(),

                                      cont.Rows[0]["smart_icon_staff_id"].ToString(),

                                      cont.Rows[0]["smart_record_person_type"].ToString(),

                                      cont.Rows[0]["smart_record_type"].ToString(),

                                      "0", //cont.Rows[0]["smart_contract_status"].ToString(),

                                      cont.Rows[0]["smart_record_keyin_type"].ToString(),

                                      cont.Rows[0]["smart_object_rental_code"].ToString(),

                                      cont.Rows[0]["smart_icon_ae_id"].ToString(),

                                         cont.Rows[0]["smart_record_daily_type"].ToString(), cont.Rows[0]["smart_record_daily_time"].ToString(),
                                         cont.Rows[0]["ContractNumber"].ToString(), Request.QueryString["id"].ToString(), hddBuildingCode.Value, txtfloor.Text,
                                         hddUsageTypeName.Value);

                }
                else if (cont.Rows[0]["smart_record_type"].ToString() == "Monthly")
                {
                    Serv.UpdateContact_Master_copy(cont.Rows[0]["UpdateDate"].ToString(),

                                     cont.Rows[0]["smart_room_no"].ToString(),

                                     cont.Rows[0]["smart_sqm"].ToString(),

                                     cont.Rows[0]["smart_update_date"].ToString(),

                                     cont.Rows[0]["smart_update_user"].ToString(),

                                     cont.Rows[0]["smart_isShop_group"].ToString(),

                                     cont.Rows[0]["smart_shop_group_id"].ToString(),

                                     cont.Rows[0]["smart_shop_open"].ToString(),

                                     cont.Rows[0]["smart_shop_close"].ToString(),

                                     cont.Rows[0]["smart_group_location"].ToString(),

                                     cont.Rows[0]["smart_contract_type"].ToString(),

                                     cont.Rows[0]["smart_Category_leasing"].ToString(),

                                     cont.Rows[0]["smart_Industry_group"].ToString(),

                                     cont.Rows[0]["smart_icon_staff_id"].ToString(),

                                     cont.Rows[0]["smart_record_person_type"].ToString(),

                                     cont.Rows[0]["smart_record_type"].ToString(),

                                      "0", //cont.Rows[0]["smart_contract_status"].ToString(),

                                     cont.Rows[0]["smart_record_keyin_type"].ToString(),

                                     cont.Rows[0]["smart_object_rental_code"].ToString(),

                                     cont.Rows[0]["smart_icon_ae_id"].ToString(),

                                        cont.Rows[0]["smart_record_monthly_month"].ToString(), cont.Rows[0]["smart_record_monthly_time"].ToString(),
                                        cont.Rows[0]["ContractNumber"].ToString(), Request.QueryString["id"].ToString(), hddBuildingCode.Value, txtfloor.Text,
                                        hddUsageTypeName.Value);

                }
                else if (cont.Rows[0]["smart_record_type"].ToString() == "Weekly")
                {
                    Serv.UpdateContact_Master_copy(cont.Rows[0]["UpdateDate"].ToString(),

                                    cont.Rows[0]["smart_room_no"].ToString(),

                                    cont.Rows[0]["smart_sqm"].ToString(),

                                    cont.Rows[0]["smart_update_date"].ToString(),

                                    cont.Rows[0]["smart_update_user"].ToString(),

                                    cont.Rows[0]["smart_isShop_group"].ToString(),

                                    cont.Rows[0]["smart_shop_group_id"].ToString(),

                                    cont.Rows[0]["smart_shop_open"].ToString(),

                                    cont.Rows[0]["smart_shop_close"].ToString(),

                                    cont.Rows[0]["smart_group_location"].ToString(),

                                    cont.Rows[0]["smart_contract_type"].ToString(),

                                    cont.Rows[0]["smart_Category_leasing"].ToString(),

                                    cont.Rows[0]["smart_Industry_group"].ToString(),

                                    cont.Rows[0]["smart_icon_staff_id"].ToString(),

                                    cont.Rows[0]["smart_record_person_type"].ToString(),

                                    cont.Rows[0]["smart_record_type"].ToString(),

                                    "0", //cont.Rows[0]["smart_contract_status"].ToString(),

                                    cont.Rows[0]["smart_record_keyin_type"].ToString(),

                                    cont.Rows[0]["smart_object_rental_code"].ToString(),

                                    cont.Rows[0]["smart_icon_ae_id"].ToString(),

                                       cont.Rows[0]["smart_record_weekly_date"].ToString(), cont.Rows[0]["smart_record_weekly_time"].ToString(),
                                       cont.Rows[0]["ContractNumber"].ToString(), Request.QueryString["id"].ToString(), hddBuildingCode.Value, txtfloor.Text,
                                       hddUsageTypeName.Value);
                }

            }

            var acc = Serv.GetAccount(cont.Rows[0]["ContractNumber"].ToString());
            if (acc.Rows.Count != 0)
            {
                for (int x = 0; x < acc.Rows.Count; x++)
                {
                    Serv.CreateAccount(txtcontractcode.Text, acc.Rows[x]["username"].ToString(), acc.Rows[x]["password"].ToString(),
                        HttpContext.Current.Session["s_userid"].ToString(),
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), Convert.ToDateTime(cc.con_date(txtClosedate.Text)).AddMonths(2).ToString("yyyy-MM-dd"), "sap", "n");
                }
            }

            var cont_point = Serv.GetContactPoint(cont.Rows[0]["ContractNumber"].ToString());
            if (cont_point.Rows.Count != 0)
            {
                for (int xxx = 0; xxx < cont_point.Rows.Count; xxx++)
                {
                    Serv.Insert_contact_point(txtcontractcode.Text, cont_point.Rows[xxx]["name"].ToString(), cont_point.Rows[xxx]["email"].ToString(),
                        cont_point.Rows[xxx]["tel1"].ToString(), cont_point.Rows[xxx]["tel2"].ToString(), cont_point.Rows[xxx]["IsMain"].ToString(), "0");
                }
            }


            // =============== Select Account / Contract ========================= //
            GridView1.DataSource = null;
            GridView1.DataBind();

            POPUPMSG("บันทึกเรียบร้อย");

            txtopendate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
            txtClosedate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);

            bind_data();

            Panel1.Visible = true;
            Panel3.Visible = false;
            Panel4.Visible = false;
            Panel5.Visible = false;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='shop_sap_master.aspx?id=" + Request.QueryString["id"].ToString() + "';", true);



        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = true;
            Panel5.Visible = false;
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel3.Visible = false;
            Panel4.Visible = false;
            Panel5.Visible = false;

        }       

        protected void Button3_Click1(object sender, EventArgs e)
        {            
            Panel1.Visible = true;
            Panel4.Visible = false;
            Panel5.Visible = false;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Button btnedit = (Button)(e.Row.FindControl("btnedit"));
                HiddenField hdd_ShopName = (HiddenField)(e.Row.FindControl("hdd_ShopName"));

                btnedit.Attributes.Add("onclick", "return confirm('ท่านต้องการคัดลอกสัญญา จากร้าน" + hdd_ShopName.Value + " ไปร้าน" + txtshopname.Text + " หรือไม่??');");
            }
        }
       
        protected void btndelMailCC_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            Serv.DeleteMailCC(hdd_id.Value);

            var email = Serv.getEmailCC(Request.QueryString["id"].ToString());
            if (email.Rows.Count != 0)
            {
                GridView2.DataSource = email;
                GridView2.DataBind();
            }
            else
            {
                GridView2.DataSource = null;
                GridView2.DataBind();
            }
        }

       
    }

}