﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class shop_smart_master : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        shop_smart_masterDLL Serv = new shop_smart_masterDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtopendate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    txtClosedate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);

                    txtSAP_contract_start.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    txtSAP_contract_end.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);

                    bind_default();
                    //bind_data();
                }
            }
        }

        protected void bind_default()
        {

            ddlmain_contact.Items.Insert(0, new ListItem("ใช่", "y"));
            ddlmain_contact.Items.Insert(1, new ListItem("ไม่ใช่", "n"));

            ddlmain_contact_edit.Items.Insert(0, new ListItem("ใช่", "y"));
            ddlmain_contact_edit.Items.Insert(1, new ListItem("ไม่ใช่", "n"));

            ddlstatus_contact_point.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus_contact_point.Items.Insert(1, new ListItem("Inactive", "0"));

            ddlstatus_contact_point_edit.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus_contact_point_edit.Items.Insert(1, new ListItem("Inactive", "0"));

            ddlflag_shop_group.Items.Insert(0, new ListItem("ไม่ใช่", "n"));
            ddlflag_shop_group.Items.Insert(1, new ListItem("ใช่", "y"));

            ddl_User_keyIntype.Items.Insert(0, new ListItem("ฝ่ายบัญชีลูกหนี้", "ar"));
            ddl_User_keyIntype.Items.Insert(1, new ListItem("ร้านค้าผู้เช่า", "te"));

            ddlshop_type.Items.Insert(0, new ListItem("", ""));
            ddlshop_type.Items.Insert(1, new ListItem("ร้านค้าผู้เช่า", "S"));
            ddlshop_type.Items.Insert(2, new ListItem("อีเว้นสถานที่", "E"));
            ddlshop_type.Items.Insert(3, new ListItem("KIOSK", "K"));

            ddl_keyin_type.Items.Insert(0, new ListItem("Daily", "Daily"));
            ddl_keyin_type.Items.Insert(1, new ListItem("Weekly", "Weekly"));
            ddl_keyin_type.Items.Insert(2, new ListItem("Monthly", "Monthly"));

            ddl_keyin_type_sub.Items.Clear();
            ddl_keyin_type_sub.Items.Insert(0, new ListItem("End of day", "end_of_day"));
            ddl_keyin_type_sub.Items.Insert(1, new ListItem("Next day", "next_day"));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

            ddlrecord_type.Items.Clear();
            ddlrecord_type.Items.Insert(0, new ListItem("ยอดขายสินค้ารวม VAT", "prod_sale"));
            ddlrecord_type.Items.Insert(1, new ListItem("ยอดขายสินค้ารวม VAT + ยอดบริการรวม VAT", "prod_service"));
            ddlrecord_type.Items.Insert(2, new ListItem("ยอดขายสินค้ารวม VAT + Service Charge รวม VAT", "prod_ser_charge"));

            ddlhh.Items.Clear();
            ddlmm.Items.Clear();


            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany_1.DataTextField = "CompanyNameTH";
                    ddlcompany_1.DataValueField = "CompanyCode";
                    ddlcompany_1.DataSource = comp;
                    ddlcompany_1.DataBind();
                }
                else
                {
                    ddlcompany_1.DataSource = null;
                    ddlcompany_1.DataBind();

                }
            }
            ddlcompany_1.Items.Insert(0, new ListItem("Company", ""));

            for (int bb = 0; bb <= 23; bb++)
            {
                ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
            }
            ddlhh.SelectedValue = "00";

            for (int bb = 0; bb <= 59; bb++)
            {
                ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
            }
            ddlmm.SelectedValue = "00";

            var IndusGroup = Serv.GetIndustry_group(HttpContext.Current.Session["s_com_code"].ToString());
            if (IndusGroup.Rows.Count != 0)
            {
                ddlindustry_group.DataTextField = "IndustryGroupNameEN";
                ddlindustry_group.DataValueField = "id";
                ddlindustry_group.DataSource = IndusGroup;
                ddlindustry_group.DataBind();
            }
            ddlindustry_group.Items.Insert(0, new ListItem("", ""));

            //if (HttpContext.Current.Session["s_com_code"] != null)
            //{
            //    var fine_admin = Serv.GetStaff("ar", HttpContext.Current.Session["s_com_code"].ToString());
            //    if (fine_admin.Rows.Count != 0)
            //    {
            //        ddlicon_staff.DataTextField = "name";
            //        ddlicon_staff.DataValueField = "userid";
            //        ddlicon_staff.DataSource = fine_admin;
            //        ddlicon_staff.DataBind();
            //    }
            //    else
            //    {
            //        ddlicon_staff.DataSource = null;
            //        ddlicon_staff.DataBind();

            //    }
            //    ddlicon_staff.Items.Insert(0, new ListItem("", ""));
            //}


            //if (HttpContext.Current.Session["s_com_code"] != null)
            //{
            //    var fine_ae = Serv.GetStaff("ae", HttpContext.Current.Session["s_com_code"].ToString());
            //    if (fine_ae.Rows.Count != 0)
            //    {
            //        ddlae_staff.DataTextField = "name";
            //        ddlae_staff.DataValueField = "userid";
            //        ddlae_staff.DataSource = fine_ae;
            //        ddlae_staff.DataBind();
            //    }
            //    else
            //    {
            //        ddlae_staff.DataSource = null;
            //        ddlae_staff.DataBind();

            //    }
            //    ddlae_staff.Items.Insert(0, new ListItem("", ""));
            //}         

            var groupLoca = Serv.GetGroupLocation(HttpContext.Current.Session["s_com_code"].ToString());
            if (groupLoca.Rows.Count != 0)
            {
                ddlgroup_location.DataTextField = "GrouplocationNameEN";
                ddlgroup_location.DataValueField = "id";
                ddlgroup_location.DataSource = groupLoca;
                ddlgroup_location.DataBind();
            }
            else
            {
                ddlgroup_location.DataSource = null;
                ddlgroup_location.DataBind();

            }
            ddlgroup_location.Items.Insert(0, new ListItem("", ""));


            var contractType = Serv.GetContract_type(HttpContext.Current.Session["s_com_code"].ToString());
            if (contractType.Rows.Count != 0)
            {
                ddlcontracttype.DataTextField = "ContractTypeNameEn";
                ddlcontracttype.DataValueField = "id";
                ddlcontracttype.DataSource = contractType;
                ddlcontracttype.DataBind();
            }
            else
            {
                ddlcontracttype.DataSource = null;
                ddlcontracttype.DataBind();

            }
            ddlcontracttype.Items.Insert(0, new ListItem("", ""));

            var contractLeasing = Serv.GetCategory_leasing(HttpContext.Current.Session["s_com_code"].ToString());
            if (contractLeasing.Rows.Count != 0)
            {
                ddlcategory_leasing.DataTextField = "CategoryleasingNameEN";
                ddlcategory_leasing.DataValueField = "id";
                ddlcategory_leasing.DataSource = contractLeasing;
                ddlcategory_leasing.DataBind();
            }
            else
            {
                ddlcategory_leasing.DataSource = null;
                ddlcategory_leasing.DataBind();

            }
            ddlcategory_leasing.Items.Insert(0, new ListItem("", ""));

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void clear_add_member()
        {
            txtname.Text = "";
            txtemail.Text = "";
            txttel1.Text = "";
            txttel2.Text = "";
            ddlmain_contact.SelectedIndex = 0;
        }




        protected void ddlshop_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            var seq = Serv.GetShop_type_seq(ddlshop_type.SelectedValue);
            if (seq.Rows.Count != 0)
            {
                i = Convert.ToInt32(seq.Rows[0]["type_seq"].ToString()) + 1;
                string number = ddlshop_type.SelectedValue + i.ToString("000000#");
                txtcontractcode.Text = number;

            }
            else
            {
                i = 1;
                string number = ddlshop_type.SelectedValue + i.ToString("000000#");
                txtcontractcode.Text = number;
            }
            //// Insert Contract Number 

            Serv.Insert_COntract(txtcontractcode.Text, HttpContext.Current.Session["s_userid"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), i.ToString(),
                ddlshop_type.SelectedValue);
            HttpContext.Current.Session["con_code"] = txtcontractcode.Text;

        }

        protected void ddlcompany_1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var build = Serv.GetBuildingByCompanyCode(ddlcompany_1.SelectedValue);
            if (build.Rows.Count != 0)
            {
                ddlbuilding.DataTextField = "BuildingNameEN";
                ddlbuilding.DataValueField = "BuildingCode";
                ddlbuilding.DataSource = build;
                ddlbuilding.DataBind();

            }
            else
            {
                ddlbuilding.DataSource = null;
                ddlbuilding.DataBind();
            }
            ddlbuilding.Items.Insert(0, new ListItem("", ""));

            if (ddlcompany_1.SelectedValue != "")
            {
                var fine_admin = Serv.GetStaff("ar", ddlcompany_1.SelectedValue);
                if (fine_admin.Rows.Count != 0)
                {
                    ddlicon_staff.DataTextField = "name";
                    ddlicon_staff.DataValueField = "userid";
                    ddlicon_staff.DataSource = fine_admin;
                    ddlicon_staff.DataBind();
                }
                else
                {
                    ddlicon_staff.DataSource = null;
                    ddlicon_staff.DataBind();

                }
                ddlicon_staff.Items.Insert(0, new ListItem("", ""));
            }


            if (ddlcompany_1.SelectedValue != "")
            {
                var fine_ae = Serv.GetStaff("ae", ddlcompany_1.SelectedValue
                    );
                if (fine_ae.Rows.Count != 0)
                {
                    ddlae_staff.DataTextField = "name";
                    ddlae_staff.DataValueField = "userid";
                    ddlae_staff.DataSource = fine_ae;
                    ddlae_staff.DataBind();
                }
                else
                {
                    ddlae_staff.DataSource = null;
                    ddlae_staff.DataBind();

                }
                ddlae_staff.Items.Insert(0, new ListItem("", ""));
            }

        }

        protected void ddlbuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            var floor = Serv.GetFloorByBuildingCode(ddlbuilding.SelectedValue);
            if (floor.Rows.Count != 0)
            {
                ddlfloor.DataTextField = "FloorDescripition";
                ddlfloor.DataValueField = "FloorDescripition";
                ddlfloor.DataSource = floor;
                ddlfloor.DataBind();

            }
            else
            {
                ddlfloor.DataSource = null;
                ddlfloor.DataBind();
            }
            ddlfloor.Items.Insert(0, new ListItem("", ""));
        }

        protected void ddlflag_shop_group_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlflag_shop_group.SelectedValue == "n")
            {
                ddlshopgroup.Visible = false;

                ddlshopgroup.DataSource = null;
                ddlshopgroup.DataBind();

            }
            else
            {
                ddlshopgroup.Visible = true;
                var shopgrup = Serv.GetShopGroup(ddlcompany_1.SelectedValue);
                if (shopgrup.Rows.Count != 0)
                {

                    ddlshopgroup.DataTextField = "ShopGroupNameTH";
                    ddlshopgroup.DataValueField = "id";
                    ddlshopgroup.DataSource = shopgrup;
                    ddlshopgroup.DataBind();
                }

            }
        }

        protected void btnadd_user_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["con_code"] = txtcontractcode.Text;
            Panel1.Visible = false;
            Panel2.Visible = true;
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            HttpContext.Current.Session["hdd_id_contact"] = hdd_id.Value;

            var u = Serv.getContact_pointByID(hdd_id.Value);
            if (u.Rows.Count != 0)
            {
                txtname_edit.Text = u.Rows[0]["name"].ToString();
                txttel1_edit.Text = u.Rows[0]["tel1"].ToString();
                txttel2_edit.Text = u.Rows[0]["tel2"].ToString();
                txtemail_edit.Text = u.Rows[0]["email"].ToString();
                ddlmain_contact_edit.SelectedValue = u.Rows[0]["IsMain"].ToString();
                ddlstatus_contact_point_edit.SelectedValue = u.Rows[0]["status"].ToString();

                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = true;

            }
            else
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
            }
        }

        protected void ddl_keyin_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_keyin_type.SelectedValue == "Daily")
            {
                ddl_keyin_type_sub.Visible = true;
                txtkeydate.Visible = false;

                ddl_keyin_type_sub.Items.Clear();
                ddl_keyin_type_sub.Items.Insert(0, new ListItem("End of day", "end_of_day"));
                ddl_keyin_type_sub.Items.Insert(1, new ListItem("Next day", "next_day"));

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "00";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
            else if (ddl_keyin_type.SelectedValue == "Weekly")
            {

                ddl_keyin_type_sub.Visible = true;
                txtkeydate.Visible = false;

                ddl_keyin_type_sub.Items.Clear();
                ddl_keyin_type_sub.Items.Insert(0, new ListItem("Mon", "Mon"));
                ddl_keyin_type_sub.Items.Insert(1, new ListItem("Tue", "Tue"));
                ddl_keyin_type_sub.Items.Insert(2, new ListItem("Wed", "Wed"));
                ddl_keyin_type_sub.Items.Insert(3, new ListItem("Thu", "Thu"));
                ddl_keyin_type_sub.Items.Insert(4, new ListItem("Fri", "Fri"));
                ddl_keyin_type_sub.Items.Insert(5, new ListItem("Sat", "Sat"));
                ddl_keyin_type_sub.Items.Insert(6, new ListItem("Sun", "Sun"));

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
            else if (ddl_keyin_type.SelectedValue == "Monthly")
            {
                ddl_keyin_type_sub.Items.Clear();


                ddl_keyin_type_sub.Visible = false;
                txtkeydate.Visible = true;


                txtkeydate.Text = "01";

                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
        }

        protected void ddl_keyin_type_sub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_keyin_type_sub.SelectedValue == "end_of_day")
            {
                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "00";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";

            }
            else
            {
                ddlhh.Items.Clear();
                ddlmm.Items.Clear();

                for (int bb = 0; bb <= 23; bb++)
                {
                    ddlhh.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlhh.SelectedValue = "10";

                for (int bb = 0; bb <= 59; bb++)
                {
                    ddlmm.Items.Insert(bb, new ListItem(bb.ToString("00"), bb.ToString("00")));
                }
                ddlmm.SelectedValue = "00";
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            dateconvert cc = new dateconvert();
            string d2 = cc.con_date(txtSAP_contract_start.Text);
            string d1 = cc.con_date(txtopendate.Text);
            int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
            if (result < 0)
            {
                txtopendate.Focus();
                POPUPMSG("วันที่เปิดร้านห้ามน้อยกว่าวันที่เริ่มสัญญา");
                return;
            }

            d2 = cc.con_date(txtSAP_contract_end.Text); //Convert.ToDateTime(txtSAP_contract_end.Text).ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            d1 = cc.con_date(txtClosedate.Text); //Convert.ToDateTime(txtClosedate.Text).ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
            if (result > 0)
            {
                txtClosedate.Focus();
                POPUPMSG("วันที่ปิดร้านห้ามมากกว่าวันที่สิ้นสุดสัญญา");
                return;
            }



            if (txtcontractcode.Text == "") { txtcontractcode.Focus(); POPUPMSG("กรุณากรอก--เลขที่สัญญา--"); return; }
            //if (txtcustomercode.Text == "") { txtcustomercode.Focus(); POPUPMSG("กรุณากรอก--Customer Code--"); return; }
            if (txtcustomername.Text == "") { txtcustomername.Focus(); POPUPMSG("กรุณากรอก--Customer Name--"); return; }
            if (txtshopname.Text == "") { txtshopname.Focus(); POPUPMSG("กรุณากรอก--Shop Name--"); return; }
            if (txtroomno.Text == "") { txtroomno.Focus(); POPUPMSG("กรุณากรอก--Room Number--"); return; }
            if (txttotalarea.Text == "") { txttotalarea.Focus(); POPUPMSG("กรุณากรอก--ขนาดพื้นที่เช่ารวม--"); return; }
            if (txtcondition_gp.Text == "") { txtcondition_gp.Focus(); POPUPMSG("กรุณากรอก--เงื่อนไขยอดขาย--"); return; }

            if (ddlshop_type.SelectedValue == "") { ddlshop_type.Focus(); POPUPMSG("กรุณากรอก--ประเภทร้านค้าสถานที่--"); return; }
            if (ddlcompany_1.SelectedValue == "") { ddlcompany_1.Focus(); POPUPMSG("กรุณากรอก--Company--"); return; }
            if (ddlbuilding.SelectedValue == "") { ddlbuilding.Focus(); POPUPMSG("กรุณากรอก--Building--"); return; }
            if (ddlfloor.SelectedValue == "") { ddlfloor.Focus(); POPUPMSG("กรุณากรอก--Floor--"); return; }
            if (ddlgroup_location.SelectedValue == "") { ddlgroup_location.Focus(); POPUPMSG("กรุณากรอก--Group Location--"); return; }
            if (ddlcontracttype.SelectedValue == "") { ddlcontracttype.Focus(); POPUPMSG("กรุณากรอก--Contract Type--"); return; }
            if (ddlcategory_leasing.SelectedValue == "") { ddlcategory_leasing.Focus(); POPUPMSG("กรุณากรอก--Category Leasing--"); return; }
            if (ddlindustry_group.SelectedValue == "") { ddlindustry_group.Focus(); POPUPMSG("กรุณากรอก--Industry group--"); return; }
            if (ddlicon_staff.SelectedValue == "") { ddlicon_staff.Focus(); POPUPMSG("กรุณากรอก--ICON STAFF--"); return; }
            if (ddlae_staff.SelectedValue == "") { ddlae_staff.Focus(); POPUPMSG("กรุณากรอก--AE Staff--"); return; }

            if (ddlflag_shop_group.SelectedValue == "y")
            {
                if (ddlshopgroup.SelectedValue == "")
                {
                    ddlshopgroup.Focus();
                    POPUPMSG("กรุณากรอก--Shop Group--");
                    return;
                }
            }

            if (GridView_List.Rows.Count != 0)
            {
                if (ddl_keyin_type.SelectedValue == "Daily")
                {
                    Serv.UpdateContact_Master(ddlcompany_1.SelectedValue, cc.con_date(txtSAP_contract_start.Text), cc.con_date(txtSAP_contract_end.Text),
                        txtshopname.Text, txtcustomercode.Text, txtcustomername.Text, txtcondition_gp.Text, ddlbuilding.SelectedValue, ddlfloor.SelectedValue,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        txtroomno.Text, txttotalarea.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        HttpContext.Current.Session["s_userid"].ToString(), ddlflag_shop_group.SelectedValue, ddlshopgroup.SelectedValue,
                        cc.con_date(txtopendate.Text), cc.con_date(txtClosedate.Text), ddlgroup_location.SelectedValue, ddlcontracttype.SelectedValue,
                        ddlcategory_leasing.SelectedValue, ddlindustry_group.SelectedValue, ddlicon_staff.SelectedValue,
                        ddl_User_keyIntype.SelectedValue, ddl_keyin_type.SelectedValue, ddlstatus.SelectedValue,
                        ddlrecord_type.SelectedValue, ddlshop_type.SelectedValue, ddlae_staff.SelectedValue, ddl_keyin_type_sub.SelectedValue,
                        ddlhh.SelectedValue + ":" + ddlmm.SelectedValue, txtcontractcode.Text);
                }
                else if (ddl_keyin_type.SelectedValue == "Weekly")
                {
                    Serv.UpdateContact_Master(ddlcompany_1.SelectedValue, cc.con_date(txtSAP_contract_start.Text), cc.con_date(txtSAP_contract_end.Text),
                        txtshopname.Text, txtcustomercode.Text, txtcustomername.Text, txtcondition_gp.Text, ddlbuilding.SelectedValue, ddlfloor.SelectedValue,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        txtroomno.Text, txttotalarea.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        HttpContext.Current.Session["s_userid"].ToString(), ddlflag_shop_group.SelectedValue, ddlshopgroup.SelectedValue,
                        cc.con_date(txtopendate.Text), cc.con_date(txtClosedate.Text), ddlgroup_location.SelectedValue, ddlcontracttype.SelectedValue,
                        ddlcategory_leasing.SelectedValue, ddlindustry_group.SelectedValue, ddlicon_staff.SelectedValue,
                        ddl_User_keyIntype.SelectedValue, ddl_keyin_type.SelectedValue, ddlstatus.SelectedValue,
                        ddlrecord_type.SelectedValue, ddlshop_type.SelectedValue, ddlae_staff.SelectedValue, ddl_keyin_type_sub.SelectedValue,
                        ddlhh.SelectedValue + ":" + ddlmm.SelectedValue, txtcontractcode.Text);
                }
                else if (ddl_keyin_type.SelectedValue == "Monthly")
                {
                    Serv.UpdateContact_Master(ddlcompany_1.SelectedValue, cc.con_date(txtSAP_contract_start.Text), cc.con_date(txtSAP_contract_end.Text),
                        txtshopname.Text, txtcustomercode.Text, txtcustomername.Text, txtcondition_gp.Text, ddlbuilding.SelectedValue, ddlfloor.SelectedValue,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        txtroomno.Text, txttotalarea.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                        HttpContext.Current.Session["s_userid"].ToString(), ddlflag_shop_group.SelectedValue, ddlshopgroup.SelectedValue,
                        cc.con_date(txtopendate.Text), cc.con_date(txtClosedate.Text), ddlgroup_location.SelectedValue, ddlcontracttype.SelectedValue,
                        ddlcategory_leasing.SelectedValue, ddlindustry_group.SelectedValue, ddlicon_staff.SelectedValue,
                        ddl_User_keyIntype.SelectedValue, ddl_keyin_type.SelectedValue, ddlstatus.SelectedValue,
                        ddlrecord_type.SelectedValue, ddlshop_type.SelectedValue, ddlae_staff.SelectedValue, txtkeydate.Text,
                        ddlhh.SelectedValue + ":" + ddlmm.SelectedValue, txtcontractcode.Text);
                }


                if (ddl_User_keyIntype.SelectedValue == "ar")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='" + HttpContext.Current.Session["lastUri"].ToString() + "';", true);
                }
                else
                {
                    var account = Serv.GetAccount(txtcontractcode.Text);
                    if (account.Rows.Count == 0)
                    {
                        Panel1.Visible = false;
                        Panel2.Visible = false;
                        Panel3.Visible = true;

                        txtusername.Text = txtshopname.Text.Replace(".", "").Replace("\'", "").Replace("@", "").ToLower().Substring(0, 3) +
                            txtcustomercode.Text;

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='" + HttpContext.Current.Session["lastUri"].ToString() + "';", true);

                    }
                }



            }
            else
            {
                POPUPMSG("กรุณาเพิ่ม Contract Point (ของผู้เช่า)");
            }


        }

        protected void btnadd_mail_cc_Click(object sender, EventArgs e)
        {
            if (txtcontractcode.Text != "")
            {
                HttpContext.Current.Session["con_code"] = txtcontractcode.Text;
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel4.Visible = false;
                Panel6.Visible = true;
            }
            else
            {
                POPUPMSG("กรุณากรอก--เลขที่สัญญา--");
                return;
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {

            var con = Serv.GetSmartContract(HttpContext.Current.Session["s_userid"].ToString());
            if (con.Rows.Count != 0)
            {
                for (int i = 0; i < con.Rows.Count; i++)
                {
                    Serv.Del_Contact_Contract(con.Rows[i]["ContractNumber"].ToString());
                }
            }
            Serv.Del_ContractSmartCollectio(HttpContext.Current.Session["s_userid"].ToString());


            Response.Redirect("~/" + HttpContext.Current.Session["lastUri"].ToString());

        }

        protected void btnaddmember_Click(object sender, EventArgs e)
        {

            if (txtname.Text != "" && txtemail.Text != "" && txttel1.Text != "")
            {

                var co = Serv.GetContactPoint_Existing(txtname.Text, txttel1.Text, txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtcontractcode.Text);
                if (co.Rows.Count != 0)
                {
                    POPUPMSG("!!! Contact Point ซ้ำ !!!");
                    return;
                }
                else
                {
                    Serv.Insert_contact_point(HttpContext.Current.Session["con_code"].ToString(), txtname.Text, txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txttel1.Text, txttel2.Text, ddlmain_contact.SelectedValue,
                    ddlstatus_contact_point.SelectedValue);

                    var contact = Serv.GetContactPoint(HttpContext.Current.Session["con_code"].ToString());
                    if (contact.Rows.Count != 0)
                    {
                        GridView_List.DataSource = contact;
                        GridView_List.DataBind();
                    }
                    else
                    {
                        GridView_List.DataSource = null;
                        GridView_List.DataBind();
                    }


                    clear_add_member();

                    Panel1.Visible = true;
                    Panel2.Visible = false;

                }

            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }

        }

        protected void btncancelmember_Click(object sender, EventArgs e)
        {
            clear_add_member();
            Panel1.Visible = true;
            Panel2.Visible = false;
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {

            sentmail mail = new sentmail();
            dateconvert cc = new dateconvert();
            if (txtusername.Text != "")
            {
                var u = Serv.GetAccountByUsername(txtusername.Text);
                var u2 = Serv.GetUserByUsername(txtusername.Text);
                if (u.Rows.Count != 0)
                {

                    POPUPMSG("Username ของท่านซ้ำกันในระบบ");
                }
                else if (u2.Rows.Count != 0)
                {
                    POPUPMSG("Username ของท่านซ้ำกันในระบบ");
                }
                else
                {
                    var contact = Serv.GetContactPoint_distinct_mail(txtcontractcode.Text);
                    if (contact.Rows.Count != 0)
                    {
                        string email = "";

                        for (int j = 0; j < contact.Rows.Count; j++)
                        {
                            if (contact.Rows[0]["IsMain"].ToString() == "y" && contact.Rows[0]["status"].ToString() == "1")
                            {
                                email = email + contact.Rows[j]["email"].ToString() + ",";
                            }
                        }

                        email = email.Substring(0, email.Length - 1);

                        mail.CallMail_new_account(email, "ระบบ Tenant Sales Data Collection  : New User  ร้าน " + txtshopname.Text + " ห้อง " +
                                         txtroomno.Text, "Tenant Sales Data Collection", txtshopname.Text, "", ddlcompany_1.SelectedItem.Text, "ชื่อร้าน", "ชื่อบริษัท",
                                     "ชั้น", "ห้อง", "Username", "Password", "", txtshopname.Text,
                                     txtcustomername.Text, ddlfloor.SelectedValue,
                                     txtroomno.Text, txtusername.Text, ConfigurationManager.AppSettings["default_password"], "icon", txtcontractcode.Text,"tenant");
                    }

                    Serv.CreateAccount(txtcontractcode.Text, txtusername.Text, ConfigurationManager.AppSettings["default_password"], HttpContext.Current.Session["s_userid"].ToString(),
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), Convert.ToDateTime(cc.con_date(txtClosedate.Text)).AddMonths(3).ToString("yyyy-MM-dd"), "smart");

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='" + HttpContext.Current.Session["lastUri"].ToString() + "';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (txtname_edit.Text != "" && txtemail_edit.Text != "" && txttel1_edit.Text != "")
            {
                var co = Serv.GetContactPoint_Existing(txtname.Text, txttel1.Text, txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtcontractcode.Text);
                if (co.Rows.Count != 0)
                {
                    POPUPMSG("!!! Contact Point ซ้ำ !!!");
                    return;
                }
                else
                {
                    Serv.Update_contact_point(txtname_edit.Text, txtemail_edit.Text, txttel1_edit.Text, txttel2_edit.Text, ddlmain_contact_edit.SelectedValue, ddlstatus_contact_point_edit.SelectedValue,
                       HttpContext.Current.Session["hdd_id_contact"].ToString());

                    var contact = Serv.GetContactPoint(txtcontractcode.Text);
                    if (contact.Rows.Count != 0)
                    {
                        GridView_List.DataSource = contact;
                        GridView_List.DataBind();
                    }
                    else
                    {
                        GridView_List.DataSource = null;
                        GridView_List.DataBind();
                    }


                    clear_add_member();

                    Panel1.Visible = true;
                    Panel3.Visible = false;
                    Panel2.Visible = false;
                    Panel4.Visible = false;

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            clear_add_member();

            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = false;
        }

        protected void btnsaveaddmail_Click(object sender, EventArgs e)
        {
            if (txtnamecc.Text != "" && txtemailcc.Text != "")
            {
                try
                {
                    var eMailValidator = new System.Net.Mail.MailAddress(txtemailcc.Text.Replace("'", "''").Replace("&", "'+char(38)+'"));

                    Serv.InsertMailCC(txtcontractcode.Text, txtemailcc.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ",""), txtnamecc.Text,
                        HttpContext.Current.Session["s_userid"].ToString());

                    var email = Serv.getEmailCC(txtcontractcode.Text);
                    if (email.Rows.Count != 0)
                    {
                        GridView2.DataSource = email;
                        GridView2.DataBind();
                    }
                    else
                    {
                        GridView2.DataSource = null;
                        GridView2.DataBind();
                    }


                    clear_add_ccmail();

                    Panel1.Visible = true;
                    Panel6.Visible = false;


                }
                catch (FormatException ex)
                {
                    POPUPMSG("Wrong Email Format");
                    return;
                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        protected void btncanceladdmail_Click(object sender, EventArgs e)
        {

            clear_add_ccmail();

            Panel1.Visible = true;
            Panel6.Visible = false;
        }

        protected void btndelMailCC_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            Serv.DeleteMailCC(hdd_id.Value);

            var email = Serv.getEmailCC(txtcontractcode.Text);
            if (email.Rows.Count != 0)
            {
                GridView2.DataSource = email;
                GridView2.DataBind();
            }
            else
            {
                GridView2.DataSource = null;
                GridView2.DataBind();
            }
        }

        protected void clear_add_ccmail()
        {
            txtnamecc.Text = "";
            txtemailcc.Text = "";
        }

    }
}