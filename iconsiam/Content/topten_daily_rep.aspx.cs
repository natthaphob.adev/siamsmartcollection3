﻿using ClosedXML.Excel;
using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class topten_daily_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        daily_repDLL Serv = new daily_repDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }

        dateconvert cc = new dateconvert();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);
                    txtenddate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);

                    bind_default();
                    bind_rep();
                }
            }
        }

        protected void bind_default()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var cl = Serv.GetCategory_leasing(comcode_);
            if (cl.Rows.Count != 0)
            {
                ddlcatagorLeasing.DataTextField = "CategoryleasingNameTH";
                ddlcatagorLeasing.DataValueField = "id";
                ddlcatagorLeasing.DataSource = cl;
                ddlcatagorLeasing.DataBind();
            }
            else
            {
                ddlcatagorLeasing.DataSource = null;
                ddlcatagorLeasing.DataBind();

            }
            ddlcatagorLeasing.Items.Insert(0, new ListItem("Select Category Leasing", ""));

            var ig = Serv.GetIndustry_group(comcode_);
            if (ig.Rows.Count != 0)
            {
                ddlindustrygroup.DataTextField = "IndustryGroupNameTH";
                ddlindustrygroup.DataValueField = "id";
                ddlindustrygroup.DataSource = ig;
                ddlindustrygroup.DataBind();
            }
            else
            {
                ddlindustrygroup.DataSource = null;
                ddlindustrygroup.DataBind();

            }
            ddlindustrygroup.Items.Insert(0, new ListItem("Select Industry Group", ""));


            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }
            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;
            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void bind_rep()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("d0");
            dt.Columns.Add("d1");
            dt.Columns.Add("d2");
            dt.Columns.Add("d3");
            dt.Columns.Add("d4");
            dt.Columns.Add("d5");
            dt.Columns.Add("d6");
            dt.Columns.Add("d_total", typeof(decimal));

            double date_diff = (Convert.ToDateTime(cc.con_date(txtenddate.Text)) - Convert.ToDateTime(cc.con_date(txtstartdate.Text))).TotalDays;

            if (date_diff < 0)
            {
                POPUPMSG("Start Date ห้ามมากกว่า End Date");
                return;
            }
            else
            {
                string Company = "";

                if (ddlcompany.SelectedValue == "")
                {
                    Company = HttpContext.Current.Session["s_com_code"].ToString();
                }
                else
                {
                    Company = "'" + ddlcompany.SelectedValue + "'";
                }

                for (int j = 0; j <= date_diff; j++)
                {

                    var rep = Serv.GetRep2(Convert.ToDateTime(cc.con_date(txtstartdate.Text)).AddDays(j).ToString("yyyy-MM-dd"),
                        Convert.ToDateTime(cc.con_date(txtstartdate.Text)).AddDays(j).ToString("yyyy-MM-dd"),
                        ddlcatagorLeasing.SelectedValue, ddlindustrygroup.SelectedValue, Company, ddlstatus.SelectedValue);

                    if (rep.Rows.Count != 0)
                    {
                        decimal sum = 0;

                        for (int i = 0; i < rep.Rows.Count; i++)
                        {
                            DataRow row1 = dt.NewRow();
                            row1["d0"] = i + 1;
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d5"] = rep.Rows[i]["smart_room_no"].ToString();
                            row1["d6"] = rep.Rows[i]["smart_sqm"].ToString();
                            row1["d_total"] = Convert.ToDecimal(rep.Rows[i]["total_ex_var"].ToString()).ToString("#,##0.00");
                            sum = sum + Convert.ToDecimal(rep.Rows[i]["total_ex_var"].ToString());
                            dt.Rows.Add(row1);

                            if (i == rep.Rows.Count - 1)
                            {
                                row1 = dt.NewRow();
                                row1["d0"] = "";
                                row1["d1"] = "";
                                row1["d2"] = "";
                                row1["d3"] = "";
                                row1["d4"] = "";
                                row1["d5"] = "";
                                row1["d6"] = "";
                                row1["d_total"] = sum;

                                dt.Rows.Add(row1);
                            }
                        }

                    }
                }


                if (dt.Rows.Count != 0)
                {
                    //dt.DefaultView.Sort = "d1,d0";

                    GridView_List.DataSource = dt;
                    GridView_List.DataBind();

                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }

            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            bind_rep();

            if (GridView1.Rows.Count != 0)
            {

                //Response.Clear();
                //Response.AddHeader("content-disposition", "attachment; filename = top10daily_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                ////Response.AddHeader("content-disposition", "attachment;filename=Export1.xlsx");
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                ////Response.ContentEncoding = System.Text.Encoding.Unicode;
                ////Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                //System.IO.StringWriter sw = new System.IO.StringWriter();
                //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                //GridView1.RenderControl(hw);
                //string headerTable = @"<Table>" +
                //                "<tr align='center'><td colspan='8'>Tenant Sales Data Collection</td></tr>" +
                //                "<tr align='center'><td colspan='8'> TOP TEN DAILY " + txtstartdate.Text + " - " + txtenddate.Text + "</td></tr>" +
                //                "<tr align='center'><td colspan='8'></td></tr>" +
                //                "</Table>";
                //Response.Write(headerTable);
                //Response.Write(sw.ToString());
                //Response.End();

                //===================================================================

                var aCode = 65;

                DataTable dt = new DataTable();
                for (int i = 0; i < GridView1.Columns.Count; i++)
                {
                    dt.Columns.Add(GridView1.Columns[i].HeaderText);
                }
                foreach (GridViewRow row_ in GridView1.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < GridView1.Columns.Count; j++)
                    {
                        if (j < 5)
                        {
                            dr[GridView1.Columns[j].HeaderText] = "'" + row_.Cells[j].Text.Replace("&nbsp;", "").Replace("&amp;", "&");
                        }
                        else
                        {
                            dr[GridView1.Columns[j].HeaderText] = row_.Cells[j].Text.Replace("&nbsp;", "").Replace("&amp;", "&").Replace(",", "");
                        }
                    }

                    dt.Rows.Add(dr);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("top10daily_report" + DateTime.Now.ToString("yyyyMMdd"));

                    var wsReportNameHeaderRange_1 = ws.Range(string.Format("A{0}:{1}{0}", 1, Char.ConvertFromUtf32(aCode + dt.Columns.Count - 1)));
                    wsReportNameHeaderRange_1.Style.Font.Bold = false;
                    wsReportNameHeaderRange_1.Merge();
                    wsReportNameHeaderRange_1.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wsReportNameHeaderRange_1.Value = "Tenant Sales Data Collection";

                    var wsReportNameHeaderRange_2 = ws.Range(string.Format("A{0}:{1}{0}", 2, Char.ConvertFromUtf32(aCode + dt.Columns.Count - 1)));
                    wsReportNameHeaderRange_2.Style.Font.Bold = false;
                    wsReportNameHeaderRange_2.Merge();
                    wsReportNameHeaderRange_2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wsReportNameHeaderRange_2.Value = "TOP TEN DAILY " + txtstartdate.Text.Replace("-", "/") + " - " + txtenddate.Text.Replace("-", "/");


                    ws.Row(2).InsertRowsBelow(1);
                    ws.Row(3).Style.Border.OutsideBorder = XLBorderStyleValues.None;
                    ws.Row(3).Style.Border.RightBorder = XLBorderStyleValues.None;
                    ws.Row(3).Style.Border.LeftBorder = XLBorderStyleValues.None;

                    int rowIndex = 4;
                    int columnIndex = 0;


                    foreach (DataColumn column in dt.Columns)
                    {
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Value = column.ColumnName;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Font.Bold = true;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        columnIndex++;
                    }
                    rowIndex++;

                    foreach (DataRow row_ in dt.Rows)
                    {
                        int valueCount = 0;
                        foreach (object rowValue in row_.ItemArray)
                        {
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue;
                            if (rowValue.ToString() != "0.00")
                            {
                                ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "#,###0.00";
                            }
                            else
                            {
                                ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "0.00";
                            }
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                            valueCount++;
                        }
                        rowIndex++;
                    }

                    ws.Columns("A", "Z").AdjustToContents();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=top10daily_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }


                }


                #region export_1_0
                //foreach (TableCell cell in GridView1.HeaderRow.Cells)
                //{
                //    dt.Columns.Add(cell.Text.Replace("&nbsp;", ""));
                //}
                //foreach (GridViewRow row in GridView1.Rows)
                //{
                //    dt.Rows.Add();
                //    for (int i = 0; i < row.Cells.Count; i++)
                //    {
                //        dt.Rows[dt.Rows.Count - 1][i] = row.Cells[i].Text.Replace("&nbsp;", "").Replace("&amp;", "&");
                //    }
                //}

                //using (XLWorkbook wb = new XLWorkbook())
                //{

                //    wb.Worksheets.Add(dt);

                //    Response.Clear();
                //    Response.Buffer = true;
                //    Response.Charset = "";

                //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AddHeader("content-disposition", "attachment;filename=top10daily_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                //    Response.ContentEncoding = System.Text.Encoding.Unicode;

                //    using (MemoryStream MyMemoryStream = new MemoryStream())
                //    {
                //        wb.SaveAs(MyMemoryStream);
                //        MyMemoryStream.WriteTo(Response.OutputStream);
                //        Response.Flush();
                //        Response.End();
                //    }
                //}
                #endregion


            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/topten_daily_rep.aspx");
        }

        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdd_d3 = (HiddenField)(e.Row.FindControl("hdd_d3"));
                if (hdd_d3.Value == "")
                {
                    e.Row.BackColor = System.Drawing.Color.LightPink;
                }//

            }
        }

        //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        HiddenField hdd_d3 = (HiddenField)(e.Row.FindControl("hdd_d3"));
        //        if (hdd_d3.Value == "")
        //        {
        //            e.Row.BackColor = System.Drawing.Color.LightPink;
        //        }//

        //    }
        //}
    }
}