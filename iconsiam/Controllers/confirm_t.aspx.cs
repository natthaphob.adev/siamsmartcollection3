﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam.Controllers
{
    public partial class confirm_t : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LogService l = new LogService();
                CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
                jobscheduleDLL Serv = new jobscheduleDLL();
                sentmail mail = new sentmail();
                dateconvert date_con = new dateconvert();


                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "confirm_t");

                var com = Serv.getCompany();
                if (com.Rows.Count != 0)
                {
                    for (int j = 0; j < com.Rows.Count; j++)
                    {
                        var job = Serv.getTime("jobconfirm", com.Rows[j]["CompanyCode"].ToString());
                        if (job.Rows.Count != 0)
                        {
                            try
                            {
                                string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
                                string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

                                int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

                                if (result > 0)
                                {
                                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                                    {
                                        int d_1 = Convert.ToInt32(Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2)) - 1;

                                        string t1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "15";
                                        string t2 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";


                                        mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
                                            t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t_adhoc");

                                        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddMonths(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));

                                    }


                                }

                            }
                            catch (Exception ex)
                            {
                                Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddMonths(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t_adhoc");
                            }
                        }




















                        var job2 = Serv.getTime("jobconfirm2", com.Rows[j]["CompanyCode"].ToString());
                        if (job2.Rows.Count != 0)
                        {
                            try
                            {
                                string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
                                string d2 = Convert.ToDateTime(job2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

                                int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

                                if (result > 0)
                                {
                                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(job2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                                    {
                                        //int d_1 = Convert.ToInt32(c.Rows[0]["second_confirm"]) - 1;
                                        int d_1 = Convert.ToInt32(Convert.ToDateTime(job2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2)) - 1;

                                        var job_ = Serv.getTime("jobconfirm", com.Rows[j]["CompanyCode"].ToString());
                                        string d_ = Convert.ToDateTime(job_.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2);

                                        string t1 = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-", EngCI) +
                                             Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.AddMonths(-1).ToString("yyyy", EngCI)),
                                             Convert.ToInt32(DateTime.Now.AddMonths(-1).ToString("MM", EngCI)))).ToString("0#");

                                        string t2 = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-", EngCI) + "16";

                                        mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
                                             t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t_adhoc");

                                        Serv.UpdateJobNext_date(job2.Rows[0]["id"].ToString(), Convert.ToDateTime(job2.Rows[0]["jobnext_datetime"]).AddMonths(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
                                    }


                                }

                            }
                            catch (Exception ex)
                            {
                                Serv.UpdateJobNext_date(job2.Rows[0]["id"].ToString(), Convert.ToDateTime(job2.Rows[0]["jobnext_datetime"]).AddMonths(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t_adhoc");
                            }
                        }


                    }
                }




            }

        }
    }
}