﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using iconsiam.Controllers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class syncsap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
                result x = new result();
                jobscheduleDLL Serv = new jobscheduleDLL();
                smart_collectionDLL Serv2 = new smart_collectionDLL();
                sentmail mail = new sentmail();
                LogService l = new LogService();

                string text = "";

                var indGroup = Serv.getSAPIndustryGroup();
                if (indGroup.Rows.Count != 0)
                {
                    string IndustryGroupScript = "";
                    for (int i = 0; i < indGroup.Rows.Count; i++)
                    {
                        var existingIndus = Serv.getTblIndustryGroup(indGroup.Rows[i]["IndustryGroupCode"].ToString());
                        if (existingIndus.Rows.Count != 0)
                        {
                            IndustryGroupScript = IndustryGroupScript + UpdateTblIndustryGroup(indGroup.Rows[i]["IndustryGroupCode"].ToString(),
                                indGroup.Rows[i]["IndustryGroupNameEN"].ToString(), indGroup.Rows[i]["IndustryGroupNameTH"].ToString(),
                                indGroup.Rows[i]["IsActive"].ToString(), "1");
                        }
                        else
                        {
                            IndustryGroupScript = IndustryGroupScript + InsertTblIndustryGroup(indGroup.Rows[i]["IndustryGroupCode"].ToString(),
                              indGroup.Rows[i]["IndustryGroupNameEN"].ToString(), indGroup.Rows[i]["IndustryGroupNameTH"].ToString(),
                              indGroup.Rows[i]["IsActive"].ToString(), "1");
                        }

                    }

                    Serv.Execute_query_insert(IndustryGroupScript);
                }


                var companyList = Serv.getCompany();
                if (companyList.Rows.Count != 0)
                {
                    for (int jj = 0; jj < companyList.Rows.Count; jj++)
                    {
                        var job = Serv.getTime("syncsap", companyList.Rows[jj]["CompanyCode"].ToString());
                        if (job.Rows.Count != 0)
                        {
                            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "_Start_Job", "syncsap_adhoc");

                            try
                            {
                                string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
                                string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

                                int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

                                if (result > 0)
                                {

                                    string com = "";
                                    var com_code = Serv2.GetCompanyCode();
                                    if (com_code.Rows.Count != 0)
                                    {
                                        for (int i = 0; i < com_code.Rows.Count; i++)
                                        {
                                            com = com + "'" + com_code.Rows[i]["CompanyCode"].ToString() + "'" + ",";


                                            var extCompany = Serv2.GetCompanyDetailByCompanyCode(com_code.Rows[i]["CompanyCode"].ToString());
                                            if (extCompany.Rows.Count == 0)
                                            {
                                                Serv2.InsertCompanyDetail(com_code.Rows[i]["CompanyCode"].ToString(), "", "", "1", "y",
                                                    com_code.Rows[i]["CompanyNameTH"].ToString(), com_code.Rows[i]["CompanyNameEN"].ToString());
                                            }
                                        }
                                        com = com.Substring(0, com.Length - 1);
                                    }


                                    var SAP_contract = Serv2.getSAPContract(com);

                                    string script_update = "";
                                    string script_update_account = "";
                                    string script_create = "";

                                    try
                                    {


                                        if (SAP_contract.Rows.Count != 0)
                                        {
                                            for (int i = 0; i < SAP_contract.Rows.Count; i++)
                                            {


                                                var smart_contract = Serv2.getSmart_Contract(SAP_contract.Rows[i]["ContractNumber"].ToString());
                                                if (smart_contract.Rows.Count != 0)
                                                {
                                                    text = "Update Step Contract Number " + SAP_contract.Rows[i]["ContractNumber"].ToString();

                                                    string room_number = "";
                                                    string RentalObjectCode = "";
                                                    decimal sqm = 0;
                                                    var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
                                                    if (room.Rows.Count != 0)
                                                    {
                                                        for (int j = 0; j < room.Rows.Count; j++)
                                                        {
                                                            room_number = room_number + "" + room.Rows[j]["RentalObjectDescription"].ToString() + ",";
                                                            sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

                                                            if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
                                                            {
                                                                if (RentalObjectCode == "")
                                                                {
                                                                    RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
                                                                }
                                                            }
                                                        }
                                                        room_number = room_number.Substring(0, room_number.Length - 1);
                                                    }

                                                    if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
                                                    {
                                                        script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
                                                            SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
                                                            SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
                                                            Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                            Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                            SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
                                                            SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
                                                            SAP_contract.Rows[i]["UpdateDate"].ToString(),
                                                            SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
                                                            SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
                                                            RentalObjectCode);


                                                        script_update_account = Update_script_Account(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"),
                                                            Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).AddDays(60).ToString("yyyy-MM-dd HH:mm:ss", EngCI));

                                                    }
                                                    else
                                                    {
                                                        script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
                                                               SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
                                                               SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
                                                               Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                               "",
                                                               SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
                                                               SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
                                                               SAP_contract.Rows[i]["UpdateDate"].ToString(),
                                                               SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
                                                               SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
                                                               RentalObjectCode);

                                                    }

                                                    Serv2.Execute_query_update(script_update);

                                                    if (script_update_account != "")
                                                    {
                                                        Serv2.Execute_query_update(script_update_account);
                                                    }


                                                }
                                                else
                                                {
                                                    text = "Insert Step Contract Number " + SAP_contract.Rows[i]["ContractNumber"].ToString();

                                                    string room_number = "";
                                                    string RentalObjectCode = "";
                                                    string FloorDescription = "";
                                                    decimal sqm = 0;
                                                    var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
                                                    if (room.Rows.Count != 0)
                                                    {

                                                        FloorDescription = room.Rows[0]["FloorDescription"].ToString();

                                                        for (int j = 0; j < room.Rows.Count; j++)
                                                        {

                                                            room_number += room.Rows[j]["RentalObjectDescription"].ToString() + ",";
                                                            sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

                                                            if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
                                                            {
                                                                if (RentalObjectCode == "")
                                                                {
                                                                    RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
                                                                }
                                                            }
                                                        }

                                                        room_number = room_number.Substring(0, room_number.Length - 1);

                                                    }

                                                    if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
                                                    {
                                                        script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
                                                                  SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
                                                                  Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                                  Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                                  SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
                                                                  SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
                                                                   Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                                   SAP_contract.Rows[i]["UpdateDate"].ToString(),
                                                                  SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
                                                                  "", "", "", "", "", "", "", "", "", "", "", "",
                                                                  "", "", "", "",
                                                                  RentalObjectCode, FloorDescription);

                                                    }
                                                    else
                                                    {
                                                        script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
                                                                  SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
                                                                  Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                                  "",
                                                                  SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
                                                                  SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
                                                                  SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
                                                                   Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                                                   SAP_contract.Rows[i]["UpdateDate"].ToString(),
                                                                  SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
                                                                  SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
                                                                  "", "", "", "", "", "", "", "", "", "", "", "",
                                                                  "", "", "", "",
                                                                  RentalObjectCode, FloorDescription);

                                                    }


                                                    Serv2.Execute_query_insert(script_create);

                                                }
                                            }
                                        }


                                    }
                                    catch (Exception ex)
                                    {
                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "_Fail : " + text + " // " + ex.ToString(), "syncsap_adhoc");
                                    }

                                    /// Sent Email ///
                                    /// 
                                    mail.CallMai_new_contract(DateTime.Now.ToString("yyyy-MM-dd", EngCI), "icon");

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "_success", "syncsap_adhoc");

                                    Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));

                                }
                            }
                            catch (Exception ex)
                            {
                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "_Fail : " + text + " // " + ex.ToString(), "syncsap_adhoc");
                            }
                        }
                    }
                }



            }

        }

        ///============================================================================================================================///
        ///
        private static string Update_script(string ContractNumber, string CompanyCode, string ContractTypeCode, string ContractTypeDescription, string ContractStartDate, string ContractEndDate, string ShopName,
             string IndustryCode, string BusinessPartnerCode, string BusinessPartnerName, string ContractTerm, string SubTypeCode, string SubTypeName, string OwnCashier, string OwnCreditCard,
             string RelevantToSales, string UpdateDate, string IsActive, string SalesTypeCode, string SalesTypeName, string smart_room_no, string smart_sqm, string smart_object_rental_code)
        {
            var str = " update tblsmart_Contract " +
                        " set CompanyCode = '" + CompanyCode + "',  " +
                        " ContractTypeCode = '" + ContractTypeCode + "', " +
                        " ContractTypeDescription = '" + ContractTypeDescription + "', " +
                        " ContractStartDate = '" + ContractStartDate + "', " +
                        " ContractEndDate = '" + ContractEndDate + "', " +
                        " ShopName = '" + ShopName + "', " +
                        " IndustryCode = '" + IndustryCode + "', " +
                        " BusinessPartnerCode = '" + BusinessPartnerCode + "', " +
                        " BusinessPartnerName = '" + BusinessPartnerName + "', " +
                        " ContractTerm = '" + ContractTerm + "', " +
                        " SubTypeCode = '" + SubTypeCode + "', " +
                        " SubTypeName = '" + SubTypeName + "', " +
                        " OwnCashier = '" + OwnCashier + "', " +
                        " OwnCreditCard = '" + OwnCreditCard + "', " +
                        " RelevantToSales = '" + RelevantToSales + "', " +
                        " UpdateDate =  GETDATE(), " +
                        " IsActive = '" + IsActive + "', " +
                        " SalesTypeCode = '" + SalesTypeCode + "', " +
                        " SalesTypeName = '" + SalesTypeName + "', " +

                        " smart_room_no = '" + smart_room_no + "', " +
                        " smart_sqm = '" + smart_sqm + "', " +
                        " smart_update_date = GETDATE(), " +
                        " smart_object_rental_code = '" + smart_object_rental_code + "' " +

                        " where ContractNumber = '" + ContractNumber + "' ;  ";




            return str;
        }


        ///
        private static string Update_script_Account(string ContractNumber, string Expiry_date)
        {
            var str = " update tblcontract_account  set Expiry_date = '" + Expiry_date + "' where ContractNumber = '" + ContractNumber + "' and flag_active = 'y' ;  ";




            return str;
        }

        private static string Insert_script(string ContractNumber, string CompanyCode, string ContractTypeCode, string ContractTypeDescription, string ContractStartDate, string ContractEndDate,
            string ShopName, string IndustryCode, string BusinessPartnerCode, string BusinessPartnerName, string ContractTerm, string SubTypeCode, string SubTypeName, string OwnCashier, string OwnCreditCard,
            string RelevantToSales, string CreatedDate, string UpdateDate, string IsActive, string SalesTypeCode, string SalesTypeName, string smart_room_no, string smart_sqm, string smart_update_date,
            string smart_update_user, string smart_isShop_group, string smart_shop_group_id, string smart_shop_open, string smart_shop_close, string smart_group_location, string smart_contract_type,
            string smart_Category_leasing, string smart_Industry_group, string smart_collection_flag_status, string smart_icon_staff_id, string smart_record_person_type,
            string smart_record_type, string smart_record_daily_type, string smart_record_daily_time, string smart_record_weekly_date, string smart_record_weekly_time,
            string smart_record_monthly_month, string smart_record_monthly_time, string smart_contract_status, string smart_record_keyin_type, string Contract_old_number, string smart_object_rental_code, string smart_floor)
        {
            var str = "Insert into tblsmart_Contract(ContractNumber,CompanyCode,ContractTypeCode,ContractTypeDescription,ContractStartDate,ContractEndDate,ShopName,IndustryCode, " +
                        " BusinessPartnerCode,BusinessPartnerName,ContractTerm,SubTypeCode,SubTypeName,OwnCashier,OwnCreditCard,RelevantToSales, " +
                        " CreatedDate,IsActive,SalesTypeCode,SalesTypeName,smart_room_no,smart_sqm,smart_create_date,smart_update_user, " +
                        " smart_isShop_group,smart_shop_group_id,smart_group_location,smart_contract_type, " +
                        " smart_Category_leasing,smart_Industry_group,smart_collection_flag_status,smart_icon_staff_id,smart_record_person_type, " +
                        " smart_record_type,smart_record_daily_type,smart_record_daily_time,smart_record_weekly_date,smart_record_weekly_time, " +
                        " smart_record_monthly_month,smart_record_monthly_time,smart_contract_status,smart_record_keyin_type,Contract_old_number,smart_object_rental_code,smart_floor) " +

                        " Values('" + ContractNumber + "','" + CompanyCode + "','" + ContractTypeCode + "','" + ContractTypeDescription + "','" + ContractStartDate + "','" + ContractEndDate + "','" + ShopName + "','" + IndustryCode + "', " +
                        " '" + BusinessPartnerCode + "','" + BusinessPartnerName + "','" + ContractTerm + "','" + SubTypeCode + "','" + SubTypeName + "','" + OwnCashier + "','" + OwnCreditCard + "','" + RelevantToSales + "', " +
                        "'" + CreatedDate + "','" + IsActive + "','" + SalesTypeCode + "','" + SalesTypeName + "','" + smart_room_no + "','" + smart_sqm + "',GETDATE(),'" + smart_update_user + "', " +
                        " '" + smart_isShop_group + "','" + smart_shop_group_id + "','" + smart_group_location + "','" + smart_contract_type + "', " +
                        " '" + smart_Category_leasing + "','" + smart_Industry_group + "','" + smart_collection_flag_status + "','" + smart_icon_staff_id + "','" + smart_record_person_type + "', " +
                        " '" + smart_record_type + "','" + smart_record_daily_type + "','" + smart_record_daily_time + "','" + smart_record_weekly_date + "','" + smart_record_weekly_time + "', " +
                        " '" + smart_record_monthly_month + "','" + smart_record_monthly_time + "','" + smart_contract_status + "','" + smart_record_keyin_type + "','" + Contract_old_number + "','" + smart_object_rental_code + "','" + smart_floor + "') ;  ";




            return str;
        }


        private static string InsertTblIndustryGroup(string IndustryGroupCode, string IndustryGroupNameEN, string IndustryGroupNameTH, string IsActive, string create_id)
        {
            var str = " Insert into TblIndustryGroup(IndustryGroupCode,IndustryGroupNameEN,IndustryGroupNameTH,CreatedDate, " +
                " UpdateDate,IsActive,create_id,update_id) " +
                " values('" + IndustryGroupCode + "','" + IndustryGroupNameEN + "','" + IndustryGroupNameTH + "', " +
                " getdate(),getdate(), '" + IsActive + "','" + create_id + "', '" + create_id + "') ; ";

            return str;
        }

        private static string UpdateTblIndustryGroup(string IndustryGroupCode, string IndustryGroupNameEN, string IndustryGroupNameTH, string IsActive, string update_id)
        {
            var str = " Update TblIndustryGroup set IndustryGroupCode = '" + IndustryGroupCode + "', " +
                "IndustryGroupNameEN = '" + IndustryGroupNameEN + "',IndustryGroupNameTH = '" + IndustryGroupNameTH + "', " +
                " UpdateDate = GETDATE(),IsActive = '" + IsActive + "', update_id = '" + update_id + "' where IndustryGroupCode = '" + IndustryGroupCode + "' ; ";

            return str;
        }











    }
}