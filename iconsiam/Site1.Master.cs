﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected string MyTheme { get; set; } //btn
        protected string NavbarColor { get; set; } //txtbtn
        protected string NavbarColor2 { get; set; } //txthover
        protected string BgColor { get; set; } //gb bar
        protected string BgColor2 { get; set; } //gb bar
        protected string BgColor3 { get; set; } //gb bar
        protected string BgColor4 { get; set; } //gb bar

        protected string BgColor5 { get; set; } //gb bar
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    lbusername.Text = HttpContext.Current.Session["s_username"].ToString();
                    lbusernam2.Text = HttpContext.Current.Session["s_username"].ToString();
                    lbemail.Text = "";
                    Image1.Visible = false;
                    Image2.Visible = false;

                    if (HttpContext.Current.Session["code_theme"] != null)
                    {
                        this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                    }
                    else
                    {
                        this.MyTheme = "#00263d";
                    }

                    if (HttpContext.Current.Session["code_Navbar"] != null)
                    {
                        this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                    }
                    else
                    {
                        this.NavbarColor = "#fff";
                    }

                    if (HttpContext.Current.Session["code_Navbar2"] != null)
                    {
                        this.NavbarColor2 = HttpContext.Current.Session["code_Navbar2"].ToString();
                    }
                    else
                    {
                        this.NavbarColor2 = "#fff";
                    }

                    if (HttpContext.Current.Session["code_Bg"] != null)
                    {
                        this.BgColor = HttpContext.Current.Session["code_Bg"].ToString();
                    }
                    else
                    {
                        this.BgColor = "##a6a29e";
                    }

                    if (HttpContext.Current.Session["code_Bg2"] != null)
                    {
                        this.BgColor2 = HttpContext.Current.Session["code_Bg2"].ToString();
                    }
                    else
                    {
                        this.BgColor2 = "##cab6a3";
                    }

                    if (HttpContext.Current.Session["code_Bg3"] != null)
                    {
                        this.BgColor3 = HttpContext.Current.Session["code_Bg3"].ToString();
                    }
                    else
                    {
                        this.BgColor3 = "#5f4a79";
                    }

                    if (HttpContext.Current.Session["code_Bg4"] != null)
                    {
                        this.BgColor4 = HttpContext.Current.Session["code_Bg4"].ToString();
                    }
                    else
                    {
                        this.BgColor4 = "#7b5f9c";
                    }

                    if (HttpContext.Current.Session["code_Bg5"] != null)
                    {
                        this.BgColor5 = HttpContext.Current.Session["code_Bg5"].ToString();
                    }
                    else
                    {
                        this.BgColor5 = "#9775c0";
                    }

                    if (HttpContext.Current.Session["PicGroupCompany"] != null)
                    {
                        this.imgCompany.ImageUrl = HttpContext.Current.Session["PicGroupCompany"].ToString();
                    }
                    else
                    {
                        this.imgCompany.ImageUrl = "img/siampi.png";
                    }
                }
            }
            else
            {
                if (HttpContext.Current.Session["code_theme"] != null)
                {
                    this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                }
                else
                {
                    this.MyTheme = "#00263d";
                }

                if (HttpContext.Current.Session["code_Navbar"] != null)
                {
                    this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                }
                else
                {
                    this.NavbarColor = "#fff";
                }

                if (HttpContext.Current.Session["code_Navbar2"] != null)
                {
                    this.NavbarColor2 = HttpContext.Current.Session["code_Navbar2"].ToString();
                }
                else
                {
                    this.NavbarColor2 = "#fff";
                }

                if (HttpContext.Current.Session["code_Bg"] != null)
                {
                    this.BgColor = HttpContext.Current.Session["code_Bg"].ToString();
                }
                else
                {
                    this.BgColor = "##a6a29e";
                }

                if (HttpContext.Current.Session["code_Bg2"] != null)
                {
                    this.BgColor2 = HttpContext.Current.Session["code_Bg2"].ToString();
                }
                else
                {
                    this.BgColor2 = "##cab6a3";
                }

                if (HttpContext.Current.Session["code_Bg3"] != null)
                {
                    this.BgColor3 = HttpContext.Current.Session["code_Bg3"].ToString();
                }
                else
                {
                    this.BgColor3 = "#5f4a79";
                }

                if (HttpContext.Current.Session["code_Bg4"] != null)
                {
                    this.BgColor4 = HttpContext.Current.Session["code_Bg4"].ToString();
                }
                else
                {
                    this.BgColor4 = "#7b5f9c";
                }

                if (HttpContext.Current.Session["code_Bg5"] != null)
                {
                    this.BgColor5 = HttpContext.Current.Session["code_Bg5"].ToString();
                }
                else
                {
                    this.BgColor5 = "#9775c0";
                }


                if (HttpContext.Current.Session["PicGroupCompany"] != null)
                {
                    this.imgCompany.ImageUrl = HttpContext.Current.Session["PicGroupCompany"].ToString();
                }
                else
                {
                    this.imgCompany.ImageUrl = "img/siampi.png";
                }
            }

            if (HttpContext.Current.Session["s_userid"] == null)
            {
                Response.Redirect("~/login.aspx");
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            Response.Redirect("~/login.aspx");
        }

    }
}