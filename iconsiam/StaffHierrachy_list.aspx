﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="StaffHierrachy_list.aspx.cs" Inherits="iconsiam.StaffHierrachy_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Staff Hierarchy
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlteam" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtname" runat="server" placeholder="Officer Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtmanager" runat="server" placeholder="Manager Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn" />
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" class="btn ThemeBtn"  />
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click"  class="btn ThemeBtn"  />

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="team" HeaderText="Team" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />                            
                            <asp:BoundField DataField="vp_name" HeaderText="VP Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="manager_name" HeaderText="Manager Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="officer_name" HeaderText="Officer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:BoundField DataField="update_date" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />


                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:Button ID="btndel" runat="server" Text="ลบ" OnClick="btndel_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>



</asp:Content>
