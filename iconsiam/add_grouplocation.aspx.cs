﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class add_grouplocation : System.Web.UI.Page
    {
        groupLocationDLL Serv = new groupLocationDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtGroupLocationEN.Focus();
                    txtGroupLocationEN.Attributes.Add("onkeypress", "return next_tools(event,'" + txtGroupLocationTH.ClientID + "')");
                    txtGroupLocationTH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                    bind_default();
                }
            }
        }
        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtGroupLocationEN.Text != "" && txtGroupLocationTH.Text != "")
            {
                var ex1 = Serv.getGrouplocationByGrouplocationNameEN(txtGroupLocationEN.Text);
                var ex2 = Serv.getGrouplocationByGrouplocationNameTH(txtGroupLocationTH.Text);
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(TH) ซ้ำ");
                    return;
                }
                else
                {
                    Serv.Inserttblgrouplocation(txtGroupLocationTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtGroupLocationEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue,
                 HttpContext.Current.Session["s_userid"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='grouplocation.aspx';", true);
                }
             
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/grouplocation.aspx");
        }


    }
}