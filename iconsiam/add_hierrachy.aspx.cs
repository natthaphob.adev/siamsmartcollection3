﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class add_hierrachy : System.Web.UI.Page
    {
        add_hierrachyDLL Serv = new add_hierrachyDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                }
            }
        }
        protected void bind_default()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            ddlteam.Items.Insert(0, new ListItem("AR", "ar"));
            ddlteam.Items.Insert(1, new ListItem("Sales", "ae"));

            var vp = Serv.GetUserByTeam_Role(ddlteam.SelectedValue, "vp", comcode_);
            if (vp.Rows.Count != 0)
            {
                ddlvp.DataSource = vp;
                ddlvp.DataTextField = "name";
                ddlvp.DataValueField = "userid";
                ddlvp.DataBind();

            }
            else
            {
                ddlvp.Items.Clear();
                ddlvp.DataSource = null;
                ddlvp.DataBind();
            }
            ddlvp.Items.Insert(0, new ListItem("Select VP", ""));

            var manager = Serv.GetUserByTeam_Role(ddlteam.SelectedValue, "manager", comcode_);
            if (manager.Rows.Count != 0)
            {
                ddlmanager.DataSource = manager;
                ddlmanager.DataTextField = "name";
                ddlmanager.DataValueField = "userid";
                ddlmanager.DataBind();
            }
            else
            {
                ddlmanager.Items.Clear();
                ddlmanager.DataSource = null;
                ddlmanager.DataBind();
            }
            ddlmanager.Items.Insert(0, new ListItem("Select Manager", ""));

            var officer = Serv.GetOfficer(ddlteam.SelectedValue, comcode_);
            if (officer.Rows.Count != 0)
            {
                ddlofficer.DataSource = officer;
                ddlofficer.DataTextField = "name";
                ddlofficer.DataValueField = "userid";
                ddlofficer.DataBind();

            }
            else
            {
                ddlofficer.Items.Clear();
                ddlofficer.DataSource = null;
                ddlofficer.DataBind();
            }
            ddlofficer.Items.Insert(0, new ListItem("Select Officer", ""));


        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (ddlvp.SelectedValue == "")
            {
                POPUPMSG("Please Select VP");
                return;
            }
            if (ddlmanager.SelectedValue == "")
            {
                POPUPMSG("Please Select Manager");
                return;
            }
            if (ddlofficer.SelectedValue == "")
            {
                POPUPMSG("Please Select Officer");
                return;
            }

            Serv.InsertHierrachy(ddlteam.SelectedValue, ddlvp.SelectedValue, ddlmanager.SelectedValue, ddlofficer.SelectedValue);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='StaffHierrachy_list.aspx';", true);

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StaffHierrachy_list.aspx");
        }

        protected void ddlteam_SelectedIndexChanged(object sender, EventArgs e)
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var vp = Serv.GetUserByTeam_Role(ddlteam.SelectedValue, "vp", comcode_);
            if (vp.Rows.Count != 0)
            {
                ddlvp.DataSource = vp;
                ddlvp.DataTextField = "name";
                ddlvp.DataValueField = "userid";
                ddlvp.DataBind();

            }
            else
            {
                ddlvp.Items.Clear();
                ddlvp.DataSource = null;
                ddlvp.DataBind();
            }
            ddlvp.Items.Insert(0, new ListItem("Select VP", ""));

            var manager = Serv.GetUserByTeam_Role(ddlteam.SelectedValue, "manager", comcode_);
            if (manager.Rows.Count != 0)
            {
                ddlmanager.DataSource = manager;
                ddlmanager.DataTextField = "name";
                ddlmanager.DataValueField = "userid";
                ddlmanager.DataBind();
            }
            else
            {
                ddlmanager.Items.Clear();
                ddlmanager.DataSource = null;
                ddlmanager.DataBind();
            }
            ddlmanager.Items.Insert(0, new ListItem("Select Manager", ""));

            var officer = Serv.GetOfficer(ddlteam.SelectedValue, comcode_);
            if (officer.Rows.Count != 0)
            {
                ddlofficer.DataSource = officer;
                ddlofficer.DataTextField = "name";
                ddlofficer.DataValueField = "userid";
                ddlofficer.DataBind();

            }
            else
            {
                ddlofficer.Items.Clear();
                ddlofficer.DataSource = null;
                ddlofficer.DataBind();
            }
            ddlofficer.Items.Insert(0, new ListItem("Select Officer", ""));


        }



    }
}