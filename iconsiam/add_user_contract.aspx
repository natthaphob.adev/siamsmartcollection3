﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="add_user_contract.aspx.cs" Inherits="iconsiam.add_user_contract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Member(s)
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txtname" runat="server" placeholder="ชื่อ-นามสกุล" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txtemail" runat="server" placeholder="Email" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txttel1" runat="server" placeholder="เบอร์โทร" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txttel2" runat="server" placeholder="เบอร์มือถือ" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-check">
                        <label>ผู้ดูแลหลัก</label><br />
                        <asp:DropDownList ID="ddlmain_contact" runat="server" class="form-control"></asp:DropDownList>

                    </div>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>



</asp:Content>
