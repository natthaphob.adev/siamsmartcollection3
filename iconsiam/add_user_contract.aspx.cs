﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class add_user_contract : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        shop_sap_masterDLL Serv = new shop_sap_masterDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    bind_default();
                }
            }
        }

        protected void bind_default()
        {
            ddlmain_contact.Items.Insert(0, new ListItem("ไม่ใช่", "n"));
            ddlmain_contact.Items.Insert(1, new ListItem("ใช่", "y"));

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["con_code"] == null)
            {
                Response.Redirect("~/conteactList_SAP.aspx");
            }
            else
            {
                if (txtname.Text != "" && txtemail.Text != "" && txttel1.Text != "")
                {
                    //Serv.Insert_contact_point(HttpContext.Current.Session["con_code"].ToString(), txtname.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txttel1.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txttel2.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlmain_contact.SelectedValue);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='shop_sap_master.aspx?id=" + HttpContext.Current.Session["con_code"].ToString() + "';", true);
                }
                else
                {
                    POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                }
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/shop_sap_master.aspx?id=" + HttpContext.Current.Session["con_code"].ToString());
        }


    }
}