﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class company_mapping : System.Web.UI.Page
    {
        company_mappingDLL Serv = new company_mappingDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {

            //ddlrole.Items.Clear();
            ////ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
            ////ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
            ////ddlrole.Items.Insert(2, new ListItem("Datacenter", "datacenter"));
            ////ddlrole.Items.Insert(0, new ListItem("Admin", "admin"));
            //ddlrole.Items.Insert(0, new ListItem("Officer", "officer"));
            //ddlrole.Items.Insert(1, new ListItem("Manager", "manager"));
            //ddlrole.Items.Insert(2, new ListItem("VP", "vp"));
            //ddlrole.Items.Insert(3, new ListItem("Management", "management"));



            var comp = Serv.getCompany(HttpContext.Current.Session["s_com_code"].ToString());
            if (comp.Rows.Count != 0)
            {
                ddlcompany.DataTextField = "CompanyNameTh";
                ddlcompany.DataValueField = "Companycode";
                ddlcompany.DataSource = comp;
                ddlcompany.DataBind();
            }
            else
            {
                ddlcompany.DataSource = null;
                ddlcompany.DataBind();

            }
        }

        protected void bind_data()
        {
            var data = Serv.GetMapping(txtusername.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlcompany.SelectedValue, "ar");
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_mapping.aspx");
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            Serv.Delete_Mapping(hdd_id.Value);
            bind_data();

            var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            if (comcode.Rows.Count != 0)
            {
                string com_code = "";
                for (int i = 0; i < comcode.Rows.Count; i++)
                {
                    com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
                }

                com_code = com_code.Substring(0, com_code.Length - 1);
                HttpContext.Current.Session["s_com_code"] = com_code;

            }


        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/company_mapping.aspx");

        }
    }
}