﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class contractList_SAP : System.Web.UI.Page
    {
        conteactList_SAPDLL Serv = new conteactList_SAPDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_rep();

                }
            }
        }

        protected void bind_default()
        {
            #region
            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //if (HttpContext.Current.Session["role"].ToString() != "super_admin" && HttpContext.Current.Session["role"].ToString() != "datacenter")
            //{
            //    var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //    if (comcode.Rows.Count != 0)
            //    {
            //        string com_code = "";
            //        for (int i = 0; i < comcode.Rows.Count; i++)
            //        {
            //            com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //        }

            //        com_code = com_code.Substring(0, com_code.Length - 1);
            //        HttpContext.Current.Session["s_com_code"] = com_code;

            //    }
            //}
            //else
            //{
            //    var comcode = Serv.GetMapCompany("");
            //    if (comcode.Rows.Count != 0)
            //    {
            //        string com_code = "";
            //        for (int i = 0; i < comcode.Rows.Count; i++)
            //        {
            //            com_code += "'" + comcode.Rows[i]["companycode"].ToString() + "',";
            //        }

            //        com_code = com_code.Substring(0, com_code.Length - 1);
            //        HttpContext.Current.Session["s_com_code"] = com_code;

            //    }
            //}

            #endregion


            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            //ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            //ddlstatus.Items.Insert(1, new ListItem("InActive", "0"));

            //ddlhavename.Items.Insert(0, new ListItem("ยังไม่มีผู้ดูแล", "n"));
            //ddlhavename.Items.Insert(1, new ListItem("มีผู้ดูแลแล้ว", "y"));
        }
        protected void bind_rep()
        {
            string[] shopname = txtshopname.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');

            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var indust = Serv.GetSmart_Contract(ddlcompany.SelectedValue, shopname, shopgroup, HttpContext.Current.Session["s_com_code"].ToString());
                if (indust.Rows.Count != 0)
                {
                    GridView_List.DataSource = indust;
                    GridView_List.DataBind();
                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();
                }
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            HttpContext.Current.Session["lastUri"] = "contractList_SAP.aspx";
            HttpContext.Current.Session["oldContract"] = "";

            Response.Redirect("~/shop_sap_master.aspx?id=" + hdd_id.Value);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contractList_SAP.aspx");

        }
    }
}