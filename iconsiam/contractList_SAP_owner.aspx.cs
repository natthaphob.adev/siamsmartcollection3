﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class contractList_SAP_owner : System.Web.UI.Page
    {
        conteactList_SAPDLL Serv = new conteactList_SAPDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_rep();                  
                }
            }
        }

        protected void bind_default()
        {
            //if (HttpContext.Current.Session["s_user_for_shop"] != null)
            //{
            //    var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //    //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //    if (comcode.Rows.Count != 0)
            //    {
            //        string com_code = "";
            //        for (int i = 0; i < comcode.Rows.Count; i++)
            //        {
            //            com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //        }

            //        com_code = com_code.Substring(0, com_code.Length - 1);
            //        HttpContext.Current.Session["s_com_code"] = com_code;

            //    }
            //}





            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            if (HttpContext.Current.Session["role"].ToString() == "officer")
            {
                ddlar.Visible = false;
            }
            else
            {
                ddlar.Visible = true;

                if (HttpContext.Current.Session["s_user_for_shop"] != null && HttpContext.Current.Session["s_user_for_shop"].ToString() != "")
                {
                    var u = Serv.GetUserByUserid(HttpContext.Current.Session["s_user_for_shop"].ToString());
                    if (u.Rows.Count != 0)
                    {
                        ddlar.DataTextField = "name";
                        ddlar.DataValueField = "userid";
                        ddlar.DataSource = u;
                        ddlar.DataBind();
                    }
                    else
                    {
                        ddlar.DataSource = null;
                        ddlar.DataBind();
                    }
                }
                ddlar.Items.Insert(0, new ListItem("User", ""));
            }

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

        }
        protected void bind_rep()
        {
            string[] shopname = txtshopname.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');


            if (HttpContext.Current.Session["s_user_for_shop"] != null && HttpContext.Current.Session["s_com_code"] != null)
            {
                var indust = Serv.GetSmart_Contract_owner(HttpContext.Current.Session["s_user_for_shop"].ToString(),
                ddlcompany.SelectedValue, shopname, shopgroup, ddlar.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString(), ddlstatus.SelectedValue);
                if (indust.Rows.Count != 0)
                {
                    if (HttpContext.Current.Session["role"].ToString() == "officer")
                    {
                        if (ddlstatus.SelectedValue == "0")
                        {
                            GridView_List1.DataSource = indust;
                            GridView_List1.DataBind();

                            GridView_List.DataSource = null;
                            GridView_List.DataBind();
                        }
                        else
                        {
                            GridView_List.DataSource = indust;
                            GridView_List.DataBind();

                            GridView_List1.DataSource = null;
                            GridView_List1.DataBind();
                        }                      

                    }
                    else
                    {
                        if (ddlstatus.SelectedValue == "0")
                        {
                            GridView_List3.DataSource = indust;
                            GridView_List3.DataBind();

                            GridView_List2.DataSource = null;
                            GridView_List2.DataBind();
                        }
                        else
                        {
                            GridView_List2.DataSource = indust;
                            GridView_List2.DataBind();

                            GridView_List3.DataSource = null;
                            GridView_List3.DataBind();
                        }                     
                    }

                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView_List1.DataSource = null;
                    GridView_List1.DataBind();

                    GridView_List2.DataSource = null;
                    GridView_List2.DataBind();

                    GridView_List3.DataSource = null;
                    GridView_List3.DataBind();
                }
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }




        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HttpContext.Current.Session["oldContract"] = "";

            HttpContext.Current.Session["lastUri"] = "contractList_SAP_owner.aspx";
            Response.Redirect("~/shop_sap_master.aspx?id=" + hdd_id.Value);
        }

        protected void btnedit_old_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HttpContext.Current.Session["oldContract"] = "";

            HttpContext.Current.Session["lastUri"] = "contractList_SAP_owner.aspx";
            Response.Redirect("~/shop_sap_master_old.aspx?id=" + hdd_id.Value);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contractList_SAP_owner.aspx");

        }

        protected void GridView_List2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List2.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void GridView_List1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List1.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void GridView_List3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List3.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }
    }
}