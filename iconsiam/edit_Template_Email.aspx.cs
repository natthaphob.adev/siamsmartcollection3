﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class edit_Template_Email : System.Web.UI.Page
    {
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }

        EmailTemplateDLL Serv = new EmailTemplateDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();
                }
            }
        }
        protected void bind_data()
        {

            if (Request.QueryString["Companygroup"].ToString() != null && Request.QueryString["Companygroup"].ToString() != "")
            {
                HttpContext.Current.Session["Companygroup_theme"] = Request.QueryString["Companygroup"].ToString();
            }
            var comp = Serv.getComName(txtcompany.Text, HttpContext.Current.Session["Companygroup_theme"].ToString());
            if (comp.Rows.Count != 0)
            {
                GridView_List.DataSource = comp;
                GridView_List.DataBind();

            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddCompanycode = (HiddenField)row.FindControl("hddCompanycode");
            Response.Redirect("~/EmailTemplate_edit.aspx?Companycode=" + hddCompanycode.Value);
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/edit_Template_Email.aspx?Companygroup=" + HttpContext.Current.Session["Companygroup_theme"].ToString());
        }
        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmailTemplate_CompanyGroup.aspx");
        }
    }
}