﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class forget_password : System.Web.UI.Page
    {
        loginDLL Serv = new loginDLL();
        sentmail mail = new sentmail();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        protected string CountPic { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtemail.Focus();
                txtemail.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsubmit.ClientID + "')");              
            }
            var logo = Serv.Get_Pic_Logo_login();
            CountPic = Convert.ToString(logo.Rows.Count.ToString());
            if (logo.Rows.Count != 0)
            {
                if (CountPic == "1")
                {
                    this.imgCompany1_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "2")
                {
                    this.imgCompany2_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany2_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "3")
                {
                    this.imgCompany3_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany3_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany3_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "4")
                {
                    this.imgCompany4_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "5")
                {
                    this.imgCompany5_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_5.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "6")
                {
                    this.imgCompany6_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_5.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_6.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }


            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtemail.Text != "")
            {
                var te = Serv.GetUserByEmail(txtemail.Text);
                if (te.Rows.Count != 0)
                {
                    var username = Serv.GetUserByContractnumber(te.Rows[0]["contractnumber"].ToString());
                    if (username.Rows.Count != 0)
                    {
                        HttpContext.Current.Session["s_userid"] = username.Rows[0]["id"].ToString();
                        HttpContext.Current.Session["s_username"] = username.Rows[0]["username"].ToString();
                        HttpContext.Current.Session["s_user_contractNumber"] = username.Rows[0]["contractnumber"].ToString();
                        HttpContext.Current.Session["s_user_type"] = "tenant";

                        //HttpContext.Current.Session["s_user_img"] = "assets/img/profile.jpg";

                        string email_ar = "";
                        string ar_name = "";
                        string shop_name = "";
                        string room_number = "";
                        string business_name = "";
                        string co_name = "";
                        string floor = "";

                        var conDetail = Serv.GetContractDetail(te.Rows[0]["contractnumber"].ToString());
                        if (conDetail.Rows.Count != 0)
                        {
                            var com = Serv.GetCompany(conDetail.Rows[0]["CompanyCode"].ToString());
                            if (com.Rows.Count != 0)
                            {
                                co_name = com.Rows[0]["CompanyNameEn"].ToString();
                            }


                            shop_name = conDetail.Rows[0]["ShopName"].ToString();
                            room_number = conDetail.Rows[0]["smart_room_no"].ToString();//
                            business_name = conDetail.Rows[0]["BUsinessPartnerName"].ToString();//
                            floor = conDetail.Rows[0]["smart_floor"].ToString();//

                            var u = Serv.GetUserByUserid(conDetail.Rows[0]["smart_icon_staff_id"].ToString());
                            if (u.Rows.Count != 0)
                            {
                                email_ar = u.Rows[0]["email"].ToString();
                                ar_name = u.Rows[0]["Fname"].ToString() + " " + u.Rows[0]["Lname"].ToString();
                            }
                        }

                        mail.CallMail_forget_password(ar_name, co_name, shop_name, business_name, floor, room_number, DateTime.Now.ToString("dd/MM/yyyy", EngCI), email_ar , conDetail.Rows[0]["CompanyCode"].ToString(),"user");


                        Response.Redirect("~/transaction_list_tenant.aspx");

                    }
                    else
                    {
                        POPUPMSG("ไม่พบ email ของท่านในระบบ");
                        return;
                    }


                }
                else
                {
                    var u = Serv.GetSmartUser_ByEmail(txtemail.Text);
                    if (u.Rows.Count != 0)
                    {
                        div_email.Visible = false;
                        divReset.Visible = true;

                        HttpContext.Current.Session["s_userid"] = u.Rows[0]["userid"].ToString();
                    }
                    else
                    {
                        POPUPMSG("Email ของท่านไม่ถูกต้อง");
                        return;
                    }
                }
            }
            else
            {
                POPUPMSG("กรุณากรอก Email ของท่าน");
                return;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (txtpassword.Text == "")
            {
                POPUPMSG("กรุณากรอก Password ใหม่ของท่าน");
                return;
            }
            if (txtconfirmpasssword.Text == "")
            {
                POPUPMSG("กรุณากรอกยืนยัน Password ของท่าน");
                return;
            }
            if (txtpassword.Text.Length < 6)
            {
                POPUPMSG("Password ต้องมีความยาวอย่างน้อย 6 ตัวอักษร");
                return;
            }

            if (txtpassword.Text == txtconfirmpasssword.Text)
            {
                Serv.UpdatePassword(txtpassword.Text, HttpContext.Current.Session["s_userid"].ToString());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='login.aspx';", true);
            }
            else
            {
                POPUPMSG("Password ใหม่ของท่านไม่ถูกต้อง");
                return;
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/login.aspx");

        }
    }
}