﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;
using System.Data;

namespace iconsiam
{
    public partial class jobList : System.Web.UI.Page
    {
        jobListDLL Serv = new jobListDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();
                    
                }
            }
        }

        protected void bind_data()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("companycode");
            dt.Columns.Add("companynameth");

            dt.Columns.Add("confirm_date1", typeof(DateTime));
            dt.Columns.Add("confirm_date2", typeof(DateTime));
            dt.Columns.Add("syncsap", typeof(DateTime));
            dt.Columns.Add("clear_copy_contract", typeof(DateTime));

            dt.Columns.Add("Update_date");

            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var com = Serv.getCompany(txtcompany.Text, comcode_);
            if (com.Rows.Count != 0)
            {
                for (int i = 0; i < com.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1["companycode"] = com.Rows[i]["Companycode"].ToString();
                    row1["companynameth"] = com.Rows[i]["CompanyNameTh"].ToString();

                    var syncsap = Serv.getJobByName("syncsap", com.Rows[i]["Companycode"].ToString());
                    if (syncsap.Rows.Count != 0)
                    {
                        row1["syncsap"] = syncsap.Rows[0]["jobnext_datetime"].ToString();
                        row1["Update_date"] = syncsap.Rows[0]["update_date"].ToString();
                    }

                    var jobconfirm = Serv.getJobByName("jobconfirm", com.Rows[i]["Companycode"].ToString());
                    if (jobconfirm.Rows.Count != 0)
                    {
                        row1["confirm_date1"] = jobconfirm.Rows[0]["jobnext_datetime"].ToString();
                        row1["Update_date"] = jobconfirm.Rows[0]["update_date"].ToString();
                    }

                    var jobconfirm2 = Serv.getJobByName("jobconfirm2", com.Rows[i]["Companycode"].ToString());
                    if (jobconfirm2.Rows.Count != 0)
                    {
                        row1["confirm_date2"] = jobconfirm2.Rows[0]["jobnext_datetime"].ToString();
                        row1["Update_date"] = jobconfirm2.Rows[0]["update_date"].ToString();
                    }

                    var jobcopy_contract = Serv.getJobByName("jobcopy_contract", com.Rows[i]["Companycode"].ToString());
                    if (jobcopy_contract.Rows.Count != 0)
                    {
                        row1["clear_copy_contract"] = jobcopy_contract.Rows[0]["jobnext_datetime"].ToString();
                        row1["Update_date"] = jobcopy_contract.Rows[0]["update_date"].ToString();
                    }


                    dt.Rows.Add(row1);

                }

                GridView_List.DataSource = dt;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }
        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddcompanycode = (HiddenField)row.FindControl("hddcompanycode");

            Response.Redirect("~/setting_schedule.aspx?id=" + hddcompanycode.Value);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/jobList.aspx");

        }


    }
}