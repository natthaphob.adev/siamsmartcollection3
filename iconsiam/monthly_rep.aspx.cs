﻿using ClosedXML.Excel;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class monthly_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        monthly_repDLL Serv = new monthly_repDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);
                    txtenddate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);

                    bind_default();
                    bind_rep();

                }
            }
        }

        protected void bind_default()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var building = Serv.GetBuilding(comcode_);
            if (building.Rows.Count != 0)
            {
                ddlbuilding.DataTextField = "BuildingNameEN";
                ddlbuilding.DataValueField = "BuildingCode";
                ddlbuilding.DataSource = building;
                ddlbuilding.DataBind();
            }
            else
            {
                ddlbuilding.DataSource = null;
                ddlbuilding.DataBind();

            }
            ddlbuilding.Items.Insert(0, new ListItem("Select Building", ""));

            var cl = Serv.GetCategory_leasing(comcode_);
            if (cl.Rows.Count != 0)
            {
                ddlcatagorLeasing.DataTextField = "CategoryleasingNameEN";
                ddlcatagorLeasing.DataValueField = "id";
                ddlcatagorLeasing.DataSource = cl;
                ddlcatagorLeasing.DataBind();
            }
            else
            {
                ddlcatagorLeasing.DataSource = null;
                ddlcatagorLeasing.DataBind();

            }
            ddlcatagorLeasing.Items.Insert(0, new ListItem("Select Category Leasing", ""));


            var ct = Serv.Getcontract_type(comcode_);
            if (ct.Rows.Count != 0)
            {
                ddlcontract_type.DataTextField = "ContractTypeNameEN";
                ddlcontract_type.DataValueField = "id";
                ddlcontract_type.DataSource = ct;
                ddlcontract_type.DataBind();
            }
            else
            {
                ddlcontract_type.DataSource = null;
                ddlcontract_type.DataBind();

            }
            ddlcontract_type.Items.Insert(0, new ListItem("Select Contract Type", ""));

            var g = Serv.Getgroup_location(comcode_);
            if (g.Rows.Count != 0)
            {
                ddlgroup_loca.DataTextField = "GrouplocationNameEN";
                ddlgroup_loca.DataValueField = "id";
                ddlgroup_loca.DataSource = g;
                ddlgroup_loca.DataBind();
            }
            else
            {
                ddlgroup_loca.DataSource = null;
                ddlgroup_loca.DataBind();

            }
            ddlgroup_loca.Items.Insert(0, new ListItem("Select Group Location", ""));

            var ig = Serv.GetIndustry_group(comcode_);
            if (ig.Rows.Count != 0)
            {
                ddlindustrygroup.DataTextField = "IndustryGroupNameEN";
                ddlindustrygroup.DataValueField = "id";
                ddlindustrygroup.DataSource = ig;
                ddlindustrygroup.DataBind();
            }
            else
            {
                ddlindustrygroup.DataSource = null;
                ddlindustrygroup.DataBind();

            }
            ddlindustrygroup.Items.Insert(0, new ListItem("Select Industry Group", ""));

            var r = Serv.GetRoomType();
            if (r.Rows.Count != 0)
            {
                ddlroomtype.DataTextField = "UsageTypeName";
                ddlroomtype.DataValueField = "UsageTypeName";
                ddlroomtype.DataSource = r;
                ddlroomtype.DataBind();
            }
            else
            {
                ddlroomtype.DataSource = null;
                ddlroomtype.DataBind();

            }
            ddlroomtype.Items.Insert(0, new ListItem("Select Room type", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void bind_rep()
        {
            string[] companyname = txtcompanyname.Text.Split(',');
            string[] customercode = txtcustomercode.Text.Split(',');
            string[] floor = txtfloor.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');
            string[] shopname = txtshopname.Text.Split(',');

            DataTable dt = new DataTable();
            dt.Columns.Add("IndustryGroupNameTH");
            dt.Columns.Add("CustomerCode");
            dt.Columns.Add("CustomerName");
            dt.Columns.Add("ShopName");
            dt.Columns.Add("Room_No");
            dt.Columns.Add("Floor");
            dt.Columns.Add("Total_area");

            int index_1 = 8;

            int xx = Convert.ToInt32(Convert.ToDateTime(txtenddate.Text).ToString("MM")) - Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).ToString("MM"));
            if (xx < 0)
            {
                POPUPMSG("Start Date ห้ามมากกว่า End Date");
                return;
            }
            else
            {
                for (int j = 0; j <= xx; j++)
                {
                    dt.Columns.Add(Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("MMM-yy"));

                }


                dt.Columns.Add("Total_all");
                dt.Columns.Add("Avg_Month");
                dt.Columns.Add("AVG_SQM");

            }

            int d_end = DateTime.DaysInMonth(Convert.ToInt32(Convert.ToDateTime(txtenddate.Text).ToString("yyyy")),
                    Convert.ToInt32(Convert.ToDateTime(txtenddate.Text).ToString("MM")));

            DataTable rep = new DataTable();

            string Company = "";
            Company = HttpContext.Current.Session["s_com_code"].ToString();


            if (HttpContext.Current.Session["s_user_group"].ToString() == "ae")
            {
                rep = Serv.GetRep1_tmp_ae(txtstartdate.Text + "-01", txtenddate.Text + "-" + d_end.ToString(),
                shopname, companyname, floor, customercode, shopgroup, ddlbuilding.SelectedValue,
                ddlroomtype.SelectedValue, ddlcontract_type.SelectedValue, ddlcatagorLeasing.SelectedValue, ddlgroup_loca.SelectedValue,
                ddlindustrygroup.SelectedValue, HttpContext.Current.Session["s_user_for_shop"].ToString(), Company, ddlstatus.SelectedValue);
            }
            else
            {
                rep = Serv.GetRep1_tmp(txtstartdate.Text + "-01", txtenddate.Text + "-" + d_end.ToString(),
                shopname, companyname, floor, customercode, shopgroup, ddlbuilding.SelectedValue,
                ddlroomtype.SelectedValue, ddlcontract_type.SelectedValue, ddlcatagorLeasing.SelectedValue, ddlgroup_loca.SelectedValue,
                ddlindustrygroup.SelectedValue, HttpContext.Current.Session["s_user_for_shop"].ToString(), Company, ddlstatus.SelectedValue);
            }


            if (rep.Rows.Count != 0)
            {


                for (int c = 0; c < rep.Rows.Count; c++)
                {
                    DataRow row1 = dt.NewRow();

                    row1["IndustryGroupNameTH"] = rep.Rows[c]["IndustryGroupNameTH"].ToString();
                    row1["CustomerCode"] = rep.Rows[c]["BusinessPartnerCode"].ToString();
                    row1["CustomerName"] = rep.Rows[c]["BusinessPartnerName"].ToString();
                    row1["ShopName"] = rep.Rows[c]["ShopName"].ToString();
                    row1["Room_No"] = rep.Rows[c]["smart_room_no"].ToString();
                    row1["Floor"] = rep.Rows[c]["smart_floor"].ToString();
                    row1["Total_area"] = rep.Rows[c]["smart_sqm"].ToString();

                    decimal sum_x = 0;
                    int cnt_m = 0;
                    for (int j = 0; j <= xx; j++)
                    {


                        int d_end_ = DateTime.DaysInMonth(Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("yyyy")),
                      Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("MM")));

                        string d1 = Convert.ToDateTime(txtstartdate.Text + "-01").AddMonths(j).ToString("yyyy-MM-dd");
                        string d2 = Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("yyyy-MM") + "-" + d_end_;

                        var rep_ = Serv.GetRep1(d1, d2, rep.Rows[c]["ContractNumber"].ToString());

                        if (rep_.Rows.Count != 0)
                        {
                            cnt_m = cnt_m + 1;
                            row1[Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("MMM-yy")] = Convert.ToDecimal(rep_.Rows[0]["total_ex_var"]).ToString("#,##0.00");
                            sum_x = sum_x + Convert.ToDecimal(rep_.Rows[0]["total_ex_var"]);
                        }
                        else
                        {
                            row1[Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("MMM-yy")] = "0.00";
                        }
                    }

                    decimal Avg_Month = sum_x / cnt_m;
                    decimal AVG_SQM = sum_x / Convert.ToDecimal(rep.Rows[c]["smart_sqm"].ToString());

                    row1["Total_all"] = sum_x.ToString("#,##0.00");
                    row1["Avg_Month"] = Avg_Month.ToString("#,##0.00");
                    row1["AVG_SQM"] = AVG_SQM.ToString("#,##0.00");

                    dt.Rows.Add(row1);
                }
            }

            if (dt.Rows.Count != 0)
            {
                GridView_List.DataSource = dt;
                GridView_List.DataBind();

                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();

                GridView1.DataSource = null;
                GridView1.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count != 0)
            {

                //Response.Clear();
                //Response.AddHeader("content-disposition", "attachment; filename = monthly_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                ////Response.AddHeader("content-disposition", "attachment;filename=Export1.xlsx");
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.ContentEncoding = System.Text.Encoding.Unicode;
                //Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                //System.IO.StringWriter sw = new System.IO.StringWriter();
                //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                //GridView1.RenderControl(hw);
                //string headerTable = @"<Table>" +
                //        "<tr align='center'><td colspan='8'>Tenant Sales Data Collection</td></tr>" +
                //        "<tr align='center'><td colspan='8'>MONTHLY Report" + txtstartdate.Text + " - " + txtenddate.Text + "</td></tr>" +
                //        "<tr align='center'><td colspan='8'></td></tr>" +
                //        "</Table>";
                //Response.Write(headerTable);
                //Response.Write(sw.ToString());
                //Response.End();


                //===================================================================

                #region export_1
                //DataTable dt = new DataTable("monthly_report");
                //foreach (TableCell cell in GridView1.HeaderRow.Cells)
                //{
                //    dt.Columns.Add(cell.Text.Replace("&nbsp;", ""));
                //}
                //foreach (GridViewRow row in GridView1.Rows)
                //{
                //    dt.Rows.Add();
                //    for (int i = 0; i < row.Cells.Count; i++)
                //    {
                //        dt.Rows[dt.Rows.Count - 1][i] = row.Cells[i].Text.Replace("&nbsp;", "").Replace("&amp;", "&");
                //    }
                //}

                //using (XLWorkbook wb = new XLWorkbook())
                //{

                //    wb.Worksheets.Add(dt);

                //    Response.Clear();
                //    Response.Buffer = true;
                //    Response.Charset = "";

                //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.AddHeader("content-disposition", "attachment;filename=monthly_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                //    Response.ContentEncoding = System.Text.Encoding.Unicode;

                //    using (MemoryStream MyMemoryStream = new MemoryStream())
                //    {
                //        wb.SaveAs(MyMemoryStream);
                //        MyMemoryStream.WriteTo(Response.OutputStream);
                //        Response.Flush();
                //        Response.End();
                //    }
                //}
                #endregion


                var aCode = 65;

                DataTable dt = new DataTable();
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    dt.Columns.Add(cell.Text.Replace("&nbsp;", ""));
                }
                foreach (GridViewRow row_ in GridView1.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < GridView1.HeaderRow.Cells.Count; j++)
                    {
                        if (j < 6)
                        {
                            dr[GridView1.HeaderRow.Cells[j].Text] = "'" + row_.Cells[j].Text.Replace("&nbsp;", "").Replace("&amp;", "&").Replace("&#39;", "'");
                        }
                        else
                        {
                            dr[GridView1.HeaderRow.Cells[j].Text] = row_.Cells[j].Text.Replace("&nbsp;", "").Replace("&amp;", "&").Replace(",", "").Replace("&#39;", "'");
                        }
                    }

                    dt.Rows.Add(dr);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("monthly_report" + DateTime.Now.ToString("yyyyMMdd"));

                    var wsReportNameHeaderRange_1 = ws.Range(string.Format("A{0}:{1}{0}", 1, Char.ConvertFromUtf32(aCode + dt.Columns.Count - 1)));
                    wsReportNameHeaderRange_1.Style.Font.Bold = false;
                    wsReportNameHeaderRange_1.Merge();
                    wsReportNameHeaderRange_1.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wsReportNameHeaderRange_1.Value = "Tenant Sales Data Collection";

                    var wsReportNameHeaderRange_2 = ws.Range(string.Format("A{0}:{1}{0}", 2, Char.ConvertFromUtf32(aCode + dt.Columns.Count - 1)));
                    wsReportNameHeaderRange_2.Style.Font.Bold = false;
                    wsReportNameHeaderRange_2.Merge();
                    wsReportNameHeaderRange_2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wsReportNameHeaderRange_2.Value = "MONTHLY Report " + txtstartdate.Text.Replace("/", "-") + " - " + txtenddate.Text.Replace("/", "-");


                    ws.Row(2).InsertRowsBelow(1);
                    ws.Row(3).Style.Border.OutsideBorder = XLBorderStyleValues.None;
                    ws.Row(3).Style.Border.RightBorder = XLBorderStyleValues.None;
                    ws.Row(3).Style.Border.LeftBorder = XLBorderStyleValues.None;

                    int rowIndex = 4;
                    int columnIndex = 0;


                    foreach (DataColumn column in dt.Columns)
                    {
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Value = "'" + column.ColumnName;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Font.Bold = true;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        columnIndex++;
                    }
                    rowIndex++;

                    foreach (DataRow row_ in dt.Rows)
                    {

                        int valueCount = 0;
                        foreach (object rowValue in row_.ItemArray)
                        {
                            string value_deci = rowValue.ToString();
                            decimal a;
                            string numberic_low = "";
                            string numberic_negative = "";

                            if (Decimal.TryParse(value_deci, out a))
                            {                                
                                if (Convert.ToDecimal(value_deci) < 0)
                                {
                                    numberic_negative = "number of negative";
                                }
                                else if (Convert.ToDecimal(value_deci) < 1.0M)
                                {
                                    numberic_low = "less than 0.49";
                                }
                            }

                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue;
                            if (rowValue.ToString() != "0.00" && numberic_low == "")
                            {
                                ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "#,###0.00";
                            }
                            else
                            {
                                ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "0.00";                            
                            }

                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            valueCount++;
                        }
                        rowIndex++;
                    }

                    ws.Columns("A", "Z").AdjustToContents();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=monthly_report" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {

                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }


                }


            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/monthly_rep.aspx");
        }
    }
}