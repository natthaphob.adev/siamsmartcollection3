﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sap_rep.aspx.cs" Inherits="iconsiam.sap_rep" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

    <script type="text/javascript">

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Sales for SAP 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtshopgroup" runat="server" placeholder="Shop Group Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtshopname" runat="server" placeholder="Shop Name" class="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlar" runat="server" class="form-control" Visible="false"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off" onkeypress="return isNumber(event)"></asp:TextBox>
                    <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="yyyy-MM"></asp:CalendarExtender>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" placeholder="Status" class="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;"></div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;"></div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn" Width ="25%" />
                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click" class="btn ThemeBtn" Width ="25%" />
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" Width ="25%" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="100" class="table mt-3">
                        <Columns>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" Checked="true" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_smart_record_keyin_type" runat="server" Value='<%# Eval("smart_record_keyin_type") %>' />
                                    <asp:HiddenField ID="hdd_ShopName" runat="server" Value='<%# Eval("ShopName") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click1" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>


                    <asp:GridView ID="GridView_List2" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List2_PageIndexChanging"
                        ShowFooter="false" PageSize="100" class="table mt-3">
                        <Columns>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" Checked="true" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="arname" HeaderText="AR" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_smart_record_keyin_type" runat="server" Value='<%# Eval("smart_record_keyin_type") %>' />
                                    <asp:HiddenField ID="hdd_ShopName" runat="server" Value='<%# Eval("ShopName") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click1" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>

    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Visible="true"
            ShowFooter="false" class="table table-bordered">
            <Columns>
                <asp:BoundField DataField="d1" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d2" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d3" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d4" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d5" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d6" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d7" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d8" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d9" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d10" HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

            </Columns>
        </asp:GridView>
    </asp:Panel>



</asp:Content>
