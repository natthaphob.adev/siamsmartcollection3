﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class sap_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        conteactList_SAPDLL Serv = new conteactList_SAPDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);
                    bind_default();
                    bind_rep();

                }

                if (HttpContext.Current.Session["code_theme"] != null)
                {
                    this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                }
                else
                {
                    this.MyTheme = "#CCA9DA";
                }

                if (HttpContext.Current.Session["code_Navbar"] != null)
                {
                    this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                }
                else
                {
                    this.NavbarColor = "#2d2339";
                }

            }
        }

        protected void bind_default()
        {

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            //if (HttpContext.Current.Session["role"].ToString() == "officer")
            //{
            //    ddlar.Visible = false;
            //}
            //else
            //{
            //    ddlar.Visible = true;

            //    var u = Serv.GetUserByUserid(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //    if (u.Rows.Count != 0)
            //    {
            //        ddlar.DataTextField = "name";
            //        ddlar.DataValueField = "userid";
            //        ddlar.DataSource = u;
            //        ddlar.DataBind();
            //    }
            //    else
            //    {
            //        ddlar.DataSource = null;
            //        ddlar.DataBind();
            //    }
            //    ddlar.Items.Insert(0, new ListItem("User", ""));
            //}

            ddlar.Visible = true;

            //var u = Serv.GetUserByUserid(HttpContext.Current.Session["s_user_for_shop"].ToString());
            var u = Serv.GetUserAR(HttpContext.Current.Session["s_com_code"].ToString());
            if (u.Rows.Count != 0)
            {
                ddlar.DataTextField = "name";
                ddlar.DataValueField = "userid";
                ddlar.DataSource = u;
                ddlar.DataBind();
            }
            else
            {
                ddlar.DataSource = null;
                ddlar.DataBind();
            }
            ddlar.Items.Insert(0, new ListItem("User", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }
        protected void bind_rep()
        {
            string[] shopname = txtshopname.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');

            string Company = "";

            if (ddlcompany.SelectedValue == "")
            {
                Company = HttpContext.Current.Session["s_com_code"].ToString();
            }
            else
            {
                Company = "'" + ddlcompany.SelectedValue + "'";
            }


            var indust = Serv.GetSmart_Contract_owner_by_date("", Company, shopname, shopgroup, ddlar.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString(), "", Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"));


            if (indust.Rows.Count != 0)
            {

                ////--------------------

                if (HttpContext.Current.Session["role"].ToString() == "officer")
                {
                    GridView_List.DataSource = indust;
                    GridView_List.DataBind();

                }
                else
                {
                    GridView_List2.DataSource = indust;
                    GridView_List2.DataBind();
                }


            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();

                GridView_List2.DataSource = null;
                GridView_List2.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }




        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


        protected void btnexport_Click1(object sender, EventArgs e)
        {
            string company = "";
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");

            DataTable dt = new DataTable();
            dt.Columns.Add("d1");
            dt.Columns.Add("d2");
            dt.Columns.Add("d3");
            dt.Columns.Add("d4");
            dt.Columns.Add("d5");
            dt.Columns.Add("d6");
            dt.Columns.Add("d7");
            dt.Columns.Add("d8");
            dt.Columns.Add("d9");
            dt.Columns.Add("d10");

            var sh = Serv.GetShopInfo(hdd_id.Value);
            if (sh.Rows.Count != 0)
            {
                DataRow row1 = dt.NewRow();

                decimal s1 = 0;
                decimal s2 = 0;
                decimal s3 = 0;
                decimal s4 = 0;
                decimal s5 = 0;
                decimal s6 = 0;


                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "Vat";
                        row1["d4"] = "Sales inc VAT";
                        row1["d5"] = "Status";
                        row1["d6"] = "Status sale";
                        row1["d7"] = "Remark";
                        //row1["d7"] = "";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);

                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d5"] = t.Rows[i]["status"].ToString();
                            row1["d6"] = t.Rows[i]["statussales"].ToString();
                            row1["d7"] = t.Rows[i]["remark"].ToString();
                            //row1["d7"] = "";
                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = "";
                        row1["d6"] = "";
                        row1["d7"] = "";
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }
                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "Service ex VAT";
                        row1["d4"] = "VAT(Sale)";
                        row1["d5"] = "Sales inc VAT";
                        row1["d6"] = "VAT(Service)";
                        row1["d7"] = "Service inc VAT";
                        row1["d8"] = "Status";
                        row1["d9"] = "Status sale";
                        row1["d10"] = "Remark";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            row1["d8"] = t.Rows[i]["status"].ToString();
                            row1["d9"] = t.Rows[i]["statussales"].ToString();
                            row1["d10"] = t.Rows[i]["remark"].ToString();

                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                        row1["d8"] = "";
                        row1["d9"] = "";
                        row1["d10"] = "";
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }
                else
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "VAT";
                        row1["d4"] = "Sale before Service Charge";
                        row1["d5"] = "Service Charge";
                        row1["d6"] = "Sales inc VAT";
                        row1["d7"] = "Status";
                        row1["d8"] = "Status sale";
                        row1["d9"] = "Remark";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(x);

                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d7"] = t.Rows[i]["status"].ToString();
                            row1["d8"] = t.Rows[i]["statussales"].ToString();
                            row1["d9"] = t.Rows[i]["remark"].ToString();

                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                        row1["d7"] = "";// Convert.ToDecimal(s6).ToString("0.00");
                        row1["d8"] = "";
                        row1["d9"] = "";
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }

                if (dt.Rows.Count != 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }


                if (GridView1.Rows.Count != 0)
                {
                    var aCode = 65;
                    //Response.Clear();
                    //Response.AddHeader("content-disposition", "attachment; filename = daily_sap_rep_" + sh.Rows[0]["ShopName"].ToString() + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                    ////Response.AddHeader("content-disposition", "attachment;filename=Export1.xlsx");
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.ContentEncoding = System.Text.Encoding.Unicode;
                    //Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                    //System.IO.StringWriter sw = new System.IO.StringWriter();
                    //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                    //GridView1.RenderControl(hw);

                    //string headerTable = @"<Table>" +
                    //        "<tr align='left'><td>" + company + "</td></tr>" +
                    //        "<tr align='left'><td>Daily sales Report</td><td></td><td>Type</td><td>GP</td></tr>" +
                    //        "<tr align='left'><td>Code</td><td>" + sh.Rows[0]["BusinessPartnerCode"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Company Name</td><td>" + sh.Rows[0]["BusinessPartnerName"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Shop Name</td><td>" + sh.Rows[0]["ShopName"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Room</td><td>" + sh.Rows[0]["smart_room_no"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Area (Sq.m)</td><td>" + sh.Rows[0]["smart_sqm"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Contract No.</td><td>" + sh.Rows[0]["ContractNumber"].ToString() + "</td></tr>" +
                    //        "<tr align='left'><td>Month</td><td>" + Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI) + "</td></tr>" +
                    //        "</Table>";
                    //Response.Write(headerTable);



                    //Response.Write(sw.ToString());
                    //Response.End();

                    DataTable dt_ = new DataTable();
                    for (int i = 0; i < GridView1.Columns.Count; i++)
                    {
                        dt_.Columns.Add("column" + i.ToString());
                    }
                    foreach (GridViewRow row_ in GridView1.Rows)
                    {
                        DataRow dr = dt_.NewRow();
                        for (int j = 0; j < GridView1.Columns.Count; j++)
                        {
                            if (j < 1)
                            {
                                dr["column" + j.ToString()] = "'" + row_.Cells[j].Text.Replace("&nbsp;", "").Replace("&amp;", "&").Replace("&#39;", "'");
                            }
                            else
                            {
                                dr["column" + j.ToString()] = row_.Cells[j].Text.Replace("&nbsp;", "").Replace(",", "").Replace("&amp;", "&").Replace("&#39;", "'");
                            }
                        }

                        dt_.Rows.Add(dr);
                    }

                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        string shopname = "";

                        if (sh.Rows[0]["ShopName"].ToString().Length < 31)
                        {
                            shopname = sh.Rows[0]["ShopName"].ToString().Replace("'", "").Replace("&#39;", "'").Replace("\\", "").Replace("/", "").Replace(":", "")
                                .Replace("?", "").Replace("*", "").Replace(";", "").Replace("[", "").Replace("]", "").Replace(" ", "");
                        }
                        else
                        {
                            shopname = sh.Rows[0]["ShopName"].ToString().Substring(0, 20).Replace("'", "").Replace("&#39;", "'").Replace("\\", "").Replace("/", "").Replace(":", "")
                                .Replace("?", "").Replace("*", "").Replace(";", "").Replace("[", "").Replace("]", "").Replace(" ", "");
                        }

                        var ws = wb.Worksheets.Add(shopname);

                        var wsReportNameHeaderRange = ws.Range(string.Format("A{0}:{1}{0}", 1, Char.ConvertFromUtf32(aCode + 0)));
                        wsReportNameHeaderRange.Value = company;

                        var wsReportDateHeaderRange = ws.Range(string.Format("A2"));
                        wsReportDateHeaderRange.Value = "Daily sales Report";

                        var wsReportDateHeaderRange_1 = ws.Range(string.Format("B2"));
                        wsReportDateHeaderRange_1.Value = "";

                        var wsReportDateHeaderRange_2 = ws.Range(string.Format("C2"));
                        wsReportDateHeaderRange_2.Value = "Type";

                        var wsReportDateHeaderRange_3 = ws.Range(string.Format("D2"));
                        wsReportDateHeaderRange_3.Value = "GP";

                        var wsReportCreatedByHeaderRange = ws.Range(string.Format("A3"));
                        wsReportCreatedByHeaderRange.Value = "Code";

                        var wsReportCreatedByHeaderRange1 = ws.Range(string.Format("B3"));
                        wsReportCreatedByHeaderRange1.Value = "'" + sh.Rows[0]["BusinessPartnerCode"].ToString();

                        var wsReportCreatedByHeaderRange_1 = ws.Range(string.Format("A4"));
                        wsReportCreatedByHeaderRange_1.Value = "Company Name";

                        var wsReportCreatedByHeaderRange_1_1 = ws.Range(string.Format("B4"));
                        wsReportCreatedByHeaderRange_1_1.Value = sh.Rows[0]["BusinessPartnerName"].ToString();

                        var wsReportCreatedByHeaderRange_2 = ws.Range(string.Format("A5"));
                        wsReportCreatedByHeaderRange_2.Value = "Shop Name";

                        var wsReportCreatedByHeaderRange_2_1 = ws.Range(string.Format("B5"));
                        wsReportCreatedByHeaderRange_2_1.Value = sh.Rows[0]["ShopName"].ToString();

                        var wsReportCreatedByHeaderRange_3 = ws.Range(string.Format("A6"));
                        wsReportCreatedByHeaderRange_3.Value = "Room";

                        var wsReportCreatedByHeaderRange_3_1 = ws.Range(string.Format("B6"));
                        wsReportCreatedByHeaderRange_3_1.Value = sh.Rows[0]["smart_room_no"].ToString();

                        var wsReportCreatedByHeaderRange_4 = ws.Range(string.Format("A7"));
                        wsReportCreatedByHeaderRange_4.Value = "Area (Sq.m)";

                        var wsReportCreatedByHeaderRange_4_1 = ws.Range(string.Format("B7"));
                        wsReportCreatedByHeaderRange_4_1.Value = "'" + sh.Rows[0]["smart_sqm"].ToString();

                        var wsReportCreatedByHeaderRange_5 = ws.Range(string.Format("A8"));
                        wsReportCreatedByHeaderRange_5.Value = "Contract No.";

                        var wsReportCreatedByHeaderRange_5_1 = ws.Range(string.Format("B8"));
                        wsReportCreatedByHeaderRange_5_1.Value = "'" + sh.Rows[0]["ContractNumber"].ToString();

                        var wsReportCreatedByHeaderRange_6 = ws.Range(string.Format("A9"));
                        wsReportCreatedByHeaderRange_6.Value = "Month";

                        var wsReportCreatedByHeaderRange_6_1 = ws.Range(string.Format("B9"));
                        wsReportCreatedByHeaderRange_6_1.Value = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);


                        ws.Row(9).InsertRowsBelow(1);
                        ws.Row(10).Style.Border.OutsideBorder = XLBorderStyleValues.None;
                        ws.Row(10).Style.Border.RightBorder = XLBorderStyleValues.None;
                        ws.Row(10).Style.Border.LeftBorder = XLBorderStyleValues.None;

                        int rowIndex = 10;
                        int columnIndex = 0;
                        int rowIndex2 = 0;
                        int columnIndex2 = 0;
                        int numcolor = 0;

                        foreach (DataColumn column in dt_.Columns)
                        {
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Value = "";// column.ColumnName;
                            //ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            columnIndex++;
                        }
                        rowIndex++;
                        rowIndex2 = rowIndex;

                        foreach (DataRow row_ in dt_.Rows)
                        {
                            //int valueCount = 0;
                            //foreach (object rowValue in row_.ItemArray)
                            //{
                            //    if (valueCount == 0)
                            //    {
                            //        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue;
                            //    }
                            //    else
                            //    {
                            //        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = "'" + rowValue;
                            //    }

                            //    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "#,###0.00";
                            //    //ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            //    valueCount++;
                            //}
                            //rowIndex++;

                            int valueCount = 0;
                            foreach (object rowValue in row_.ItemArray)
                            {
                                string value_deci = rowValue.ToString();
                                decimal a;
                                string numberic_low = "";
                                string numberic_negative = "";

                                if (Decimal.TryParse(value_deci, out a))
                                {
                                    if (Convert.ToDecimal(value_deci) < 0)
                                    {
                                        numberic_negative = "number of negative";
                                    }
                                    else if (Convert.ToDecimal(value_deci) < 1.0M)
                                    {
                                        numberic_low = "less than 0.49";
                                    }
                                }

                                ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue;

                                if (rowValue.ToString() != "0.00" && numberic_low == "")
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "#,###0.00";
                                }
                                else
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.NumberFormat.Format = "0.00";
                                }

                                //ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                valueCount++;
                            }
                            rowIndex++;

                        }

                        foreach (DataRow row_ in dt_.Rows)
                        {
                            if (numcolor < (dt_.Rows.Count / 2))
                            {
                                columnIndex2 = 0;
                                foreach (DataColumn column in dt_.Columns)
                                {
                                    if (columnIndex2 < dt_.Columns.Count)
                                    {
                                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex2), rowIndex2)).Style.Fill.BackgroundColor = XLColor.FromTheme(XLThemeColor.Accent1, 0.7);

                                    }
                                    columnIndex2++;
                                }
                                rowIndex2 = rowIndex2 + 2;
                            }
                            numcolor++;
                        }


                        ws.Columns("A", "Z").AdjustToContents();


                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=daily_sap_rep_" + sh.Rows[0]["ShopName"].ToString().Replace("'", "").Replace("&#39;", "'").Replace("\\", "").Replace("/", "").Replace(":", "").Replace(",", "")
                                .Replace("?", "").Replace("*", "").Replace(";", "").Replace("[", "").Replace("]", "").Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {

                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }


                    }



                }

            }
        }


        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            string company = "";
            if (GridView_List.Rows.Count != 0)
            {
                DataSet ds = new DataSet();
                string shopName_ = "";


                foreach (GridViewRow row in GridView_List.Rows) //Running all lines of grid
                {

                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                        HiddenField hdd_ShopName = (HiddenField)row.FindControl("hdd_ShopName");
                        HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");
                        CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");

                        if (chkRow.Checked)
                        {

                            string name_ = "";

                            if (shopName_.Contains(hdd_ShopName.Value.Replace("/", "-").Replace("'", "")) == true)
                            {
                                name_ = hdd_ShopName.Value.Replace("/", "-").Replace("'", "") + "_1";

                                shopName_ = shopName_ + "::" + name_;
                            }
                            else
                            {
                                name_ = hdd_ShopName.Value.Replace("/", "-").Replace("'", "");

                                shopName_ = shopName_ + "::" + name_;
                            }

                            DataTable dt = new DataTable(name_);
                            dt.Columns.Add("d1");
                            dt.Columns.Add("d2");
                            dt.Columns.Add("d3");
                            dt.Columns.Add("d4");
                            dt.Columns.Add("d5");
                            dt.Columns.Add("d6");
                            dt.Columns.Add("d7");
                            dt.Columns.Add("d8");
                            dt.Columns.Add("d9");
                            dt.Columns.Add("d10");

                            var sh = Serv.GetShopInfo(hdd_id.Value);
                            if (sh.Rows.Count != 0)
                            {
                                DataRow row1 = dt.NewRow();

                                decimal s1 = 0;
                                decimal s2 = 0;
                                decimal s3 = 0;
                                decimal s4 = 0;
                                decimal s5 = 0;
                                decimal s6 = 0;


                                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                                {

                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM-01"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {

                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        //row1 = dt.NewRow();
                                        //row1["d1"] = "Date";
                                        //row1["d2"] = "Sales ex VAT";
                                        //row1["d3"] = "Vat";
                                        //row1["d4"] = "Sales inc VAT";
                                        //row1["d5"] = "";
                                        //row1["d6"] = "";
                                        //row1["d7"] = "";
                                        //dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);


                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Vat";
                                        row1["d4"] = "Sales inc VAT";
                                        row1["d5"] = "Status";
                                        row1["d6"] = "Status sale";
                                        row1["d7"] = "Remark";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();

                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            //row1["d5"] = "";
                                            //row1["d6"] = "";
                                            //row1["d7"] = "";

                                            row1["d5"] = t.Rows[i]["status"].ToString();
                                            row1["d6"] = t.Rows[i]["statussales"].ToString();
                                            row1["d7"] = t.Rows[i]["remark"].ToString();

                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";

                                        dt.Rows.Add(row1);
                                    }
                                }
                                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                                {

                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        //row1 = dt.NewRow();
                                        //row1["d1"] = "Date";
                                        //row1["d2"] = "Sales ex VAT";
                                        //row1["d3"] = "Vat";
                                        //row1["d4"] = "Sales inc VAT";
                                        //row1["d5"] = "";
                                        //row1["d6"] = "";
                                        //row1["d7"] = "";
                                        //dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Service ex VAT";
                                        row1["d4"] = "VAT(Sale)";
                                        row1["d5"] = "Sales inc VAT";
                                        row1["d6"] = "VAT(Service)";
                                        row1["d7"] = "Service inc VAT";
                                        row1["d8"] = "Status";
                                        row1["d9"] = "Status sale";
                                        row1["d10"] = "Remark";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d8"] = t.Rows[i]["status"].ToString();
                                            row1["d9"] = t.Rows[i]["statussales"].ToString();
                                            row1["d10"] = t.Rows[i]["remark"].ToString();

                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else
                                {


                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        //row1 = dt.NewRow();
                                        //row1["d1"] = "Date";
                                        //row1["d2"] = "Sales ex VAT";
                                        //row1["d3"] = "Vat";
                                        //row1["d4"] = "Sales inc VAT";
                                        //row1["d5"] = "";
                                        //row1["d6"] = "";
                                        //row1["d7"] = "";
                                        //dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "VAT";
                                        row1["d4"] = "Sale before Service Charge";
                                        row1["d5"] = "Service Charge";
                                        row1["d6"] = "Sales inc VAT";
                                        //row1["d7"] = "";
                                        row1["d7"] = "Status";
                                        row1["d8"] = "Status sale";
                                        row1["d9"] = "Remark";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(x);

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            //row1["d7"] = "";
                                            row1["d7"] = t.Rows[i]["status"].ToString();
                                            row1["d8"] = t.Rows[i]["statussales"].ToString();
                                            row1["d9"] = t.Rows[i]["remark"].ToString();
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        //row1["d7"] = "";// Convert.ToDecimal(s6).ToString("0.00");
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }

                            }

                            if (dt.Rows.Count != 0)
                            {
                                ds.Tables.Add(dt);
                            }
                        }
                    }
                }

                if (ds.Tables.Count != 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        foreach (DataTable dt in ds.Tables)
                        {
                            if (dt.TableName.ToString().Length > 31)
                            {
                                dt.TableName = dt.TableName.ToString().Substring(0, 20);
                            }

                            //Add DataTable as Worksheet.
                            var ws = wb.Worksheets.Add(dt.TableName.Replace("'", "").Replace("&#39;", "'").Replace("\\", "").Replace("/", "").Replace(":", "")
                                .Replace("?", "").Replace("*", "").Replace(";", "").Replace("[", "").Replace("]", "").Replace(" ", ""));

                            ws.FirstRow().FirstCell().InsertData(dt.Rows);
                            ws.Columns("A", "Z").AdjustToContents();

                            int rowIndex2 = 11;
                            int columnIndex2 = 0;
                            int numcolor = 0;
                            var aCode = 65;

                            foreach (DataRow row_ in dt.Rows)
                            {
                                if (numcolor < ((dt.Rows.Count - 10) / 2))
                                {
                                    columnIndex2 = 0;
                                    foreach (DataColumn column in dt.Columns)
                                    {
                                        if (columnIndex2 < dt.Columns.Count)
                                        {
                                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex2), rowIndex2)).Style.Fill.BackgroundColor = XLColor.FromTheme(XLThemeColor.Accent1, 0.7);

                                        }
                                        columnIndex2++;
                                    }
                                    rowIndex2 = rowIndex2 + 2;
                                }
                                numcolor++;
                            }

                            int rowIndex3 = 12;
                            foreach (DataRow row_ in dt.Rows)
                            {
                                columnIndex2 = 1;
                                foreach (DataColumn column in dt.Columns)
                                {
                                    if (columnIndex2 < dt.Columns.Count)
                                    {
                                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex2), rowIndex3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    }
                                    columnIndex2++;
                                }
                                rowIndex3++;
                            }

                        }

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


                        Response.AddHeader("content-disposition", "attachment;filename=daily_sap_rep_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    POPUPMSG("ร้านค้าที่ท่านเลือกไม่มียอดในเดือนนี้");
                    return;
                }



            }
            else if (GridView_List2.Rows.Count != 0)
            {
                DataSet ds = new DataSet();

                string shopName_ = "";

                foreach (GridViewRow row in GridView_List2.Rows) //Running all lines of grid
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {


                        HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                        HiddenField hdd_ShopName = (HiddenField)row.FindControl("hdd_ShopName");
                        HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");
                        CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");

                        if (chkRow.Checked)
                        {
                            string name_ = "";

                            if (shopName_.Contains(hdd_ShopName.Value.Replace("/", "-").Replace("'", "")) == true)
                            {
                                name_ = hdd_ShopName.Value.Replace("/", "-").Replace("'", "") + "_1";

                                shopName_ = shopName_ + "::" + name_;
                            }
                            else
                            {
                                name_ = hdd_ShopName.Value.Replace("/", "-").Replace("'", "");

                                shopName_ = shopName_ + "::" + name_;
                            }

                            DataTable dt = new DataTable(name_);
                            dt.Columns.Add("d1");
                            dt.Columns.Add("d2");
                            dt.Columns.Add("d3");
                            dt.Columns.Add("d4");
                            dt.Columns.Add("d5");
                            dt.Columns.Add("d6");
                            dt.Columns.Add("d7");
                            dt.Columns.Add("d8");
                            dt.Columns.Add("d9");
                            dt.Columns.Add("d10");

                            var sh = Serv.GetShopInfo(hdd_id.Value);
                            if (sh.Rows.Count != 0)
                            {
                                DataRow row1;

                                decimal s1 = 0;
                                decimal s2 = 0;
                                decimal s3 = 0;
                                decimal s4 = 0;
                                decimal s5 = 0;
                                decimal s6 = 0;


                                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Vat";
                                        row1["d4"] = "Sales inc VAT";
                                        row1["d5"] = "Status";
                                        row1["d6"] = "Status sale";
                                        row1["d7"] = "Remark";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();

                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            //row1["d5"] = "";
                                            //row1["d6"] = "";
                                            //row1["d7"] = "";
                                            row1["d5"] = t.Rows[i]["status"].ToString();
                                            row1["d6"] = t.Rows[i]["statussales"].ToString();
                                            row1["d7"] = t.Rows[i]["remark"].ToString();
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Service ex VAT";
                                        row1["d4"] = "VAT(Sale)";
                                        row1["d5"] = "Sales inc VAT";
                                        row1["d6"] = "VAT(Service)";
                                        row1["d7"] = "Service inc VAT";
                                        row1["d8"] = "Status";
                                        row1["d9"] = "Status sale";
                                        row1["d10"] = "Remark";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d8"] = t.Rows[i]["status"].ToString();
                                            row1["d9"] = t.Rows[i]["statussales"].ToString();
                                            row1["d10"] = t.Rows[i]["remark"].ToString();

                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";

                                        dt.Rows.Add(row1);
                                    }
                                }
                                else
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString().Replace("/", "-").Replace("&#39;", "'");
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "VAT";
                                        row1["d4"] = "Sale before Service Charge";
                                        row1["d5"] = "Service Charge";
                                        row1["d6"] = "Sales inc VAT";
                                        //row1["d7"] = "";
                                        row1["d7"] = "Status";
                                        row1["d8"] = "Status sale";
                                        row1["d9"] = "Remark";
                                        dt.Rows.Add(row1);


                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(x);

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            //row1["d7"] = "";
                                            row1["d7"] = t.Rows[i]["status"].ToString();
                                            row1["d8"] = t.Rows[i]["statussales"].ToString();
                                            row1["d9"] = t.Rows[i]["remark"].ToString();
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = "";// Convert.ToDecimal(s6).ToString("0.00");
                                        row1["d8"] = "";
                                        row1["d9"] = "";
                                        row1["d10"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }



                            }


                            if (dt.Rows.Count != 0)
                            {
                                ds.Tables.Add(dt);
                            }
                        }


                    }


                }

                if (ds.Tables.Count != 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {

                        foreach (DataTable dt in ds.Tables)
                        {
                            if (dt.TableName.ToString().Length > 31)
                            {
                                dt.TableName = dt.TableName.ToString().Substring(0, 20);
                            }


                            //Add DataTable as Worksheet.
                            var ws = wb.Worksheets.Add(dt.TableName.Replace("'", "").Replace("&#39;", "'").Replace("\\", "").Replace("/", "").Replace(":", "")
                                .Replace("?", "").Replace("*", "").Replace(";", "").Replace("[", "").Replace("]", "").Replace(" ", ""));

                            ws.FirstRow().FirstCell().InsertData(dt.Rows);
                            ws.Columns("A", "Z").AdjustToContents();

                            int rowIndex2 = 11;
                            int columnIndex2 = 0;
                            int numcolor = 0;
                            var aCode = 65;

                            foreach (DataRow row_ in dt.Rows)
                            {
                                if (numcolor < ((dt.Rows.Count - 10) / 2))
                                {
                                    columnIndex2 = 0;
                                    foreach (DataColumn column in dt.Columns)
                                    {
                                        if (columnIndex2 < dt.Columns.Count)
                                        {
                                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex2), rowIndex2)).Style.Fill.BackgroundColor = XLColor.FromTheme(XLThemeColor.Accent1, 0.7);

                                        }
                                        columnIndex2++;
                                    }
                                    rowIndex2 = rowIndex2 + 2;
                                }
                                numcolor++;
                            }

                            int rowIndex3 = 12;
                            foreach (DataRow row_ in dt.Rows)
                            {
                                columnIndex2 = 1;
                                foreach (DataColumn column in dt.Columns)
                                {
                                    if (columnIndex2 < dt.Columns.Count)
                                    {
                                        ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex2), rowIndex3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    }
                                    columnIndex2++;
                                }
                                rowIndex3++;
                            }

                        }

                        //for (int j = 1; j <= ds.Tables.Count; j++)
                        //{
                        //    for (int i = 1; i <= 10; i++)
                        //    {
                        //        wb.Worksheets.Worksheet(j).Row(i).Style.Fill.BackgroundColor = XLColor.White;
                        //    }
                        //}


                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                        Response.AddHeader("content-disposition", "attachment;filename=daily_sap_rep_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    POPUPMSG("ร้านค้าที่ท่านเลือกไม่มียอดในเดือนนี้");
                    return;
                }

            }

        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/sap_rep.aspx");

        }

        protected void GridView_List2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List2.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }
    }
}