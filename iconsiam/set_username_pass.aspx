﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="set_username_pass.aspx.cs" Inherits="iconsiam.set_username_pass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }
    </script>

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p class="text-center">
                Create Username 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtusername" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtpassword_com" runat="server" TextMode="Password" placeholder="Confirm Password" class="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6" style="text-align: center">
            <asp:Button ID="btnsubmit" runat="server" Text="Login" OnClick="btnsubmit_Click" Width="100%" ForeColor="Black" class="btn" BackColor="#f8ca3e" BorderColor="#a78a65" />
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>



</asp:Content>
