﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="user_acc_tenant.aspx.cs" Inherits="iconsiam.user_acc_tenant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="Panel1" runat="server">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <p style="font-size: 25px; line-height: 1.5;">
                    User List (Tenant)
                </p>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row">
                     <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                        <asp:TextBox ID="txtcontactnumber" runat="server" class="form-control" placeholder="Contract Number"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                        <asp:TextBox ID="txtname" runat="server" class="form-control" placeholder="Username"></asp:TextBox>
                    </div>
                   <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                        <asp:TextBox ID="txtshopname" runat="server" class="form-control" placeholder="Shop Name"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                        <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                        <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"/>
                        <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" class="btn ThemeBtn"/>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div style="margin: 20px 0px; overflow: scroll">

                        <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            OnPageIndexChanging="GridView_List_PageIndexChanging"  OnRowDataBound="GridView_List_RowDataBound"
                            ShowFooter="false" PageSize="50" class="table mt-3">
                            <Columns>

                                <asp:BoundField DataField="contractnumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="username" HeaderText="Username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Expiry_date" HeaderText="Expiration date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdd_username" runat="server" Value='<%# Eval("username") %>' />
                                        <asp:HiddenField ID="hdd_contractNumber" runat="server" Value='<%# Eval("contractnumber") %>' />
                                        <asp:HiddenField ID="hdd_flag_active" runat="server" Value='<%# Eval("flag_active") %>' />
                                        <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Button ID="btnreset" runat="server" Text="Reset Password" OnClick="btnreset_Click" CssClass="btn btn-success btn-block" />
                                        <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </div>
                </div>


            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="Panel3" runat="server" Visible="false">

        <div class="row">
            <div class="col-12">
                <p class="text-center">
                    Edit Username 
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6">
                <asp:HiddenField ID="HiddenField_user" runat="server" />
                <asp:HiddenField ID="HiddenField_id" runat="server" />
                <asp:TextBox ID="txtusername" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6">
                <asp:DropDownList ID="ddlstatus2" runat="server" class="form-control"></asp:DropDownList>
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6">
                <asp:Button ID="btnreset2" runat="server" Text="Reset Password" OnClick="btnreset2_Click" CssClass="btn btn-success btn-block" />
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6" style="text-align: center">
                <asp:Button ID="btnsubmit" runat="server" Text="SAVE" OnClick="btnsubmit_Click" Width="100%" class="btn ThemeBtn" />
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>

    </asp:Panel>





</asp:Content>
